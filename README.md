# Common Unix Tools

This is a utility for setting up a common Unix environment among
several machines. It assumes that git is installed so that the needed
repositories can be cloned locally.

## Installation

The following series of commands should be all that are needed to set
up a new account once git is installed on the machine. For machines on
which you anticipate making changes to the `src` folder and committing
them back to the repository, first edit ~/.ssh/config to contain an
entry like:

	Host bitbucketalias
		User git
		Hostname bitbucket.org
		Port 22
		AddressFamily inet
		IdentityFile ~/.ssh/bitbucketalias.pem

and make sure that you upload the `bitbucketalias.pem` file. See
`~/usr/src/notes/cheatsheets/ssh.txt` for help. Then clone the repo
using ssh:

	cd
	mkdir usr
	cd usr
	git clone ssh://bitbucketalias/ssaundersla/usr-src.git src
	./src/install.sh

For machines on which you anticipate just using the repository and not
making changes to it, just use https:

	cd
	mkdir usr
	cd usr
	git config --global http.sslVerify false
	git clone https://ssaundersla@bitbucket.org/ssaundersla/usr-src.git src
	cd src
	rm ~/.gitconfig
	./install.sh

## Local Customization

The install script creates a ~/usr/local folder and places some
template files in it. You can edit these files to implement some local
special features. Note that you will need to edit the
`~/usr/local/update.sh` script and then run the `~/usr/src/update.sh`
script again

## Sample Customization

	cp ~/usr/src/notes/samples/login      ~/usr/local/dotfiles/login
	cp ~/usr/src/notes/samples/bashrc     ~/usr/local/dotfiles/bashrc
	cp ~/usr/src/notes/samples/gitconfig  ~/usr/local/dotfiles/gitconfig
	cp ~/usr/src/notes/samples/exrc       ~/usr/local/dotfiles/exrc
	mkdir local/gnome
	cp ~/usr/src/notes/samples/local-work.sh  ~/usr/local/gnome/work.sh
	cp ~/bin/uncrustify  ~/usr/opt/
	cp -r ~/bin/namebench-1.3.1  ~/usr/opt/
	mkdir -p ~/usr/opt/remind
	cp ~/bin/remind-appt.png         ~/usr/opt/remind
	cp ~/bin/remind-am.sh            ~/usr/opt/remind
	cp ~/bin/start-remind-daemon.sh  ~/usr/opt/remind
	~/usr/src/update.sh

## Directory Structure

The following directory structure is built in the account's home directory:

~/usr
: The root directory for everything else.

~/usr/src
: This is the directory that is under source control. It contains the tools and the scripts which install the tools. The `src/update.sh` script should be maintained to rebuild only the common configuration. The local configuration is built by the `src/update.sh` script.

~/usr/local
: This directory is initially created by the `install.sh` script, but is expected to be set up and maintained in order to do custom configuration of the target host. It holds local variations on items from `src` and `opt`. The `local/update.sh` script should be maintained to rebuild the local configuration from the common configuration.

~/usr/bin
: The bin folder is created dynamically from items in the `src` and `local` folders. It's deleted and recreated whenever the `update.sh` script is run.

~/usr/opt
: This directory is created by the `install.sh` script. This is a place to store tools that will only be used locally. These tools are outside of the source control for the common configuration, so they can have their own source control.

## Sources

The `src` folder is the only one in `~/usr` which is under source control by a remote repo, although `opt` is intended to hold controlled projects as well. It contains several files and subfolders which may or may not be installed, depending on system configuration:

* README.md - this file
* dotfiles - dotfiles-folder files, e.g. '.login', '.bashrc'
* gnome - gnome tools, only useful when the target host runs Linux with gnome
* mac - MacOS tools, only useful when the target host runs MacOS
* gcc - gcc tools, only useful when gcc is installed
* java - Java things, only useful when a JRE (JDK?) is installed
* perl - tools written in perl
* python - tools written in python
* notes - plain text notes
* other - anything else
* update.sh - the config-update script, should be run after mods to ~/usr/src
* install.sh - the initial install script, should only be run once

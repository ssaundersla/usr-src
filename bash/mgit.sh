#!/usr/bin/env bash --login

function main()
{
  if [ $# -eq 0 ]; then
    echo "mgit 'Multi-git', e.g.:"
    echo "mgit rev-parse --abbrev-ref HEAD"
    echo ""
    echo "mgit runs whatever git command you enter on all of the site"
    echo "directories under the current working directory. mgit uses MacOS"
    echo "'attributes' to identify repo directories:"
    echo ""
    echo "    Use 'xattr -w' to 'write' an attribute to a file"
    echo "       e.g.: xattr -w 'local.mooreboeck.site' 'y' DIR"
    echo "    "
    echo "    Use 'xattr -d' to 'delete' an attribute from a file"
    echo "       e.g.: xattr -d 'local.mooreboeck.site' DIR"
  else
    local colored=`tput setaf 5`
    local bold=`tput bold`
    local reset=`tput sgr0`
    local this_ifs="$IFS"
    IFS=$'\n' dir_element=($(xattr -l *))
    export LC_ALL=C
    for dir_element in "${dir_element[@]}"
    do
      the_subdirectory="$(echo "$dir_element" | cut -d ":" -f1)"
      if [ -d $the_subdirectory ]; then
        the_attr="$(echo "$dir_element" | cut -d ":" -f2)"
        if [ "$the_attr" = " ${MULTIGIT_DIR_ATTR}" ]; then
          if [ -d "${the_subdirectory}/.git" ]; then
            command cd "${the_subdirectory}"
            echo -e "\n${bold}${colored}${the_subdirectory}${reset}"
            git $@
            cd ..
          fi
        fi
      fi
    done
    IFS="${this_ifs}"
  fi
}

main "$@"

#!/usr/bin/env bash --login

function main()
{
    local colored=`tput setaf 5`
    local bold=`tput bold`
    local reset=`tput sgr0`
    local this_ifs="$IFS"
    IFS=$'\n' dir_element=($(xattr -l *))
    export LC_ALL=C
    for dir_element in "${dir_element[@]}"
    do
      the_subdirectory="$(echo "$dir_element" | cut -d ":" -f1)"
      if [ -d $the_subdirectory ]; then
        the_attr="$(echo "$dir_element" | cut -d ":" -f2)"
        if [ "$the_attr" = " local.mooreboeck.site" ]; then
          if [ -d "${the_subdirectory}/.git" ]; then
            command cd "${the_subdirectory}"
            echo -e "\n${bold}${colored}${the_subdirectory}${reset}"

            # Clean the repo
            git prune
            git remote prune origin
            git gc

            # Get the target branch
            git rev-parse --verify production > /dev/null 2>&1
            if [ "$?" = "0" ]; then
                target_branch="production"
            else
                target_branch="master"
            fi

            # List branches that can be removed
            local sha_target_branch="$(git show -s --format='%H' ${target_branch})"
            local sha_target_branch="$(git show -s --format='%H' ${target_branch})"
            printf "\n${BOLD}The following branches are merged into ${target_branch} and can be deleted:${RESET}\n\n"
            for branch in $(git branch --format "%(refname:short)" --merged ${target_branch}); do
              local sha_branch="$(git show -s --format='%H' ${branch})"
              if [ "${sha_branch}" != "${sha_target_branch}" ]; then
                if [ "${branch}" != "master" ]; then
                  printf "git branch -d %-45s # %s\n" "${branch}" "${branch}"
                fi
              fi
            done

            # There's no place like home
            cd ..
          fi
        fi
      fi
    done
    IFS="${this_ifs}"
}

main "$@"

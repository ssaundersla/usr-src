#!/usr/bin/env bash

################################################################################
#
function list_directories()
{
	local tld='local.mooreboeck'
	local the_key=$1
	local the_value=$2
	if [ -z "${the_key}" ]; then
		# No key given; list all keys
		list_directories_for_pattern "${tld}"
	else
		if [ -z "${the_value}" ]; then
			# User has entered a key; list just that key
			list_directories_for_pattern "${tld}.${the_key}"
		else
			# User has entered a key and a value; list just those
			list_directories_for_pattern "${tld}.${the_key}: ${the_value}"
		fi
	fi
}

################################################################################
#
function list_directories_for_pattern()
{
	local the_pattern=$1

    IFS=$'\n' dir_element=($(xattr -l *))
    for dir_element in "${dir_element[@]}"
    do
		if echo "$dir_element" | grep -q "${the_pattern}"; then
		    echo "$dir_element"
		fi
    done
    IFS="${this_ifs}"
}

################################################################################
#
function mark_directories()
{
	local tld='local.mooreboeck'
	local the_key=$1
	local the_value=$2
	shift
	shift

	if [ -z "${the_key}" ]; then
		# No key given; error
		echo "Error: Missing key (-k ???)"
		return
	else
		if [ -z "${the_value}" ]; then
			# No value given; error
			echo "Error: Missing value (-v ???)"
			return
		else
			# User has entered a key and a value; list just those
			list_directories_for_pattern "${tld}.${the_key}: ${the_value}"
		fi
	fi

	while [[ $# -gt 0 ]]
	do
		file="$1"
		shift
		echo "Marking '${file}' as '${tld}.${the_key}=${the_value}'"
		xattr -w "${tld}.${the_key}" "${the_value}" "${file}"
	done
}

################################################################################
#
function unmark_directories()
{
	local tld='local.mooreboeck'
	local the_key=$1
	shift

	if [ -z "${the_key}" ]; then
		echo "Error: Missing key (-k ???)"
		return
	fi

	if [ $# -le 0 ]; then
		echo "Error: missing file list, no files to unmark"
		return
	fi

	while [[ $# -gt 0 ]]
	do
		file="$1"
		shift
		echo "Removing mark '${tld}.${the_key}' from '${file}'"
		xattr -d "${tld}.${the_key}" "${file}"
	done
}

################################################################################
#
usage="$(basename "$0") [-ldkvh] [FILES]

Mark file(s) and directory(s) with xattrs

where:
    -k  key for xattr
    -v  value for xattr
    -l  list directories with 'local.mooreboeck' marks
    -d  delete marks on files
    -h  show this help text

Examples:

    mrk -k progress -v blocked *-waiting.txt
        Mark all files ending in '-waiting.txt' with 'progress=blocked'

    mrk -l
        List all marked files with their marks

    mrk -l -k progress
        List all files marked 'progress' with their marks

    mrk -l -k progress -v blocked
        List all files marked 'progress=blocked' with their marks

    mrk -d progress *-working.txt
        Remove marks from all files ending in '-working.txt'
"

cmd_list='n'
cmd_delete='d'
the_key=''
the_value=''
while getopts ':ldk:v:h' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    k) the_key="$OPTARG"
       ;;
    v) the_value="$OPTARG"
       ;;
    l) cmd_list='yes'
       ;;
    d) cmd_delete='yes'
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

if [ 'yes' = "${cmd_list}" ]; then
	list_directories "${the_key}" "${the_value}"
	exit 0
fi

if [ 'yes' = "${cmd_delete}" ]; then
	unmark_directories "${the_key}" "$@"
	exit 0
fi

mark_directories "${the_key}" "${the_value}" "$@"


# This is the 'xattr' command -- it turns out to be a Python script
#
#   cat /usr/bin/xattr
#   #!/usr/bin/python
#
#   import sys, os
#   import glob, re
#
#   partA = """\
#   python version %d.%d.%d can't run %s.  Try the alternative(s):
#
#   """
#   partB = """
#   Run "man python" for more information about multiple version support in
#   Mac OS X.
#   """
#
#   sys.stderr.write(partA % (sys.version_info[:3] + (sys.argv[0],)))
#
#   dir, base = os.path.split(sys.argv[0])
#   specialcase = (base == 'python-config')
#   if specialcase:
#       pat = "python*-config"
#   else:
#       pat = base + '*'
#   g = glob.glob(os.path.join(dir, pat))
#   # match a single digit, dot and possibly multiple digits, because we might
#   # have 2to32.6, where the program is 2to3 and the version is 2.6.
#   vpat = re.compile("\d\.\d+")
#   n = 0
#   for i in g:
#       vers = vpat.search(i)
#       if vers is None:
#   	continue
#       sys.stderr.write("%s (uses python %s)\n" % (i, i[vers.start():vers.end()]))
#       n = 1
#   if n == 0:
#       sys.stderr.write("(Error: no alternatives found)\n")
#
#   sys.stderr.write(partB)
#   sys.exit(1)

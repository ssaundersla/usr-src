# This file can be included in  ~/.gitconfig, e.g.:
#
#	[include]
#	    path = ~/usr/src/dotfiles/gitconfig
#
# The ~/.gitconfig file should look something like this:
#
#	[include]
#	    path = ~/usr/src/dotfiles/gitconfig
#	[user]
#		name = Stephen Saunders\n
#		email = stephen.saunders.la@gmail.com
#	[diff]
#		tool = p4merge
#		prompt = false
#	[merge]
#		tool = kdiff3
#
[core]
	excludesfile = ~/.gitignore
	pager = cat
	autocrlf = input
[alias]
	st = status
	a = add . --all
	b = branch
	c = commit
	cm = commit -m
	co = checkout
	mkb = checkout -b
	rmb = branch -D
	mas = checkout master
	qs = !git commit -m\"WIP: Quick save $(date +'%Y-%m-%d %H:%M:%S')\"
	qst = !git commit -m\"Test run success at $(date +'%Y-%m-%d %H:%M:%S')\"
	qas = !git add . && git commit -a -m\"WIP: Quick save $(date +'%Y-%m-%d %H:%M:%S')\"
	qasp = !git add . && git commit -a -m\"WIP: Quick save $(date +'%Y-%m-%d %H:%M:%S')\" && git push
	hi = log --date-order --reverse --pretty=format:\"%C(cyan)%h%C(reset) %s %C(yellow)(%ar)%C(reset)\"
	hil = log --date-order --date=short --reverse --pretty=format:\"%C(cyan)%h %C(green)%ar: %ad %C(magenta)%an%C(reset) %s\"
	cl = log --date-order --reverse --pretty=format:\"%x23%x23%x23 %B%n    -- %an <%ae>%n       %aD (%ar)%n       Rev %h (%H)%n\"
	clf = log --name-only --date-order --reverse --pretty=format:\"-------------------------------------------------%nRevision %H%n%n%B%n  -- %an <%ae>%n     %aD (%ar)%n%nFiles Changed:%n\"
	h5 = log -5 --date-order --reverse --pretty=format:\"%C(cyan)%h%C(reset) %s %C(yellow)(%ar)%C(reset)\"
	c5  = log -5 --date-order --reverse --pretty=format:\"%x23%x23%x23 %C(green)%B%C(reset)%n    -- %an <%ae>%n       %aD (%ar)%n       Rev %C(cyan)%h%C(reset) (%H)%n\"
	cf5  = log -5 --name-only --date-order --reverse --pretty=format:\"%C(cyan)%h%C(reset) %C(green)%s%C(reset) %C(yellow)%ar%C(reset)%n\"
	lb = remote get-url origin
	# lb = !git remote show origin && git branch -v
	la = "!git config -l | grep alias | cut -c 7-"
	lrm = "log --diff-filter=D --summary"
	difff = "diff --ignore-space-at-eol -b -w --ignore-blank-lines"
	#
	# List some info for a tag, e.g. git cat-file -p mytag
	#   cat-file -p
	# Un-add a file to the index, e.g. git reset HEAD oopsfile.txt
	#   reset HEAD
	# Roll back changes to a specific file, e.g. git co -- damagedfile.txt
	#   checkout --
	# Roll back the whole working copy, e.g. git reset --head HEAD
	#   reset --hard HEAD
	# Generate a full description of the current repo state
	#   describe --long --tags --dirty --always
[push]
	default = tracking
#
# Merge tool options
#
[mergetool "Sublime Merge"]
	cmd = smerge mergetool \"$BASE\" \"$REMOTE\" \"$LOCAL\" -o \"$MERGED\"
[mergetool "kdiff3"]
	cmd = /Applications/kdiff3.app/Contents/MacOS/kdiff3 --cs "ShowInfoDialogs=0" --L2 Mine --L3 Merging-in $BASE $LOCAL $REMOTE --output $MERGED
	keepTemporaries = false
	trustExitCode = false
	keepBackup = false
[mergetool "p4merge"]
	cmd = /Applications/p4merge.app/Contents/Resources/launchp4merge "$BASE" "$LOCAL" "$REMOTE" "$MERGED"
	keepTemporaries = false
	trustExitCode = false
	keepBackup = false
[mergetool "sourcetree"]
	cmd = /Applications/SourceTree.app/Contents/Resources/opendiff-w.sh \"$LOCAL\" \"$REMOTE\" -ancestor \"$BASE\" -merge \"$MERGED\"
	trustExitCode = true
[mergetool "diffmerge"]
	cmd = /Applications/DiffMerge.app/Contents/Resources/diffmerge.sh --merge --result=\"$MERGED\" \"$LOCAL\" \"$BASE\" \"$REMOTE\"
	trustexitcode = false
#
# Diff tool options
#
[difftool "p4merge"]
	cmd = /Applications/p4merge.app/Contents/Resources/launchp4merge "$LOCAL" "$REMOTE"
[difftool "opendiff"]
	cmd = /Applications/Xcode.app/Contents/Developer/usr/bin/opendiff "$LOCAL" "$REMOTE"
[difftool "opendiff_b"]
	cmd = /Applications/Xcode-Beta.app/Contents/Developer/usr/bin/opendiff "$LOCAL" "$REMOTE"
[difftool "sourcetree"]
	cmd = opendiff \"$LOCAL\" \"$REMOTE\"
	path =
[difftool "diffmerge"]
	cmd = /Applications/DiffMerge.app/Contents/Resources/diffmerge.sh \"$LOCAL\" \"$REMOTE\"
[difftool]
	prompt = false

#! /usr/bin/env bash
#
# Tests for the token @disable-push and prevents the push operation if found.
#
# Your .git/hooks/pre-push file should look something like:
#
#	#!/bin/sh
#	remote="$1"
#	url="$2"
#	while read local_ref local_sha remote_ref remote_sha
#	do
#		status=0
#		#
#		#	Test for 'WIP' commits
#		#
#		${BUILD_HOME}/git-hooks/wip.sh "${remote}" "${url}" "${local_ref}" "${local_sha}" "${remote_ref}" "${remote_sha}"
#		if [ $? -ne 0 ] ; then status=1 ; fi
#	done
#
#	exit $(expr $status)
#
remote="$1"
url="$2"
local_ref="$3"
local_sha="$4"
remote_ref="$5"
remote_sha="$6"

FLAG_REGEX="(@disable-push|@disable-checkin)"
z40=0000000000000000000000000000000000000000

if [ "$local_sha" = $z40 ]
then
	# Handle delete
	:
else
	if [ "$remote_sha" = $z40 ]
	then
		# New branch, examine all commits
		range="$local_sha"
	else
		# Update to existing branch, examine new commits
		range="$remote_sha..$local_sha"
	fi
	# List the files that have changed and grep each one
	changed_files=`git diff --name-only "$range"`
	status=0
    for path in $changed_files; do
    	grep -EH "${FLAG_REGEX}" "${path}"
    	# grep exits with a non-zero status if nothing is found
    	# so a 0 status means a disbleflag was found
		if [ $? -eq 0 ] ; then status=1 ; fi
    done
fi

	if [ $status -eq 0 ]
	then
		exit 0
	else
		echo >&2 "ERROR: disable-push flags found in one or more changed files"
		exit 1
	fi

exit 1

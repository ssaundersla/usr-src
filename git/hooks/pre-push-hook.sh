#!/bin/sh
remote="$1"
url="$2"
while read local_ref local_sha remote_ref remote_sha
do
	status=0
	#
	#	Test for WIP commits ('WIP' in commit messages)
	#
	${HOME}/usr/src/git/hooks/wip.sh "${remote}" "${url}" "${local_ref}" "${local_sha}" "${remote_ref}" "${remote_sha}"
	if [ $? -ne 0 ] ; then status=1 ; fi
	#
	#	Test for flags (in changed files)
	#
	${HOME}/usr/src/git/hooks/disable-push.sh "${remote}" "${url}" "${local_ref}" "${local_sha}" "${remote_ref}" "${remote_sha}"
	if [ $? -ne 0 ] ; then status=1 ; fi
done
exit $(expr $status)

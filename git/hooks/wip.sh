#! /usr/bin/env bash
#
# Tests for 'WIP' in commit messages and prevents the push operation if found.
#
# Your .git/hooks/pre-push file should look something like:
#
#	#!/bin/sh
#	remote="$1"
#	url="$2"
#	while read local_ref local_sha remote_ref remote_sha
#	do
#		status=0
#		#
#		#	Test for 'WIP' commits
#		#
#		${BUILD_HOME}/git-hooks/wip.sh "${remote}" "${url}" "${local_ref}" "${local_sha}" "${remote_ref}" "${remote_sha}"
#		if [ $? -ne 0 ] ; then status=1 ; fi
#	done
#
#	exit $(expr $status)
#
remote="$1"
url="$2"
local_ref="$3"
local_sha="$4"
remote_ref="$5"
remote_sha="$6"

z40=0000000000000000000000000000000000000000

if [ "$local_sha" = $z40 ]
then
	# Handle delete
	:
else
	if [ "$remote_sha" = $z40 ]
	then
		# New branch, examine all commits
		range="$local_sha"
	else
		# Update to existing branch, examine new commits
		range="$remote_sha..$local_sha"
	fi
	# Check for WIP commit
	commit=`git rev-list -n 1 --grep '^WIP' "$range"`
	if [ -n "$commit" ]
	then
		# The git log command does not append a final newline to its
		# output, hence the increment to 'count'
		count=`git log --pretty=format:'%h' "$range" | wc -l`
		count=$(expr $count + 1)
		#
		echo >&2 "ERROR: Found WIP commit in branch. Commits have not been pushed."
		echo >&2 "Please squash the work-in-progress commits with:"
		echo >&2 ""
		if [ $count -eq 1 ]
		then
			echo >&2 "    git commit --amend"
		else
			echo >&2 "    git rebase -i HEAD~$count"
		fi
		# echo >&2 "    git rebase -i HEAD~$count"
		echo >&2 ""
		#
		# I find the interactive rebase a little counter-intuitive. Rather than
		# re-thinking it every time I use it, print a quick reminder.
		#
		echo >&2 "Remember to squash the last commit to go into a rebased revision, and"
		echo >&2 "to *not* squash the first commit. You'll have a chance to re-word the"
		echo >&2 "commit message after you exit the rebasing editor session."
		echo >&2 ""
		exit 1
	fi
fi

exit 0
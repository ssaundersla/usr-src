#! /usr/bin/python

import os
import re
import glob
import datetime
import shutil
import subprocess

logFile="/home/steve/usr/bin/caja-scripts/logs/manifest.log"
dateRegex = re.compile("(.*)([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})([^0-9])(.*)")

def Main():

	caja = Caja()
	mainDir  = caja.mainDir()
	fileList = caja.mainFileList()+caja.otherFileList()

	logList=[]

	if len(fileList) < 1:
		fileList = glob.glob(os.path.join(mainDir,"*"))

	maxlen=0
	for path in fileList:
		(dirName,fileName) = os.path.split(path)
		n = len(fileName)
		maxlen = n if n > maxlen else maxlen
	fmt = "* %-{maxlen}s - ".replace("{maxlen}",str(maxlen))

	outFile=GetUniqueNameFor(mainDir,"README.md")
	try:
		fp = open(outFile,"w")
		for path in fileList:
			(dirName,fileName) = os.path.split(path)
			line = fmt % fileName
			fp.write(line+'\n')
	except:
		ShowInfo("Error writing "+outFile)
	finally:
		fp.close()

	# Log(logList)


#===========================================================================

def GetUniqueNameFor(dirName,baseName):
	existingFileSet = GetExistingFileNamesIn(dirName)
	newName = GetUniqueNameBasedOn(existingFileSet,baseName,1)
	return os.path.join(dirName,newName)

#===========================================================================

def GetExistingFileNamesIn(dirName):
	existingFileSet = set()
	existingPathList = glob.glob(os.path.join(dirName,"*"))
	for existingPath in existingPathList:
		(d,fn) = os.path.split(existingPath)
		existingFileSet.add(fn)
	return existingFileSet

#===========================================================================

def GetUniqueNameBasedOn(existingFileSet,in_filename,n):
	if in_filename in existingFileSet:
		fnList = in_filename.split(".")
		if len(fnList)>1:
			filename = ".".join(fnList[0:-1])
			ext="."+fnList[-1]
		else:
			filename = in_filename
			ext=""
		proposedNewName = filename + '.{n}' + ext
		proposedNewName = proposedNewName.replace( "{n}", str(n) )
		if proposedNewName in existingFileSet:
			return GetUniqueNameBasedOn(in_filename,n+1)
		return proposedNewName
	else:
		return in_filename

#===========================================================================

class Caja():

	def dummyInitForTesting(self):

		# Get data from Caja:
		mainDir       = "file:///home/steve/Documents/CurrentTickets"
		otherDir      = "file:///home/steve/Desktop"

		# Caja is weird:
		mainFileList = [
			# "/home/steve/Documents/CurrentTickets/st-lucia-110710",
			# "/home/steve/Documents/CurrentTickets/Diakonessenhuis-747SUR",
			# "/home/steve/Documents/CurrentTickets/tristate",
			# "/home/steve/Documents/CurrentTickets/Veres-539MI",
			# "/home/steve/Documents/CurrentTickets/PIL-reflab",
			# "/home/steve/Documents/CurrentTickets/Central Diag Lab-118BM",
			""
		]

		otherFileList= [
		# 	"/home/steve/Desktop/t1/t2/notes.md",
		# 	"/home/steve/Desktop/t1/t3/notes-2013-08-28.md",
		# 	"/home/steve/Desktop/t1/t4/notes.md",
			""
		]

		# Finally:
		self.ivar_mainDir         = mainDir[7:]
		self.ivar_otherDir        = otherDir[7:]
		self.ivar_mainFileList    = mainFileList[0:-1]
		self.ivar_otherFileList   = otherFileList[0:-1]


	def __init__(self):

		# Get data from Caja:
		mainDir       = os.getenv("CAJA_SCRIPT_CURRENT_URI")
		otherDir      = os.getenv("CAJA_SCRIPT_NEXT_PANE_CURRENT_URI")
		mainFileList  = os.getenv("CAJA_SCRIPT_SELECTED_FILE_PATHS")
		otherFileList = os.getenv("CAJA_SCRIPT_NEXT_PANE_SELECTED_FILE_PATHS")

		# Caja is weird:
		mainFileString  = "".join(mainFileList)
		mainFileList    = mainFileString.split("\n")
		otherFileString = "".join(otherFileList)
		otherFileList   = otherFileString.split("\n")

		# Finally:
		self.ivar_mainDir         = mainDir[7:]
		self.ivar_otherDir        = otherDir[7:]
		self.ivar_mainFileList    = mainFileList[0:-1]
		self.ivar_otherFileList   = otherFileList[0:-1]

	def mainDir(self):
		return self.ivar_mainDir

	def otherDir(self):
		return self.ivar_otherDir

	def mainFileList(self):
		return self.ivar_mainFileList

	def otherFileList(self):
		return self.ivar_otherFileList

#===========================================================================

def ShowInfo(theString):
	cmd = [
		'matedialog',
		'--warning',
		'--title=Error',
		'--width=300',
		'--height=200',
	]
	cmd.append("--text="+theString)
	theString = subprocess.check_output(cmd)

#===========================================================================

def Log(lineList):
	try:
		fp = open(logFile,"w")
		for line in lineList:
			fp.write(line+'\n')
	except:
		print "Error writing "+"logFile"
		traceback.print_exc()
	finally:
		fp.close()

#===========================================================================

def VetWithUser(theString):
	cmd = [
		'matedialog',
		'--entry',
		'--width=300',
		'--height=200',
		'--title=Duplicating File',
		'--text=Name of duplicated file',
	]
	cmd.append("--entry-text="+theString)
	theString = subprocess.check_output(cmd)
	tList = theString.split("\n")
	if len(tList)>1:
		theString = tList[0]
	return theString

#===========================================================================

if __name__ == '__main__':
	Main()

# ln -sf /home/steve/usr/bin/caja-scripts/create-manifest.py '/home/steve/.config/caja/scripts/Create Manifest'

#! /usr/bin/python
#
#===========================================================================
#	Problems:
#
# If the script fails, check the /home/steve/usr/logs/local/tex.log file to see
# what happened. It will probably be necessary to run the shell file which
# this script creates.

import os
import re
import glob
import datetime
import shutil
import subprocess
import random
import stat

logFile="/home/steve/usr/local/logs/tex.log"
tmpDirPrefix="/tmp/create-tex-from-md-py-"
shellFileContent="""
#!/bin/sh
cd {tmpDir}
/home/steve/usr/bin/multimarkdown --to=latex -b {mainFileBase}.md
/opt/texlive/2011/bin/x86_64-linux/pdflatex {mainFileBase}.tex
/opt/texlive/2011/bin/x86_64-linux/pdflatex {mainFileBase}.tex
/opt/texlive/2011/bin/x86_64-linux/pdflatex {mainFileBase}.tex
mv {mainFileBase}.pdf {finalPdfFilePath}
"""[1:]

#===========================================================================
## Installation:
##
## A caja script is installed in caja (the Nautilus successor) by placing
## it in the `~/.config/caja/scripts` folder. Ergo, the following command
## 'installs' this script:
##
##     ln -sf /home/steve/usr/src/gnome/caja-scripts/create-tex-from-md.py '/home/steve/.config/caja/scripts/Generate PDF from MD'
#
def Main():

	log = Logger(logFile)
	caja = Caja()
	log.log("caja object built")

	# this is correct most of the time
	mainDir  = caja.mainDir()

	# The user can invoke this script in one of four ways:

	if caja.userClickedFolderBg():
		log.log("userClickedFolderBg")
		mainFilePath = caja.mainDir()
		mdFileList = glob.glob(os.path.join(mainDir,"*.md"))

	if caja.userSelectedFolder():
		log.log("userSelectedFolder")
		mainDir = caja.mainFileList()[0]
		mainFilePath = caja.mainFileList()[0]
		mdFileList = glob.glob(os.path.join(mainDir,"*.md"))

	if caja.userSelectedOneFile():
		log.log("userSelectedOneFile")
		mainFilePath = caja.mainFileList()[0]
		mdFileList = caja.mainFileList()

	if caja.userSelectedMultipleFiles():
		log.log("userSelectedMultipleFiles")
		mainFilePath = caja.mainDir()
		mdFileList = caja.mainFileList()

	# basename of the main file
	t = os.path.splitext(os.path.basename(mainFilePath))
	mainFileBase = t[0]
	log.log("mainDir="+mainDir)
	log.log("mainFileBase="+mainFileBase)
	log.log("mdFileList="+str(mdFileList))

	# title of article
	title = GenerateTitle(mainFileBase)
	log.log("title="+title)

	# PDF file
	ugen = UniquePathGenerator()
	ugen.setFolder(mainDir)
	ugen.setBase(mainFileBase+".pdf")
	finalPdfFilePath = ugen.generate()
	log.log("finalPdfFilePath="+finalPdfFilePath)

	# temporary files
	tmpDir = tmpDirPrefix + str(random.randint(100000,999999))
	tmpMdPath = os.path.join(tmpDir,mainFileBase+".md")
	tmpShPath = os.path.join(tmpDir,mainFileBase+".sh")
	log.log("tmpDir="+tmpDir)
	log.log("tmpMdPath="+tmpMdPath)
	log.log("tmpShPath="+tmpShPath)

	# Create the tmp folder
	os.mkdir(tmpDir)

	# Create the shell script
	content = shellFileContent
	content = content.replace("{tmpDir}",tmpDir)
	content = content.replace("{mainFileBase}",mainFileBase)
	content = content.replace("{finalPdfFilePath}",finalPdfFilePath)
	try:
		fp = open(tmpShPath,"w")
		fp.write(content)
	except:
		ShowInfo("Error writing "+tmpShPath)
	finally:
		fp.close()
	os.chmod(tmpShPath,0777)
	log.log("shell file created: "+tmpShPath)

	# Load the MD files
	content = latexHeader
	content = content.replace("{title}",title)
	for f in mdFileList:
		content += "\n\n"
		if f.endswith(".md"):
			content += LoadContents(f)
			log.log(f+" read for "+tmpMdPath)
		if f.endswith(".png"):
			log.log("TODO "+f+" copied to "+tmpMdPath)

	# Create the tmp MD file
	try:
		fp = open(tmpMdPath,"w")
		fp.write(content)
	except:
		ShowInfo("Error writing "+tmpMdPath)
	finally:
		fp.close()
	os.chmod(tmpMdPath,0666)
	log.log("temporary MD file created: "+tmpMdPath)


	#---------------------------------------------------------------------------
	#
	#	test only
	#
	#---------------------------------------------------------------------------
	# os.exit()

	# Run the script

	log.log("running shell file: "+tmpShPath)
	os.system(tmpShPath)
	log.log("shell file "+tmpShPath+" completed")
	log.close()


#===========================================================================
## Converts a file name to a title
##
## @param inString the file name
## @returns the title
#
def GenerateTitle(inString):
	title = "Unknown"

	try:
		title = ConvertCamelToTitle(inString)
	except:
		title = None
	if title != None:
		return title

	try:
		title = ConvertSnakeToTitle(inString,'-')
	except:
		title = None
	if title != None:
		return title

	try:
		title = ConvertSnakeToTitle(inString,'_')
	except:
		title = None
	if title != None:
		return title

	return title

#===========================================================================
## Converts a snake-cased string (e.g. 'the_tale_of_two_cities') to a space-
## separated string suitable for a title of a book or article.
##
## @param inString the snake-cased string
## @returns the title
#
def ConvertSnakeToTitle(inString,separator):
	if not separator in inString:
		return None
	cList = list(inString)
	theWord = None
	wordList = []
	for c in cList:
		if c == separator:
			if theWord != None:
				wordList.append(theWord.capitalize())
			theWord = ""
		else:
			if theWord != None:
				theWord = theWord + str(c)
			else:
				theWord = str(c)
	wordList.append(theWord.capitalize())
	return " ".join(wordList)

#===========================================================================
## Converts a camel-cased string (e.g. 'TheTaleOfTwoCities') to a space-
## separated string suitable for a title of a book or article. The title
## begins with the first upper-case letter, so an input string can be
## prefixed with characters that will not appear in the title.
##
## @param camel the camel-cased string
## @returns the title
#
def ConvertCamelToTitle(camel):
	uList = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
	cList = list(camel)
	theWord = None
	wordList = []
	wordLength = 0
	nWords = 0
	for c in cList:
		if c in uList:
			if theWord != None:
				wordList.append(theWord)
				wordLength += len(theWord)
				nWords += 1
			theWord = ""
		if theWord != None:
			theWord = theWord + str(c)
	wordList.append(theWord)
	wordLength += len(theWord)
	nWords += 1
	ratio = float(wordLength)/nWords
	if ratio < 2.0:
		return None
	return " ".join(wordList)

#=============================================================================
## Loads the contents of the file into a string and returns the string.
## @param inFile the file to load
## @returns the contents of the inFile file as a single string
#
def LoadContents(inFile):
	content=""
	try:
		fp = open(inFile)
		for line in fp:
			content += line
	except:
		ShowInfo("Error reading "+inFile)
	finally:
		fp.close()
	return content

#===========================================================================
## Interface object for Caja.
#
class Caja():

	def dummyInitForTesting(self):

		# Get data from Caja:
		mainDir       = "file:///home/steve/dev/J2EE/schuybase/src/docs/coding-notes"
		otherDir      = "file:///home/steve/Desktop"

		# Caja is weird:
		mainFileList = [
			"/home/steve/dev/J2EE/schuybase/src/docs/coding-notes/file-organization.md",
			"/home/steve/dev/J2EE/schuybase/src/docs/coding-notes/jdbc-tests.md",
			"/home/steve/dev/J2EE/schuybase/src/docs/coding-notes/log4j.md",
			"/home/steve/dev/J2EE/schuybase/src/docs/coding-notes/logins.md",
			""
		]
		otherFileList= [
			# "/home/steve/Desktop/t1/t2/notes.md",
			# "/home/steve/Desktop/t1/t3/notes-2013-08-28.md",
			# "/home/steve/Desktop/t1/t4/notes.md",
			""
		]

		# Finally:
		self.ivar_mainDir         = mainDir[7:]
		self.ivar_otherDir        = otherDir[7:]
		self.ivar_mainFileList    = mainFileList[0:-1]
		self.ivar_otherFileList   = otherFileList[0:-1]


	def __init__(self):

		# Get data from Caja:
		mainDir       = os.getenv("CAJA_SCRIPT_CURRENT_URI")
		otherDir      = os.getenv("CAJA_SCRIPT_NEXT_PANE_CURRENT_URI")
		mainFileList  = os.getenv("CAJA_SCRIPT_SELECTED_FILE_PATHS")
		otherFileList = os.getenv("CAJA_SCRIPT_NEXT_PANE_SELECTED_FILE_PATHS")

		# Caja is weird:
		mainFileString  = "".join(mainFileList)
		mainFileList    = mainFileString.split("\n")
		otherFileString = "".join(otherFileList)
		otherFileList   = otherFileString.split("\n")

		# Finally:
		self.ivar_mainDir         = mainDir[7:]
		self.ivar_otherDir        = otherDir[7:]
		self.ivar_mainFileList    = mainFileList[0:-1]
		self.ivar_otherFileList   = otherFileList[0:-1]

		# Try to figure out what the user did
		self.isUserClickedFolderBg = False
		self.isUserSelectedFolder = False
		self.isUserSelectedOneFile = False
		self.isUserSelectedMultipleFiles = False
		self.whatDidTheUserDo()

	def whatDidTheUserDo(self):
		if len(self.ivar_mainFileList) < 1:
			self.isUserClickedFolderBg = True
			return

		if len(self.ivar_mainFileList) == 1:
			if os.path.isdir(self.ivar_mainFileList[0]):
				self.isUserSelectedFolder = True
			else:
				self.isUserSelectedOneFile = True
			return

		self.isUserSelectedMultipleFiles = True
		return

	def mainDir(self):
		return self.ivar_mainDir

	def otherDir(self):
		return self.ivar_otherDir

	def mainFileList(self):
		return self.ivar_mainFileList

	def otherFileList(self):
		return self.ivar_otherFileList

	def mainDir(self):
		return self.ivar_mainDir

	def userClickedFolderBg(self):
		return self.isUserClickedFolderBg

	def userSelectedFolder(self):
		return self.isUserSelectedFolder

	def userSelectedOneFile(self):
		return self.isUserSelectedOneFile

	def userSelectedMultipleFiles(self):
		return self.isUserSelectedMultipleFiles

#===========================================================================
## Utility for showing an error message to the Caja user.
#
def ShowInfo(theString):
	cmd = [
		'matedialog',
		'--warning',
		'--title=Error',
		'--width=300',
		'--height=200',
	]
	cmd.append("--text="+theString)
	theString = subprocess.check_output(cmd)

#===========================================================================
## Generates a unique name for a file in a folder. An object of this class
## takes a path to an existing folder which may or may not already contain
## files, and a candidate name for a file to be created in the folder. It
## returns a file path which will not overwrite another file in the folder.
#
class UniquePathGenerator():

	def __init__(self):
		self.folder = None
		self.baseName = None
		self.ext = None
		self.existingFileSet = set()

	def setFolder(self,p):
		self.folder = p

	def setBase(self,p):
		# self.baseName = p
		(self.baseName,self.ext) = os.path.splitext(p)

	def generate(self):
		if len(self.existingFileSet) < 1:
			self.initExistingFileSet()

		newName = self.baseName + self.ext
		if newName not in self.existingFileSet:
			self.existingFileSet.add(newName)
			return os.path.join(self.folder,newName)

		newName = self.getUniqueNameBasedOn(1)
		self.existingFileSet.add(newName)
		return os.path.join(self.folder,newName)

	def initExistingFileSet(self):
		existingPathList = glob.glob(os.path.join(self.folder,"*"))
		for existingPath in existingPathList:
			(d,newName) = os.path.split(existingPath)
			self.existingFileSet.add(newName)

	def getUniqueNameBasedOn(self,n):
		newName = self.baseName + '.' + str(n) + self.ext
		if newName in self.existingFileSet:
			return self.getUniqueNameBasedOn(n+1)
		return newName

#===========================================================================
## A simple logging object. A logger is created with a log file to write
## to, and then the log() method is called on it to log messages to the
## file. Should be closed with the close() method.
#
class Logger():

	def __init__(self, logFile):
		self.logFile = logFile
		try:
			self.fp = open(self.logFile,"w")
		except:
			ShowInfo( "Cannot open log file "+logFile )
			if hasattr(self, 'fp'):
				self.fp.close()

	def __repr__(self):
		return self.logFile

	def log(self,line):
		if hasattr(self, 'fp'):
			self.fp.write(line+'\n')

	def close(self):
		if hasattr(self, 'fp'):
			self.fp.close()

#===========================================================================
## Not used at present. Might be useful someday.
#
def VetWithUser():
	cmd = [
		'matedialog',
		'--width=300',
		'--height=200',
		'--list',
		'--checklist',
		'--title=LaTeX Options',
		'--column "Use"',
		'--column "Option"',
		'TRUE Apples TRUE Oranges FALSE Pears FALSE Toothpaste',
	]
	theString = subprocess.check_output(cmd)
	# tList = theString.split("|")
	# if len(tList)>1:
	# 	theString = tList[0]
	return theString

#===========================================================================
latexHeader="""
latex input:        mmd-article-header
Title:              {title}
Author:             Schuyler House
Base Header Level:  2
LaTeX Mode:         article
latex input:        mmd-article-begin-doc

"""[1:]

# #===========================================================================
# latexHeader="""
# latex input:        mmd-memoir-header
# Title:              {title}
# Author:             Schuyler House
# Copyright:          Schuyler House
# Base Header Level:  2
# LaTeX Mode:         memoir
# latex input:        mmd-memoir-begin-doc
# latex footer:       mmd-memoir-footer

# """[1:]
#===========================================================================

if __name__ == '__main__':
	Main()

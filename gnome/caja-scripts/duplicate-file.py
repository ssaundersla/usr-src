#! /usr/bin/python

import os
import re
import glob
import datetime
import shutil
import subprocess

logFile="/home/steve/usr/bin/caja-scripts/logs/duplicate-file.log"
dateRegex = re.compile("(.*)([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})([^0-9])(.*)")

def Main():

	caja = Caja()
	mainDir       = caja.mainDir()
	otherDir      = caja.otherDir()
	mainFileList  = caja.mainFileList()
	otherFileList = caja.otherFileList()

	logList=[]

	if len(mainFileList) < 1:
		logList.append("no files to process")
		ShowInfo("No files have been selected")

	if len(mainFileList) == 1:
		path = mainFileList[0]
		(dirName,fileName) = Get1NewPath( path )
		(basename,ext) = os.path.splitext(fileName)
		basename = VetWithUser(basename)
		fileName = "".join([basename,ext])
		newPath = os.path.join(dirName,fileName)
		logList.append("'%s' becomes '%s'"%(path,fileName))
		shutil.copy2(path,newPath)

	# if len(mainFileList) == 1:
	# 	path = mainFileList[0]
	# 	(dirName,fileName) = Get1NewPath( path )
	# 	fileName = VetWithUser(fileName)
	# 	newPath = os.path.join(dirName,fileName)
	# 	logList.append("'%s' becomes '%s'"%(path,fileName))
	# 	shutil.copy2(path,newPath)

	if len(mainFileList) > 1:
		for path in mainFileList:
			(dirName,fileName) = Get1NewPath(path)
			newPath = os.path.join(dirName,fileName)
			logList.append("'%s' becomes '%s'"%(path,newPath))
			shutil.copy2(path,newPath)

	Log(logList)

#===========================================================================

def Get1NewPath( oldPath ):
	(dirName,fileName) = os.path.split(oldPath)

	global existingFileSet
	existingFileSet = set()
	existingPathList = glob.glob(os.path.join(dirName,"*"))
	for existingPath in existingPathList:
		(d,fn) = os.path.split(existingPath)
		existingFileSet.add(fn)

	newName = GetNewName(fileName)
	newName = GetUniqueNameBasedOn(newName,1)
	return (dirName,newName)

#===========================================================================

def GetNewName( oldName ):

	# if the file looks like an ISO date, increment by one week
	dateSearchResult = dateRegex.search(oldName)
	if dateSearchResult:
		pre = dateSearchResult.group(1)
		y = int(dateSearchResult.group(2))
		m = int(dateSearchResult.group(3))
		d = int(dateSearchResult.group(4))
		post = dateSearchResult.group(5)+dateSearchResult.group(6)
		oneWeek = datetime.timedelta(weeks=1)
		dy = datetime.date(y,m,d)+oneWeek
		return pre+dy.isoformat()+post

	# default method
	return oldName

#===========================================================================

def GetUniqueNameBasedOn(in_filename,n):
	if in_filename in existingFileSet:
		fnList = in_filename.split(".")
		if len(fnList)>1:
			filename = ".".join(fnList[0:-1])
			ext="."+fnList[-1]
		else:
			filename = in_filename
			ext=""
		proposedNewName = filename + '.{n}' + ext
		proposedNewName = proposedNewName.replace( "{n}", str(n) )
		if proposedNewName in existingFileSet:
			return GetUniqueNameBasedOn(in_filename,n+1)
		return proposedNewName
	else:
		return in_filename

#===========================================================================

class Caja():

	def dummyInitForTesting(self):

		# Get data from Caja:
		mainDir       = "file:///home/steve/Desktop"
		otherDir      = "file:///home/steve/Desktop"

		# Caja is weird:
		mainFileList = [
			"/home/steve/Desktop/t1/t2/notes.md",
			"/home/steve/Desktop/t1/t3/notes-2013-08-28.md",
			"/home/steve/Desktop/t1/t4/notes.md",
			""
		]

		otherFileList= [
			"/home/steve/Desktop/t1/t2/notes.md",
			"/home/steve/Desktop/t1/t3/notes-2013-08-28.md",
			"/home/steve/Desktop/t1/t4/notes.md",
			""
		]

		# Finally:
		self.ivar_mainDir         = mainDir[7:]
		self.ivar_otherDir        = otherDir[7:]
		self.ivar_mainFileList    = mainFileList[0:-1]
		self.ivar_otherFileList   = otherFileList[0:-1]


	def __init__(self):

		# Get data from Caja:
		mainDir       = os.getenv("CAJA_SCRIPT_CURRENT_URI")
		otherDir      = os.getenv("CAJA_SCRIPT_NEXT_PANE_CURRENT_URI")
		mainFileList  = os.getenv("CAJA_SCRIPT_SELECTED_FILE_PATHS")
		otherFileList = os.getenv("CAJA_SCRIPT_NEXT_PANE_SELECTED_FILE_PATHS")

		# Caja is weird:
		mainFileString  = "".join(mainFileList)
		mainFileList    = mainFileString.split("\n")
		otherFileString = "".join(otherFileList)
		otherFileList   = otherFileString.split("\n")

		# Finally:
		self.ivar_mainDir         = mainDir[7:]
		self.ivar_otherDir        = otherDir[7:]
		self.ivar_mainFileList    = mainFileList[0:-1]
		self.ivar_otherFileList   = otherFileList[0:-1]

	def mainDir(self):
		return self.ivar_mainDir

	def otherDir(self):
		return self.ivar_otherDir

	def mainFileList(self):
		return self.ivar_mainFileList

	def otherFileList(self):
		return self.ivar_otherFileList

#===========================================================================

def ShowInfo(theString):
	cmd = [
		'matedialog',
		'--warning',
		'--title=Error',
		'--width=300',
		'--height=200',
	]
	cmd.append("--text="+theString)
	theString = subprocess.check_output(cmd)

#===========================================================================

def Log(lineList):
	try:
		fp = open(logFile,"w")
		for line in lineList:
			fp.write(line+'\n')
	except:
		print "Error writing "+"logFile"
		traceback.print_exc()
	finally:
		fp.close()

#===========================================================================

def VetWithUser(theString):
	cmd = [
		'matedialog',
		'--entry',
		'--width=300',
		'--height=200',
		'--title=Duplicating File',
		'--text=Name of duplicated file',
	]
	cmd.append("--entry-text="+theString)
	theString = subprocess.check_output(cmd)
	tList = theString.split("\n")
	if len(tList)>1:
		theString = tList[0]
	return theString

#===========================================================================

if __name__ == '__main__':
	Main()


################################### Notes: ###################################

# CAJA_SCRIPT_CURRENT_URI
# CAJA_SCRIPT_SELECTED_FILE_PATHS
# CAJA_SCRIPT_SELECTED_URIS
# CAJA_SCRIPT_NEXT_PANE_CURRENT_URI
# CAJA_SCRIPT_NEXT_PANE_SELECTED_FILE_PATHS

# ln -sf /home/steve/usr/bin/caja-scripts/duplicate-file.py '/home/steve/.config/caja/scripts/Duplicate'

# command='matedialog --title "Send file using Thunderbird " --text "Do you want to compress the files?" --list --radiolist --column "Choice" --column "Format" true "Do not compress" false ".zip" false ".rar" false ".tar.gz" false ".tar.bz2"'
# responseList = os.popen(command).read().split("\n")


# Other proposed commands:
#  * dos2unix

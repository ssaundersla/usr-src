#! /usr/bin/python

import os
import re
import glob
import datetime
import shutil
import subprocess

logFile="/home/steve/usr/bin/caja-scripts/logs/meld-files.log"
dateRegex = re.compile("(.*)([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})([^0-9])(.*)")

def Main():

	caja = Caja()
	mainDir       = caja.mainDir()
	mainFileList  = caja.mainFileList()

	logList=[]

	if len(mainFileList) < 1:
		ShowInfo("No files selected")

	if len(mainFileList) == 1:
		ShowInfo("Only one file selected")
		logList.append(mainFileList[0])

	if len(mainFileList) == 2:
		cmd = ['meld']
		cmd.append(mainFileList[0])
		cmd.append(mainFileList[1])
		theString = subprocess.check_output(cmd)

	if len(mainFileList) > 2:
		ShowInfo("Too many files selected")

	Log(logList)

#===========================================================================

class Caja():

	def dummyInitForTesting(self):

		# Get data from Caja:
		mainDir       = "file:///home/steve/Desktop"
		otherDir      = "file:///home/steve/Desktop"

		# Caja is weird:
		mainFileList = [
			"/home/steve/Desktop/t1/t2/notes.md",
			"/home/steve/Desktop/t1/t3/notes-2013-08-28.md",
			"/home/steve/Desktop/t1/t4/notes.md",
			""
		]

		otherFileList= [
			"/home/steve/Desktop/t1/t2/notes.md",
			"/home/steve/Desktop/t1/t3/notes-2013-08-28.md",
			"/home/steve/Desktop/t1/t4/notes.md",
			""
		]

		# Finally:
		self.ivar_mainDir         = mainDir[7:]
		self.ivar_otherDir        = otherDir[7:]
		self.ivar_mainFileList    = mainFileList[0:-1]
		self.ivar_otherFileList   = otherFileList[0:-1]


	def __init__(self):

		# Get data from Caja:
		mainDir       = os.getenv("CAJA_SCRIPT_CURRENT_URI")
		otherDir      = os.getenv("CAJA_SCRIPT_NEXT_PANE_CURRENT_URI")
		mainFileList  = os.getenv("CAJA_SCRIPT_SELECTED_FILE_PATHS")
		otherFileList = os.getenv("CAJA_SCRIPT_NEXT_PANE_SELECTED_FILE_PATHS")

		# Caja is weird:
		mainFileString  = "".join(mainFileList)
		mainFileList    = mainFileString.split("\n")
		otherFileString = "".join(otherFileList)
		otherFileList   = otherFileString.split("\n")

		# Finally:
		self.ivar_mainDir         = mainDir[7:]
		self.ivar_otherDir        = otherDir[7:]
		self.ivar_mainFileList    = mainFileList[0:-1]
		self.ivar_otherFileList   = otherFileList[0:-1]

	def mainDir(self):
		return self.ivar_mainDir

	def otherDir(self):
		return self.ivar_otherDir

	def mainFileList(self):
		return self.ivar_mainFileList+self.ivar_otherFileList

	def otherFileList(self):
		return self.ivar_otherFileList

#===========================================================================
theHow2="""
In order to meld two files, you have to have them either in the same folder (Caja) window or in the same Caja window split into two panes (View > Extra Pane).
"""[1:-1]

def ShowInfo(theString):
	cmd = [
		'matedialog',
		'--warning',
		'--width=400',
		'--height=130',
	]
	cmd.append("--title="+theString)
	cmd.append("--text="+theHow2)
	theString = subprocess.check_output(cmd)

#===========================================================================

def Log(lineList):
	try:
		fp = open(logFile,"w")
		for line in lineList:
			fp.write(line+'\n')
	except:
		print "Error writing "+"logFile"
		traceback.print_exc()
	finally:
		fp.close()

#===========================================================================

def VetWithUser(theString):
	cmd = [
		'matedialog',
		'--entry',
		'--width=300',
		'--height=200',
		'--title=Duplicating File',
		'--text=Name of duplicated file',
	]
	cmd.append("--entry-text="+theString)
	theString = subprocess.check_output(cmd)
	tList = theString.split("\n")
	if len(tList)>1:
		theString = tList[0]
	return theString

#===========================================================================

if __name__ == '__main__':
	Main()


################################### Notes: ###################################

# ln -sf /home/steve/usr/bin/caja-scripts/meld-2-files.py '/home/steve/.config/caja/scripts/Meld two files'

#! /usr/bin/python

import os
import re
import glob
import datetime
import shutil
import subprocess

logFile="/home/steve/usr/bin/caja-scripts/logs/pause-files.log"
getCmd = "gvfs-info -a metadata::emblems '{path}'"
setCmd = "gvfs-set-attribute -t stringv '{path}' metadata::emblems \"{emblem}\""
theRegex = re.compile("metadata::emblems: \[([^\]]+)\]")
embCurrentWork = "01 CurrentWork"
embNeedInput = "03 NeedInput"

def Main():

	caja = Caja()
	mainDir       = caja.mainDir()
	otherDir      = caja.otherDir()
	mainFileList  = caja.mainFileList()
	otherFileList = caja.otherFileList()

	logList=[]

	if len(mainFileList) < 1:
		logList.append("no files to process")
		ShowInfo("No files have been selected")

	else:
		for path in mainFileList:
			cmd = getCmd.replace( "{path}", path )
			emblemList=GetEmblems(path)
			try:
				FixEmblems(emblemList,path)
			except:
				ShowInfo("FixEmblems failed for "+path)

	# Log(logList)

#===========================================================================

def GetEmblems(path):
	retval=[]
	cmd = getCmd.replace( "{path}", path )
	try:
		subfileList = os.popen( cmd ).read().split("\n")
		if len(subfileList) > 1:
			theSearchResult = theRegex.search(subfileList[1])
			if theSearchResult:
				emblemString = theSearchResult.group(1)
				emblemList = emblemString.split(", ")
				for emblem in emblemList:
					retval.append(emblem)
	except:
		ShowInfo("GetEmblems failed for "+path)

	return retval

#===========================================================================

def FixEmblems(emblemList,path):
	newEmblemList=[]
	if len(emblemList) > 0:
		for emblem in emblemList:
			if emblem==embCurrentWork:
				newEmblemList.append(embNeedInput)
			elif emblem==embNeedInput:
				newEmblemList.append(embCurrentWork)
			else:
				newEmblemList.append(emblem)

	emblemString = ", ".join(newEmblemList)
	cmd = setCmd.replace( "{path}", path )
	cmd = cmd.replace( "{emblem}", emblemString )
	os.system(cmd)

#===========================================================================

class Caja():

	def dummyInitForTesting(self):

		# Get data from Caja:
		mainDir       = "file:///home/steve/Desktop"
		otherDir      = "file:///home/steve/Desktop"

		# Caja is weird:
		mainFileList = [
			"/home/steve/Desktop/t1/t2/notes.md",
			"/home/steve/Desktop/t1/t3/notes-2013-08-28.md",
			"/home/steve/Desktop/t1/t4/notes.md",
			""
		]

		otherFileList= [
			"/home/steve/Desktop/t1/t2/notes.md",
			"/home/steve/Desktop/t1/t3/notes-2013-08-28.md",
			"/home/steve/Desktop/t1/t4/notes.md",
			""
		]

		# Finally:
		self.ivar_mainDir         = mainDir[7:]
		self.ivar_otherDir        = otherDir[7:]
		self.ivar_mainFileList    = mainFileList[0:-1]
		self.ivar_otherFileList   = otherFileList[0:-1]


	def __init__(self):

		# Get data from Caja:
		mainDir       = os.getenv("CAJA_SCRIPT_CURRENT_URI")
		otherDir      = os.getenv("CAJA_SCRIPT_NEXT_PANE_CURRENT_URI")
		mainFileList  = os.getenv("CAJA_SCRIPT_SELECTED_FILE_PATHS")
		otherFileList = os.getenv("CAJA_SCRIPT_NEXT_PANE_SELECTED_FILE_PATHS")

		# Caja is weird:
		mainFileString  = "".join(mainFileList)
		mainFileList    = mainFileString.split("\n")
		otherFileString = "".join(otherFileList)
		otherFileList   = otherFileString.split("\n")

		# Finally:
		self.ivar_mainDir         = mainDir[7:]
		self.ivar_otherDir        = otherDir[7:]
		self.ivar_mainFileList    = mainFileList[0:-1]
		self.ivar_otherFileList   = otherFileList[0:-1]

	def mainDir(self):
		return self.ivar_mainDir

	def otherDir(self):
		return self.ivar_otherDir

	def mainFileList(self):
		return self.ivar_mainFileList

	def otherFileList(self):
		return self.ivar_otherFileList

#===========================================================================

def ShowInfo(theString):
	cmd = [
		'matedialog',
		'--warning',
		'--title=Error',
		'--width=300',
		'--height=200',
	]
	cmd.append("--text="+theString)
	theString = subprocess.check_output(cmd)

#===========================================================================

def Log(lineList):
	try:
		fp = open(logFile,"w")
		for line in lineList:
			fp.write(line+'\n')
	except:
		print "Error writing "+"logFile"
		traceback.print_exc()
	finally:
		fp.close()

#===========================================================================

def VetWithUser(theString):
	cmd = [
		'matedialog',
		'--entry',
		'--width=300',
		'--height=200',
		'--title=Duplicating File',
		'--text=Name of duplicated file',
	]
	cmd.append("--entry-text="+theString)
	theString = subprocess.check_output(cmd)
	tList = theString.split("\n")
	if len(tList)>1:
		theString = tList[0]
	return theString

#===========================================================================

if __name__ == '__main__':
	Main()


# ln -sf /home/steve/usr/bin/caja-scripts/pause-files.py '/home/steve/.config/caja/scripts/Pause or unpause emblem'

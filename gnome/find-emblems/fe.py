#! /usr/bin/python

import os
import sys
import optparse
import re
import traceback

rootPath = '/home/steve/Documents/Projects/CurrentProjects/emblems'
rootPath = '/home/steve'
theOutput=[]

getCmd = "gvfs-info -a metadata::emblems {path}"
setCmd = "gvfs-set-attribute -t stringv {path} metadata::emblems \"{emblem}\""
theRegex = re.compile("metadata::emblems: \[([^\]]+)\]")

# gvfs-set-attribute -t stringv /home/steve/Documents/Projects/CurrentProjects/doxygen.7z metadata::emblems "14 Done"
# gvfs-info -a metadata::emblems /home/steve/Documents/Projects/CurrentProjects/doxygen.7z

def Main():
	fileList=[]
	dirList=[]
	for root, dirs, files in os.walk(rootPath):
		for filename in files:
			fileList.append( os.path.join(root, filename) )
		for filename in dirs:
			dirList.append( os.path.join(root, filename) )

	for filename in dirList:
		path = filename.replace( " ", "\ " )
		path = path.replace( "|", "\|" )
		path = path.replace( "(", "\(" )
		path = path.replace( ")", "\)" )
		path = path.replace( "'", "\\'" )
		path = path.replace( '"', '\\"' )
		cmd = getCmd.replace( "{path}", path )
		try:
			# if len(opt.outputPath)>0:
			# 	opt.fp = open(opt.outputPath,"w")
			subfileList = os.popen( cmd ).read().split("\n")
			if len(subfileList) > 1:
				theSearchResult = theRegex.search(subfileList[1])
				if theSearchResult:
					theEmblem = theSearchResult.group(1)
					OutputLine('x', "[%s]: %s" % (theEmblem,filename) )
		except:
			print "Error writing "
			traceback.print_exc()

	for line in theOutput:
		print line

#===========================================================================

def OutputLine(opt, theLine):
	# if len(opt.outputPath)>0:
	# 	fp.write( theLine+"\n" )
	# else:
	# 	print theLine+"\n"
	theOutput.append(theLine)

#===========================================================================

if __name__ == '__main__':
	Main()

# ln -s ~/dev/python/find-emblems/find-emblems.py ~/bin/find-emblems
# find . -exec find-emblems --emblemFirst {} \;
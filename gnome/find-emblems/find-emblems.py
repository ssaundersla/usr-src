#! /usr/bin/python

import os
import sys
import optparse
import re
import traceback

getCmd = "gvfs-info -a metadata::emblems {path}"
setCmd = "gvfs-set-attribute -t stringv {path} metadata::emblems \"{emblem}\""
theRegex = re.compile("metadata::emblems: \[([^\]]+)\]")

# gvfs-set-attribute -t stringv /home/steve/Documents/Projects/CurrentProjects/doxygen.7z metadata::emblems "14 Done"
# gvfs-info -a metadata::emblems /home/steve/Documents/Projects/CurrentProjects/doxygen.7z

def Main():
	(opt, theFile) = GetCLOptions()
	path = theFile.replace( " ", "\ " )
	path = path.replace( "|", "\|" )
	path = path.replace( "(", "\(" )
	path = path.replace( ")", "\)" )
	path = path.replace( "'", "\\'" )
	path = path.replace( '"', '\\"' )
	cmd = getCmd.replace( "{path}", path )
	try:
		if len(opt.outputPath)>0:
			opt.fp = open(opt.outputPath,"w")
		subfileList = os.popen( cmd ).read().split("\n")
		if len(subfileList) > 1:
			theSearchResult = theRegex.search(subfileList[1])
			if theSearchResult:
				theEmblem = theSearchResult.group(1)
				if opt.noEmblem:
					OutputLine(opt, "%s" % (theFile) )
				else:
					if opt.emblemFirst:
						OutputLine(opt, "[%s]: %s" % (theEmblem,theFile) )
					else:
						OutputLine(opt, "%s: [%s]" % (theFile,theEmblem) )
		else:
			OutputLine(opt, "Error, File="+theFile )
	except:
		print "Error writing "+opt.outputPath
		traceback.print_exc()
	finally:
		if len(opt.outputPath)>0:
			opt.fp.close()


#===========================================================================

def OutputLine(opt, theLine):
	if len(opt.outputPath)>0:
		fp.write( theLine+"\n" )
	else:
		print theLine+"\n"


#===========================================================================

def GetCLOptions():
	parser = optparse.OptionParser( usage="%prog [options] args", version="1.0" )
	parser.add_option(
		"--noEmblem",
		action="store_true",
		dest="noEmblem",
		default=False,
		help=""
	)
	parser.add_option(
		"--emblemFirst",
		action="store_true",
		dest="emblemFirst",
		default=False,
		help=""
	)
	parser.add_option(
		"--output",
		dest="outputPath",
		default="",
		help="The file to which output will be written"
	)

	(options, args) = parser.parse_args()

	if len(args) < 1:
		print "Error: This command requires one file argument. Use --help."
		sys.exit()

	if len(args) > 1:
		print "Error: This command takes only one file argument. Use --help."
		sys.exit()

	return (options, args[0])

#===========================================================================

if __name__ == '__main__':
	Main()

# ln -s ~/dev/python/find-emblems/find-emblems.py ~/bin/find-emblems
# find . -exec find-emblems --emblemFirst {} \;
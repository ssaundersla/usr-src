#! /usr/bin/python

import optparse
import os

def Main():
	(opt, fileList) = GetCLOptions()
	for filename in fileList:
		ConvertFile( opt, filename )

#===========================================================================

def ConvertFile( opt, filename ):
	cmd = GetCommand( opt.fromFmt, opt.toFmt )
	if cmd != None:
		cmd = cmd.replace( "{filename}", filename )
		os.system(cmd)
		cmd = "/bin/mv /tmp/{filename} {filename}"
		cmd = cmd.replace( "{filename}", filename )
		os.system(cmd)

#===========================================================================
#tr -d '\15\32' < winfile.txt > unixfile.txt
#gawk '{ sub("\r$", ""); print }' winfile.txt > unixfile.txt
#gawk 'sub("$", "\r")' unixfile.txt > winfile.txt

def GetCommand( f, t ):
#	TODO: The first command should vary based on the opt fields
	if (f=='dos') & (t=='unix'):
		return "/usr/bin/gawk '{ sub(\"\\r$\", \"\"); print }' {filename} > /tmp/{filename}"
	elif (f=='dos') & (t=='mac'):
		return None # TODO
	elif (f=='mac') & (t=='unix'):
		return None # TODO
	elif (f=='mac') & (t=='dos'):
		return None # TODO
	elif (f=='unix') & (t=='dos'):
		return "/usr/bin/gawk 'sub(\"$\", \"\\r\")' {filename} > /tmp/{filename}"
	elif (f=='unix') & (t=='mac'):
		return None # TODO
	elif (f=='dos') & (t=='dos'):
		return None # makes no sense
	elif (f=='mac') & (t=='mac'):
		return None # makes no sense
	elif (f=='unix') & (t=='unix'):
		return None # makes no sense
	else:
		return None # should not happen

	return None

#===========================================================================

def GetCLOptions():
	parser = optparse.OptionParser( usage="%prog [options] files", version="1.0" )
	parser.add_option(
		"-s", "--stringOption",
		dest="myStringOption",
		default="myDefaultString",
		help="enter the string"
	)
	parser.add_option(
		"--from",
		dest="fromFmt",
		choices=["unix","mac","dos"],
		default="dos",
		help=""
	)
	parser.add_option(
		"--to",
		dest="toFmt",
		choices=["unix","mac","dos"],
		default="unix",
		help=""
	)

	(options, args) = parser.parse_args()

	return (options, args)

#===========================================================================

if __name__ == '__main__':
	Main()


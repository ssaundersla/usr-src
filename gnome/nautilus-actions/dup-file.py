#! /usr/bin/python

import optparse
import os
import re
import glob
import datetime
import shutil

logFile='/home/steve/usr/bin/nautilus-actions/dup-file.log'
dirFileSet = frozenset(glob.glob('./*'))
dateRegex = re.compile("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})([^0-9])(.*)")

def Main():
	(opt, filenameList) = GetCLOptions()
	d = opt.dir+"/"
	# global fp
	# fp = open(logFile,"w")
	# fp.write('starting\n')
	# fp.write(opt.dir+'\n')
	for filename in filenameList:
		# fp.write('filename: '+filename+'\n')
		newName = GetNewName( filename )
		# fp.write('newname:  '+newName+'\n')
		# fp.write('d      :  '+d+'\n')
		shutil.copy( d+filename, d+newName )
		# fp.write( d+filename +' '+ d+newName +'\n')
	# fp.close()

#===========================================================================

def GetNewName( oldName ):

#	if the file looks like a date
	dateSearchResult = dateRegex.search(oldName)
	if dateSearchResult:
		y = int(dateSearchResult.group(1))
		m = int(dateSearchResult.group(2))
		d = int(dateSearchResult.group(3))
		x = dateSearchResult.group(4)+dateSearchResult.group(5)
		oneWeek = datetime.timedelta(weeks=1)
		dy = datetime.date(y,m,d)+oneWeek
		return dy.isoformat()+x

#	default method
	return GetUniqueName( oldName, 1 )

#===========================================================================

def GetUniqueName(fnString,n):
	fnList = fnString.split(".")
	if len(fnList)>1:
		ext="."+fnList[-1]
		fnString = ".".join(fnList[0:-1])
	else:
		ext=""
	nm = fnString + '-copy-{n}' + ext
	# fp.write('nm 1: '+nm+'\n')
	nm = nm.replace( "{n}", str(n) )
	# fp.write('nm 2: '+nm+'\n')
	if nm in dirFileSet:
		return GetUniqueName(fnString,n+1)
	return nm

#===========================================================================

def GetCLOptions():
	parser = optparse.OptionParser( usage="%prog [options] files", version="1.0" )

	# NOTE: *CANNOT* use '--' options with nautilus actions!
	parser.add_option(
		"-d",
		dest="dir",
		default="dir",
		help="enter the dir"
	)

	(options, args) = parser.parse_args()

	# NOTE: Nautilus actions do weird things to input:
	options.dir = options.dir.replace( "%20", ' ' )

	return (options, args)

#===========================================================================

if __name__ == '__main__':
	Main()


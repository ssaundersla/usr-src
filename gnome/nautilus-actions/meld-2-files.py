#! /usr/bin/python

import optparse
import os
import re
import glob
import datetime
import shutil

logFile='/home/steve/usr/bin/nautilus-actions/meld-2-files.log'
dirFileSet = frozenset(glob.glob('./*'))
dateRegex = re.compile("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})([^0-9])(.*)")

def Main():
	(opt, filenameList) = GetCLOptions()
	d = opt.dir+"/"
	# global fp
	fp = open(logFile,"w")
	fp.write('starting\n')
	fp.write(opt.dir+'\n')
	for filename in filenameList:
		fp.write('filename: '+filename+'\n')
	fp.close()

#===========================================================================

def GetCLOptions():
	parser = optparse.OptionParser( usage="%prog [options] files", version="1.0" )

	# NOTE: *CANNOT* use '--' options with nautilus actions!
	parser.add_option(
		"-d",
		dest="dir",
		default="dir",
		help="enter the dir"
	)

	(options, args) = parser.parse_args()

	# NOTE: Nautilus actions do weird things to input:
	options.dir = options.dir.replace( "%20", ' ' )

	return (options, args)

#===========================================================================

if __name__ == '__main__':
	Main()


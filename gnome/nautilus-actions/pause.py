#! /usr/bin/python

import optparse
import re
import os
import traceback

logFile='/home/steve/usr/bin/nautilus-actions/pause.log'
getCmd = "gvfs-info -a metadata::emblems '{path}'"
setCmd = "gvfs-set-attribute -t stringv '{path}' metadata::emblems \"{emblem}\""
theRegex = re.compile("metadata::emblems: \[([^\]]+)\]")
embCurrentWork = "01 CurrentWork"
embNeedInput = "03 NeedInput"

def Main():
	(opt, filename) = GetCLOptions()
	path = opt.dir+"/"+filename
	fp = open(logFile,"w")
	global fp
	fp.write('starting:\n\n')
	fp.write("path="+path+"\n")

	cmd = getCmd.replace( "{path}", path )
	emblemList=GetEmblems(path)
	try:
		FixEmblems(emblemList,path)
	except:
		fp.write("\n\n------------------\nGetEmblems: Error:\n")
		fp.write(traceback.format_exc())

	fp.close()

#===========================================================================

def FixEmblems(emblemList,path):
	newEmblemList=[]
	if len(emblemList) > 0:
		for emblem in emblemList:
			fp.write("old emblem="+emblem+"\n")
			if emblem==embCurrentWork:
				newEmblemList.append(embNeedInput)
			elif emblem==embNeedInput:
				newEmblemList.append(embCurrentWork)
			else:
				newEmblemList.append(emblem)
		for emblem in newEmblemList:
			fp.write("new emblem="+emblem+"\n")

	emblemString = ", ".join(newEmblemList)
	fp.write("new emblemString="+emblemString+"\n")
	cmd = setCmd.replace( "{path}", path )
	cmd = cmd.replace( "{emblem}", emblemString )
	fp.write("new cmd="+cmd+"\n")
	os.system(cmd)

#===========================================================================

def GetEmblems(path):
	retval=[]
	cmd = getCmd.replace( "{path}", path )
	fp.write("GetEmblems: cmd="+cmd+"\n")
	try:
		subfileList = os.popen( cmd ).read().split("\n")
		fp.write("GetEmblems: len(subfileList)="+str(len(subfileList))+"\n")
		if len(subfileList) > 1:
			theSearchResult = theRegex.search(subfileList[1])
			if theSearchResult:
				emblemString = theSearchResult.group(1)
				emblemList = emblemString.split(", ")
				for emblem in emblemList:
					retval.append(emblem)
	except:
		fp.write("\n\n------------------\nGetEmblems: Error:\n")
		fp.write(traceback.format_exc())

	return retval

#===========================================================================

def GetCLOptions():
	parser = optparse.OptionParser( usage="%prog [options] files", version="1.0" )

	# NOTE: *CANNOT* use '--' options with nautilus actions!
	parser.add_option(
		"-d",
		dest="dir",
		default="dir",
		help="enter the dir"
	)

	(options, args) = parser.parse_args()

	filename = args[0]

	# NOTE: Nautilus actions do weird things to input:
	options.dir = options.dir.replace( "%20", ' ' )
	filename = filename.replace( "%20", ' ' )

	return (options, filename)

#===========================================================================

if __name__ == '__main__':
	Main()


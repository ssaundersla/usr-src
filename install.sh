#! /bin/sh
#
# This script should be run only once in order to do the initial
# installation. See README.md for more info.
#
if [ -f "$HOME/usr" ]; then
	echo "Error: $HOME/usr already exists. Aborting."
	exit
fi
#
if [ -d "$HOME/usr" ]; then
	echo "Error: $HOME/usr already exists. Aborting."
	exit
fi
#
mkdir "$HOME/usr"
cd "$HOME/usr"
#
#	Create the `local` folder hierarchy.
#	Not the `bin` folder -- the bin folder is rebuilt by update.sh.
#
mkdir local
mkdir local/opt
mkdir local/dotfiles
cp src/notes/samples/bashrc     local/dotfiles/bashrc
cp src/notes/samples/exrc       local/dotfiles/exrc
cp src/notes/samples/gitconfig  local/dotfiles/gitconfig
cp src/notes/samples/gitignore  local/dotfiles/gitignore
cp src/notes/samples/local-update.sh local/update.sh
#
#	Add 'source' lines to bashrc
#
if [ -f "$HOME/bashrc" ]; then
	echo '. $HOME/usr/src/dotfiles/bashrc' >> ~/.bashrc
fi
#
if [ -f "$HOME/bash_profile" ]; then
	echo '. $HOME/usr/src/dotfiles/bashrc' >> ~/.bash_profile
fi
#
#	Finally, run the update script
#
chmod +x local/update.sh

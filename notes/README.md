This 'notes' directory has two functions at present:

1. It's a place for sample local configuration files
2. It's a place for brief documentation for common tasks

This cartoon summarizes the purpose of the 'guides' and 'cheatsheets'
directories. Some commands fall into a trap of complex & nitpicky, not
used frequently enough to remember, and used frequently enough that
man-page and google searches get tedious.

https://xkcd.com/1168/


Old content of this file -- not sure now what it's about:

The `local-update.sh` file is an example of what the `local/update.sh`
file should be. It should be copied into the `~/usr/local` folder by
the `install.sh` script. After that, it should be modified locally and
perhaps even be placed under local source control. Here are some more
things that might go into it.

--------------------------------------------------------------------------------
# gitignore options

None?

--------------------------------------------------------------------------------
# Extra Python tools

## Setup

cd ~/usr/opt
git clone ssh://bitbucket-alias/ssaundersla/webdot.git
git clone ssh://bitbucket-alias/ssaundersla/stinkers.git
git clone ssh://bitbucket-alias/ssaundersla/mock-code-gen.git

## Place in ~/usr/local/update.sh

ln -sf ~/usr/opt/mock-code-gen/main.py          ~/usr/bin/mock-code-gen
ln -sf ~/usr/opt/stinkers/stinkers.py           ~/usr/bin/stinkers
ln -sf ~/usr/opt/webdot/main.py                 ~/usr/bin/webdot


"Cheatsheets" are like guides but more compact. They are for quick
reminders or copy-and-paste versions of commands.
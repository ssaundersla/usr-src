SSH cheatsheet
================

ssh -i ~/.ssh/sealevel_prototype.pem mooreboeck@54.153.111.61

Copy a public key to a server and put it in authorized_keys
ssh-copy-id -i MBDevTwo.pub rails@54.176.121.59

Create a public key from a private key
ssh-keygen -y -f MBDevTwo.pem


if ~/.ssh/config is set up
==========================

rsync
-----

Recursively  transfer  all files from the directory src/bar on the machine foo into the /data/tmp/bar directory on the local machine.

rsync -ravz foo:src/bar /data/tmp

A  trailing slash on the source changes this behavior to avoid creating an additional directory level at the destination.  You can think of a trailing / on a source as meaning "copy the contents of this directory" as opposed to "copy the directory by name":

rsync -ravz foo:src/bar/ /data/tmp

In other words, each of the following commands copies the files in the same way:

	rsync -rav /src/foo /dest
	rsync -rav /src/foo/ /dest/foo


scp
---

scp -r /Users/mb/Desktop/ansible_playbooks_cliv/ jenkins:ansible_playbooks_cliv

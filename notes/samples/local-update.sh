#! /bin/sh
#
# Rebuild the local customization
#
# This script should be run from inside the 'local' folder. The
# '~/usr/src/update.sh' script should do this.
#
#-------------------------------------------------------------------------------
# Local configuration
# .bashrc et al
#-------------------------------------------------------------------------------
#
#	cat ~/usr/local/dotfiles/exrc          >> ~/.exrc
#	cat ~/usr/local/dotfiles/gitconfig     >> ~/.gitconfig
#	cat ~/usr/local/dotfiles/gitignore     >> ~/.gitignore
#
#-------------------------------------------------------------------------------
# Local tools
#-------------------------------------------------------------------------------
#	#
#	# ~/usr/local/opt/ tools
#	#
#	ln -sf ~/usr/local/opt/namebench-1.3.1/namebench.py ~/usr/bin/namebench
#	ln -sf ~/usr/local/opt/uncrustify ~/usr/bin/uncrustify
#	ln -sf ~/usr/local/opt/start-mail.sh ~/usr/bin/start-mail.sh
#	ln -sf ~/usr/local/opt/work.sh ~/usr/bin/work
#	#
#	# /opt/ tools
#	#
#	ln -sf /opt/multimarkdown/multimarkdown ~/usr/bin/multimarkdown
#	ln -sf /opt/apache-ant-1.9.4/bin/ant ~/usr/bin/ant
#	ln -sf /opt/remind/rem.sh  ~/usr/bin/rem
#	ln -sf /opt/remind/remcal.sh  ~/usr/bin/remcal
#	ln -sf /opt/remind/remind-03.01.13/src/remind  ~/usr/bin/remind
#	#
#	# Other local tools
#	#
#	ln -sf ~/dev/python/mkweb-py/mkweb.py ~/usr/bin/mkweb
#	ln -sf ~/dev/python/analogsn/main.py ~/usr/bin/analogsn
#	ln -sf ~/dev/python/webdot/main.py ~/usr/bin/webdot

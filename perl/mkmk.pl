#! /usr/bin/perl
# 
# Module:      mkmk
# -------      ---------------------------------
# Usage:       Usage: mkmk [-options] cpp-file
# Output:      cpp-file.make
# Created by:  Kodan Web Technologies, Inc. <ssaunders@kodanwebtech.com> 2001
# 
# Note:
# -----
# This is the Mac OS X version. It uses relative paths to directories because
# of the frequent use of multi-word folder names. Also, the library info in
# PrintExe is different.
# 
# Description:
# ------------
# mkmk makes makefiles.
# 
# 	mkmk -k -dk myapp
# 
# creates a makefile for myapp.cpp that puts the executable file in
# ~/kbin
# 
# 	mkmk -cgi -dea mycgi
# 
# creates a makefile for mycgi.cpp that puts the executable file in
# ~/www/ezn/cgibin/admin
# 
# Modification History:
# ---------------------
# When       Who What
# 8/13/2001  SCS updated for gcc 3.0
# 6/24/1997  SCS Created
# 
# Copyright (C) 2001 by Kodan Web Technologies, Inc. All rights reserved.
#==========================================================================
$gRoot = $ENV{'HOME'};
# $ENV{'PATH'} = "/bin:/usr/bin";
# push @INC, "$gRoot/bin/lib";

&Setup();
&SetUpEnv();
&MakeMakefile();

#==========================================================================

sub Setup
{
	my( $currentDir, $rootDir );

	$currentDir = '.';
	$rootDir = '..';

	$gSpecialExeDir{'d.'}->{'dir'}   = $currentDir;
	$gSpecialExeDir{'d.'}->{'desc'}  = "current";
	$gSpecialExeDir{'db'}->{'dir'}  = "$rootDir/bin";
	$gSpecialExeDir{'db'}->{'desc'} = "/bin for this project/version";
	$gSpecialExeDir{'dc'}->{'dir'}  = "$rootDir/cgi";
	$gSpecialExeDir{'dc'}->{'desc'} = "/cgi for this project/version";
	
}# end Setup

#==========================================================================

sub MakeMakefile
{
	my( %components, @obj, $key, $exeDep, $makefile );

# 	Recurse the .cpp files looking for dependencies
	%components = &GetComponents( $gEnv{'cpp file'} );
	
# 	The input (main executable) source code file is a special case
	$exeDep = $components{ $gEnv{'cpp file'} };
	delete $components{ $gEnv{'cpp file'} };
	
# 	Place the executable source code file name into @obj so that the 
# 	'objects' variable at the top of the makefile can be created
	@obj = ( $gEnv{'cpp file'} );
	foreach $key (keys %components)
	{
		push( @obj, $key );
	}
	
# 	Write the makefile
	$makefile = "$gEnv{'cpp file'}.make";
	unless( open( OUT, ">$makefile" ) )
	{
		print "cannot create $makefile: $!\n";
		exit;
	}
	&PrintHeader();
	&PrintExe( $exeDep, @obj );
	&PrintComponentArea( %components);
	close( OUT );
	
}# end MakeMakefile

###########################################################################
# Analyze files and store data
###########################################################################

#==========================================================================
# GetComponents() just initializes for the recursive StoreDependencies()
#==========================================================================

sub GetComponents
{
	my( $exeCpp )=@_;
	local( %out );

	%out = ();                     # initialize the hash
	&StoreDependencies( $exeCpp ); # fill the hash by recursion
	
	return %out;
	
}# end GetComponents

#==========================================================================
# Fill the %out hash (local to GetComponents) via recursion
#==========================================================================
       
sub StoreDependencies
{
	my( $infile )=@_;
	my( $newFile, $localDependencies, @files, $file );

# 	Exit condition for recursive algorithm
	return if( $out{$infile} );
	
# 	Deal with *this* file ($inFile).  Use GetDependencies() to identify all 
# 	of the other files upon which this file depends.  These 'dependency 
# 	files' are stored in @files. The %out hash if filled in this step also.
	$localDependencies = &GetDependencies( $infile );
	$out{$infile} = $localDependencies;
	@files = split( / /, $localDependencies );
	
# 	The 'dependency files' may have dependency files of their own.  Call 
# 	self for each one of them.  If the %out hash already has an entry for a 
# 	file, it will exit (see "Exit condition for recursive algorithm" above)
	foreach $file (@files)
	{
		if( $file =~ /^(.+)\.h$/ )
		{
			$newFile = $1;
			if( -e "$newFile.cpp" )
			{
				&StoreDependencies( $newFile );
			}
		}
	}

}# end StoreDependencies

#==========================================================================
# Use the -MM option of g++ to get the dependencies for this file ($file).
# Also, strip the junk that g++ -MM adds to its output.
#==========================================================================

sub GetDependencies
{
	my( $file )=@_;
	my( $tmp, $dep, $d1 );

	$tmp = `g++ -MM $file.cpp`;
	( $d1, $dep ) = split( /:/, $tmp );
	$dep =~ s/\s/ /g;
	$dep =~ s/\n/ /g;
	$dep =~ s/\\/ /g;
	$dep =~ s/ +/ /g;
	$dep =~ s/^ //g;
	$dep =~ s/ $//g;
	return $dep;
	
}# end GetDependencies

###########################################################################
# Print 
###########################################################################

sub PrintExe
{
	my( $dependency, @obj )=@_;
	my( $exePath );
	
# 	Get the full path of the executable
	$exePath = "$gEnv{'exe path'}/$gEnv{'cpp file'}";
	$exePath .= ".cgi" if( $gEnv{'cgi'} );
	
# 	Create the list of object files to link into the executable
	$obj = join( ".o \\\n          ", @obj );
	
	print OUT "#=========================================================================\n";
	print OUT "# Executable:\n";
	print OUT "#=========================================================================\n";
	print OUT "\n";
	print OUT "objects = $obj.o\n";
	print OUT "\n";
	print OUT "$exePath: \$(objects)\n";
	print OUT "\t$gEnv{'linker'} $exePath \$(objects)";
	print OUT " -lcurses -ltermcap" if( $gEnv{'curses'} );
	print OUT " -L/usr/local/mysql/lib -lmysqlclient -lz" if( $gEnv{'mysql'} );
	print OUT " -L/usr/lib -lreadline"                    if( $gEnv{'readline'} );
	print OUT "\n\n";
	print OUT "$gEnv{'cpp file'}.o: $dependency\n";
	print OUT "\t$gEnv{'compiler'} $gEnv{'cpp file'}.cpp\n";
	print OUT "\n";

}# end PrintExe

#==========================================================================

sub PrintComponentArea
{
	my( %components )=@_;
	
	print OUT "#=========================================================================\n";
	print OUT "# Component objects:\n";
	print OUT "#=========================================================================\n";
	print OUT "\n";
	foreach $key (sort keys %components)
	{
		&Print1Component( $key, $components{$key} );
	}

}# end PrintComponentArea

#==========================================================================

sub Print1Component
{
	my( $cpp, $dependency )=@_;
	
	print OUT "$cpp.o: $dependency\n";
	print OUT "\t$gEnv{'compiler'} $cpp.cpp\n\n";
}# end Print1Component

#==========================================================================

sub PrintHeader
{
	my( @months, @days, $theDate );
	my( $sec, $min, $hr, $mday, $mon, $yr, $wday, $yday, $isdst );
	
	@months = (
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December',
	);
	
	@days = (
		'Sunday',
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
	);

	( $sec, $min, $hr, $mday, $mon, $yr, $wday, $yday, $isdst ) = localtime();
	$yr += 1900;
	$theDate = "$days[$wday], $months[$mon] $mday, $yr";
	
	print OUT "##########################################################################\n";
	print OUT "#\n";
	print OUT "# Makefile for $gEnv{'cpp file'}\n";
	print OUT "# Last update:  $theDate\n";
	print OUT "# Command Line: mkmk $gTheCommandLine\n";
	print OUT "#\n";
	print OUT "##########################################################################\n\n";

}# end PrintHeader

###########################################################################
# Utilities
###########################################################################

#==========================================================================
# Environment Options:
# --------------------
# cgi:       default = no
# curses:    default = no
# mysql:     default = no
# debug:     default = no
# cpp file:  no default - must be specified
# exe path:  no default - must be specified
#==========================================================================

sub SetUpEnv
{
	local( @clOptions, @clFiles );
	my( $clOption, $dirOpt );

#	Store the command line for documentation
	$gTheCommandLine = join( " ", @ARGV );

# 	Parse the command line
	&ParseCommandLine();

# 	No options or no files is a known error condition
	&PrintUsage() if( $#clOptions < 0 );
	&PrintUsage() if( $#clFiles < 0 );

# 	Set the cpp file (the C++ file which contains the main() function).
# 	Try to strip the suffix from it, just in case it's there.
	$gEnv{'cpp file'} = $clFiles[0];
	$gEnv{'cpp file'} =~ s/\.cpp$//;

# 	Loop through the command line options, setting
# 	(most) of the environment options.
	$gEnv{'cgi'}      = 0;
	$gEnv{'curses'}   = 0;
	$gEnv{'mysql'}    = 0;
	$gEnv{'readline'} = 0;
	$gEnv{'debug'}    = 0;
	$dirOpt           = '*';
	foreach $clOption (@clOptions)
	{
		&PrintUsage() if( $clOption eq 'h' );
		$gEnv{'cgi'}      = 1 if( $clOption eq 'cgi' );
		$gEnv{'curses'}   = 1 if( $clOption eq 'curses' );
		$gEnv{'curses'}   = 1 if( $clOption eq 'k' );
		$gEnv{'mysql'}    = 1 if( $clOption eq 'mysql' );
		$gEnv{'mysql'}    = 1 if( $clOption eq 'm' );
		$gEnv{'readline'} = 1 if( $clOption eq 'readline' );
		$gEnv{'readline'} = 1 if( $clOption eq 'r' );
		$gEnv{'debug'}    = 1 if( $clOption eq 'b' );
		$gEnv{'debug'}    = 1 if( $clOption eq 'g' );
		$dirOpt = $clOption if( $clOption =~ /^d/ );
	}

#   Try to set the path for the executable
	if( $dirOpt eq 'd' )
	{
		&CheckForExeDir();
		$gEnv{'exe path'} = $clFiles[1];
	}
	else
	{
		$gEnv{'exe path'} = $gSpecialExeDir{ $dirOpt }->{'dir'};
	}

# 	Expand the '~' if it is present.  Flag the error if there is no 
# 	indication of where to put the executable.
	if( defined( $gEnv{'exe path'} ) )
	{
		$gEnv{'exe path'} =~ s/^~/$gRoot/;
	}
	else
	{
		print "\nError: a directory must be specified for the executable\n\n";
		&PrintUsage();
	}

# 	Flag the -cgi -k error
	if( $gEnv{'cgi'} && $gEnv{'curses'} )
	{
		print "\nError: the -cgi and -k options are incompatible\n\n";
		&PrintUsage();
	}

# 	Online version
# 	$gEnv{'compiler'} = "g++ -fmessage-length=0 -O2 -c";
# 	$gEnv{'linker'}   = "g++ -fmessage-length=0 -O2 -o";

# 	Testbed version
	$gEnv{'compiler'} = "g++ -fmessage-length=0 -Wall -c";
	$gEnv{'linker'}   = "g++ -fmessage-length=0 -Wall -o";

	$gEnv{'compiler'} = "g++ -fmessage-length=0 -Wall -c -g" if( $gEnv{'debug'} );
	$gEnv{'linker'}   = "g++ -fmessage-length=0 -Wall -g -o" if( $gEnv{'debug'} );
	
}# end SetUpEnv

#==========================================================================

sub CheckForExeDir
{
	if( $clFiles[1] eq "" )
	{
		print "\nError: a directory must be specified for the executable\n\n";
		&PrintUsage();
	}

}# end CheckForExeDir

#==========================================================================

sub PrintUsage
{
	my( $key, $x );
	
	print "Usage: mkmk [-options] {main cpp source code file} [{directory}]\n";
	print "            -h      prints this\n";
	print "            -cgi    CGI executable (adds '.cgi' suffix)\n";
	print "            -k      curses executable (don't use with -cgi)\n";
	print "            -m      mysql executable\n";
	print "            -r      readline executable (don't use with -cgi)\n";
	print "            -b      compile with debug option (gdb a.out -c a.out.core)\n";
	print "            -g      same as -b\n";
	print "            -d      place executable in directory specified by {directory}\n";
	
	foreach $key (sort keys %gSpecialExeDir)
	{
		print "            -$key";
		$x = 7 - length( $key );
		print ' ' x $x;
		print "place executable in $gSpecialExeDir{$key}->{'desc'} directory\n";
	}

	print "\n";
	print "Standard CGI makefile: mkmk -cgi -m -dc myapp\n";
	print "Standard mek makefile: mkmk -m -db myappmek\n";
	print "\n";
	
	exit;
}# end PrintUsage

#==========================================================================

sub ParseCommandLine
{
	my( $arg );
	
	@clOptions = ();
	@clFiles   = ();
	
	for(;;)
	{
		$arg = shift @ARGV;
		if( defined( $arg ) )
		{
			if( $arg =~ /^-(.+)$/ )
			{
				push( @clOptions, $1 );
			}
			else
			{
				push( @clFiles, $arg );
			}
		}
		else
		{
			return;
		}
	}

}# end ParseCommandLine

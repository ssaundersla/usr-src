#! /usr/bin/python3

import optparse
import os

def Main():
	(opt, args) = GetCLOptions()
	theCommand = GetTheCommand(opt)
	recList = CreateList(theCommand)
	for rec in recList:
		print (rec)

#-----------------------------------------------------------------------------

def GetTheCommand(opt):
	if opt.units:
		u='h'
	else:
		u='k'

	pwd = os.getcwd()
	if pwd=='/':
		retval="du --exclude=cro -s"+u+" *"
	else:
		retval="du -s"+u+" *"
	return retval
	# TODO: Add `du -hs .[^.]*` to get dotfile directory sizes

#-----------------------------------------------------------------------------

def CreateList(subfileCmd):
	subfileList = os.popen( subfileCmd ).read().split("\n")
	recList=[]
	for subfile in subfileList:
		fieldList = subfile.split("\t")
		if len(fieldList)==2:
			rec = "%12s %-55s" % (fieldList[0],fieldList[1])
			recList.append(rec)
	recList.sort()
	recList.reverse()
	return recList

#===========================================================================

def GetCLOptions():
	parser = optparse.OptionParser( usage="%prog [options] args", version="1.0" )
	parser.add_option(
		"--units",
		action="store_true",
		dest="units",
		default=False,
		help="shows units (e.g. 'M', 'G')"
	)

	(options, args) = parser.parse_args()

	return (options, args)

#===========================================================================

if __name__ == '__main__':
	Main()


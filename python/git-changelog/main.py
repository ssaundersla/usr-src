#! /usr/bin/python

import optparse
import popen2
import re
import sys

# TODO: Improve version number determination
# See VersionFinder below.
#
# Note that this will only work for projects which store their version
# numbers in a particular way.

#===============================================================================
#	Usage notes:
#
#	 -	The arguments are a range. The first is the low range and is non-
#		inclusive. The second is the high range and is inclusive. The
#		second argument defaults to HEAD (the most recent commit). This
#		means that main.py v2.0 will *not* print the release notes for
#		version 'v2.0' -- it will print all of the release notes *after*
#		version 'v2.0'
#
#	 -	The arguments must match the git tags exactly. 'v1.0' and 'v1.0.0'
#		are completely different tags as far as git can tell.
#
def Main():
	(opt, args) = GetCLOptions()
	addArgsToOpt(opt,args)

	if opt.printManual:
		printManual()
		sys.exit()

	reader = GitLogReader(opt)
	print reader.reportReleaseNotes()

 #    if opt.outputType == "releasenotes":

	# if opt.outputType == "detail":
	# 	print reader.reportDetail()


#=============================================================================
## Print the user manual.
#
def printManual():
	print theManual

#=============================================================================
## Modifes the `opt` object to contain the log range.
#
def addArgsToOpt(opt,args):
	opt.gitCmdRange = ""
	if len(args) == 1:
		opt.gitCmdRange = " "+args[0]+"..HEAD"
	if len(args) == 2:
		opt.gitCmdRange = " "+args[0]+".."+args[1]


#===============================================================================
##	Reads the git commit log in the current directory.
##	You have to use the report... methods to create reports.
#
class GitLogReader():

	tagPattern = re.compile("^(.+)")
	versionDescTemplate = "-"*72+"\n## Version {version}\n*Released: {date}*\n\n{shortDesc}\n\n#### Changes ####\n\n{changeLog}"

	def __init__(self, opt):
		self.opt = opt
		if opt.formatFile != "":
			self.versionDescTemplate = self.readFormatFile(opt.formatFile)

	#===========================================================================
	##	Build report: release notes.
	##	Builds a release notes document using the output format given on the
	##	command line.
	##
	##	@note This report should be run from a stable branch to avoid
	##	artifacts of incorrect releases picking up commits. If this report
	##	is run from a stable branch, there should be nothing in the
	##	'development' release.
	#
	def reportReleaseNotes(self):
		self.tagMap = self.readTagMap()
		self.entryList = self.readGitLogEntries()
		theList = self.buildVersionList()
		if self.opt.reverse:
			theList.reverse()
		return "\n\n\n".join(theList)

	#===========================================================================
	##	Lists the tags found in the git repo.
	##
	##	@returns a dict which maps revision IDs to dicts of tag data
	#
	def readTagMap(self):
		e1, e2, e3 = popen2.popen3('git tag')
		tagList = e1.readlines()
		e1.close()
		e2.close()
		tagMap={}
		for tag in tagList:
			r = self.tagPattern.search(tag)
			if r:
				tagName = r.group(1)
				fields = self.readTagFields(tagName)
				fields['version'] = tagName
				if 'rev' in fields:
					key = fields['rev']
					tagMap[key]=fields
		return tagMap

	#===========================================================================
	##	Given a tag name, polls the git repo for details about the commit
	##	to which the tag refers.
	##
	##	@param tagName a string naming a tag to be read
	##	@returns a dict of tag data
	#
	def readTagFields(self,tagName):
		e1, e2, e3 = popen2.popen3(AnnotatedTagParser.gitCmd+tagName)
		lineList = e1.readlines()
		e1.close()
		e2.close()
		parser = AnnotatedTagParser()
		for line in lineList:
			parser.parse(line)
		return parser.fields

	#===========================================================================
	##	Reads the entire git log using a format which CommitEntryParser can
	##	evaluate. Reads from the current directory.
	##
	##	@returns a list of dicts: Each dict describes one git commit.
	#
	def readGitLogEntries(self):
		e1, e2, e3 = popen2.popen3(CommitEntryParser.gitCmd+self.opt.gitCmdRange)
		lineList = e1.readlines()
		e1.close()
		e2.close()
		parser = CommitEntryParser(self.opt)
		for line in lineList:
			parser.parse(line)
		return parser.entryList

	#===============================================================================
	##	Given the git tag map and the git commit log entry list (self.tagMap
	##	and self.entryList), builds a list of changes for each tagged release.
	##	This assumes that each tag is a release.
	##
	##	@returns a list of strings, each of which is a report on one release
	#
	def buildVersionList(self):
		changeList=[]
		versionList = []
		for entry in self.entryList:
			if 'log' in entry:
				changeList.append(entry['log'])
			key = entry['rev']
			if key in self.tagMap:
				tag = self.tagMap[key]
				changeLog = ""
				for x in changeList:
					changeLog += "* "+x+"\n"
				versionDesc = self.versionDescTemplate
				versionDesc = versionDesc.replace( "{changeLog}", changeLog )
				versionDesc = versionDesc.replace( "{version}",   tag['version'] )
				versionDesc = versionDesc.replace( "{shortDesc}", tag['message'].strip() )
				versionDesc = versionDesc.replace( "{name}",      tag['name'] )
				versionDesc = versionDesc.replace( "{date}",      tag['date'] )
				versionDesc = versionDesc.replace( "{email}",     tag['email'] )
				versionList.append(versionDesc.strip())
				changeList=[]
		if len(changeList) > 0:
			changeLog = ""
			for x in changeList:
				changeLog += "* "+x+"\n"
			versionDesc = self.versionDescTemplate
			versionDesc = versionDesc.replace( "{changeLog}", changeLog )
			versionDesc = versionDesc.replace( "{version}",   "(Development)" )
			versionDesc = versionDesc.replace( "{shortDesc}", "These are changes currently in development." )
			versionDesc = versionDesc.replace( "{name}",      "(not yet released)" )
			versionDesc = versionDesc.replace( "{date}",      "(no release date)" )
			versionDesc = versionDesc.replace( "{email}",     "(not yet released)" )
			versionList.append(versionDesc.strip())
		return versionList

	#=============================================================================
	##	Reads the format file and uses its contents to overwrite the version
	##	description template.
	##
	## @returns the contents of the format file
	## @param ffname the path to the format file
	#
	def readFormatFile(self,ffname):
		lineList=[]
		try:
			fp = open(ffname)
			for line in fp:
				lineList.append(line.rstrip())
		except:
			print "Error reading "+ffname
			traceback.print_exc()
		finally:
			fp.close()
		return "\n".join(lineList)


#===============================================================================
##	Parses a git 'annotated tag' description, provided the input has been
##	generated by the AnnotatedTagParser-specific git log command.
##
##	@par Sample Usage
##	@code
##		e1, e2, e3 = popen2.popen3(AnnotatedTagParser.gitCmd+tagName)
##		lineList = e1.readlines()
##		parser = AnnotatedTagParser()
##		for line in lineList:
##			parser.parse(line)
##	@endcode
#
class AnnotatedTagParser():

	gitCmd = "git cat-file -p "
	revPattern    = re.compile("^object\s+([0-9a-fA-F]+)")
	taggerPattern = re.compile("^tagger\s+([^<]+)\s+<([^>]+)>\s+(.+)")

	def __init__(self):
		self.state = 0
		self.fields = {}

	def parse(self,line):

		if self.state==0:
			if len(line)<2:
				self.state=1
				self.fields['message'] = ''
				return
			r = self.revPattern.search(line)
			if r:
				self.fields['rev'] = r.group(1)
				return
			r = self.taggerPattern.search(line)
			if r:
				self.fields['name'] = r.group(1)
				self.fields['email'] = r.group(2)
				self.fields['date'] = r.group(3)
				return

		if self.state==1:
			self.fields['message'] += line


#===============================================================================
## Parses the lines of a git log, provided the log has been generated by
## the CommitEntryParser-specific git log command. The product of the parsing is
## the entryList element. The entryList element is a list of hashes.
##
##	@par Sample Usage
##	@code
##		e1, e2, e3 = popen2.popen3(CommitEntryParser.gitCmd)
##		lineList = e1.readlines()
##		parser = CommitEntryParser()
##		for line in lineList:
##			parser.parse(line)
##		DoSomethingWith(parser.entryList)
##	@endcode
#
class CommitEntryParser():

	gitCmd = 'git log --date-order --reverse --pretty=format:"--- start --- 32e39a803fd2ad5a4396b29c633efee4e6a90814ef8d622ae34f ---%n--- rev 59ccb30cf --- %H%n--- name 87873dd6b --- %an%n--- email e950f1e5c --- %ae%n--- date 63ebd25a4 --- %ar%n--- message --- 77036889199054c2616d2fa1ea97708f34591eb2516797c5eb ---%n%B%n--- end --- 408d37b7d0dd373df4f59aac024b5c93d3a0757c0e0fba805895f5 ---%n"'

	startLine        = "--- start --- 32e39a803fd2ad5a4396b29c633efee4e6a90814ef8d622ae34f ---"
	startMessageLine = "--- message --- 77036889199054c2616d2fa1ea97708f34591eb2516797c5eb ---"
	endLine          = "--- end --- 408d37b7d0dd373df4f59aac024b5c93d3a0757c0e0fba805895f5 ---"
	revPattern   = re.compile("^--- rev 59ccb30cf --- (.+)")
	namePattern  = re.compile("^--- name 87873dd6b --- (.+)")
	emailPattern = re.compile("^--- email e950f1e5c --- (.+)")
	datePattern  = re.compile("^--- date 63ebd25a4 --- (.+)")

	def __init__(self,opt):
		self.opt=opt
		self.entryList=[]
		self.zero()

	def zero(self):
		self.state = 0
		self.rev = None
		self.name = None
		self.email = None
		self.date = None
		self.message = None
		self.logParser = None

	def buildEntry(self):
		if self.rev == None:
			self.zero()
			return
		t={}
		t['rev']=self.rev
		vf = VersionFinder(self.rev)
		t['version']=vf.version
		if self.message != None:
			t['message']=self.message.strip()
			log = self.logParser.message
			if len(log)>0:
				t['log']=log
		if self.name != None:
			t['name']=self.name
		if self.email != None:
			t['email']=self.email
		if self.date != None:
			t['date']=self.date
		self.entryList.append(t)
		self.zero()

	def parse(self,line):

		if self.state==0:
			if line.startswith(self.startLine):
				self.state=1
				return

		if self.state==1:
			if line.startswith(self.startMessageLine):
				self.state=2
				self.message = ''
				self.logParser = ChangelogParser(self.opt)
				return
			r = self.revPattern.search(line)
			if r:
				self.rev = r.group(1)
				return
			r = self.datePattern.search(line)
			if r:
				self.date = r.group(1)
				return
			r = self.emailPattern.search(line)
			if r:
				self.email = r.group(1)
				return
			r = self.namePattern.search(line)
			if r:
				self.name = r.group(1)
				return

		if self.state==2:
			if line.startswith(self.endLine):
				self.state=0
				self.buildEntry()
				return
			self.message += line
			self.logParser.parse(line)


# TODO: This version of ChangelogParser will only produce one change log
# entry per commit. This should not be a problem in a real situation,
# but it's not good practice. Better would be to produce an element like
# ChangelogParser.messageList instead of ChangelogParser.message.

#===============================================================================
##	Scans the body of a git commit message, looking for changelog tags.
##	The format of the changelog tags is specified by the `logtag` command
##	line options (see GetCLOptions). The product of this parsing is the
##	message element.
##
##	@par Sample Usage
##	@code
##		logParser = ChangelogParser(self.opt)
##		for line in lineList:
##			logParser.parse(line)
##		log = logParser.message
##	@endcode
#
class ChangelogParser():

	def __init__(self,opt):
		self.opt=opt
		self.state=0
		self.message = ''

	def parse(self,line):
		lcLine = line.lower()
		if self.state==0:
			if lcLine.startswith(self.opt.logTagStart):
				self.state=1
				return
		if self.state==1:
			if lcLine.startswith(self.opt.logTagEnd):
				self.state=0
				return
			self.message += line

#===========================================================================
##	Stub of a class which should find the version of the project given
##	the revision hash value.
#
# TODO: Improve version number determination
#
# The current methodology for getting version numbers for commits is
# non-project-dependent, but also non-accurate. It assumes a more linear
# development path than any normal project will have. Better would be
# to parse the file that contains the version number, e.g.
#
# 	git show {commit}:build.xml
#
# and piping it to
#
# 	grep '^\+\s*<property name="version"\s*value="([^"]+)"/>'
#
class VersionFinder():
	def __init__(self, rev):
		self.version = rev

#===========================================================================

def GetCLOptions():
	parser = optparse.OptionParser( usage="%prog [options] [tag1 [tag2]]", version="1.0" )
	parser.add_option(
		"--logtag-start",
		dest="logTagStart",
		default='<changelog>',
		metavar='"<changelog>"',
		help="Marks beginning of change log entry in a commit message"
	)
	parser.add_option(
		"--logtag-end",
		dest="logTagEnd",
		default='</changelog>',
		metavar='"</changelog>"',
		help="Marks end of change log entry in a commit message"
	)
	# parser.add_option(
	# 	"--output-type",
	# 	dest="outputType",
	# 	choices=["releasenotes","detail"],
	# 	default='releasenotes',
	# 	metavar='releasenotes|detail',
	# 	help="Type of output"
	# )
	parser.add_option(
		"--format-file",
		dest="formatFile",
		default="",
		metavar='FILE',
		help="Name of file to use for custom output format"
	)
	parser.add_option(
		"--reverse",
		action="store_true",
		dest="reverse",
		default=False,
		help="Print output in reverse chronological order"
	)
	parser.add_option(
		"--man",
		action="store_true",
		dest="printManual",
		default=False,
		help="Print the manual"
	)

	(options, args) = parser.parse_args()

	return (options, args)

#===========================================================================
theManual="""
Generate Git Change Log

Creates a set of release notes for a project which is controlled by
git. The command can take up to two arguments, both of which are tags
in the git repo. The first is the low range and is non-inclusive. The
second is the high range and is inclusive. The second argument
defaults to HEAD (the most recent commit). This means that

% ggcl v2.0

will *not* print the release notes for version 'v2.0' -- it will print
all of the release notes *after* version 'v2.0'. Yes this is confusing.

Some notes:

The arguments must match the git tags exactly. 'v1.0' and 'v1.0.0' are
completely different tags as far as git can tell.

Note: This report should be run from a stable branch to avoid
artifacts of incorrect releases picking up commits. If this report is
run from a stable branch, there should be nothing in the 'development'
release.

"""[1:]

#===========================================================================

if __name__ == '__main__':
	Main()

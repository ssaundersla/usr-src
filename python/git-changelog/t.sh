#!/bin/sh

# Setup
cd ~/Desktop
rm -rf tmp
mkdir tmp
cd tmp
ls -la > t1.txt
git init
git a
git cam "initial import"
git br stable

# change master
ls -la >> t1.txt
git a
git cam "a change"
ls -la >> t1.txt
git a
git commit -m "Add a feature

<changelog>
New feature #1
(belongs in v1)
</changelog>
"

# Release v1
git co stable
git merge master
git tag -a v1 -m"These are the release notes for this version."

# change master
git co master
ls -la >> t1.txt
git a
git cam "another change #1"
ls -la >> t1.txt
git a
git commit -m "Add another feature

<changelog>
New feature #2 (new contact form)
being developed while/before hotfix
belongs in v2
</changelog>
"

### Oops (v1.1)
git co stable
git br hotfix
git co hotfix
ls -la >> t2.txt
git a
git commit -m "Fix a bug

<changelog>
Fix #1
part of v1.1
</changelog>
"
git tag -a v1.1 -m"These are the release notes for this version."
git co stable
git merge hotfix
git co master
git merge hotfix
git br -d hotfix
### end oops

# back to work on master
git co master
ls -la >> t1.txt
git a
git cam "another change #2"
ls -la >> t1.txt
git a
git commit -m "Add a feature

<changelog>
New feature #3
developed after hotfix
belongs in v2
</changelog>
"

# Release v2
git co stable
git merge master
git tag -a v2 -m"These are the release notes for this version."

# back to work on master
git co master
ls -la >> t1.txt
git a
git cam "another change #3"
ls -la >> t1.txt
git a
git commit -m "Add a feature

<changelog>
New feature #4
developed after v2
belongs in dev
</changelog>
"

# Change log for master
git co master
~/dev/J2EE/schuybase/src/tools/git-chlg/main.py

# Change log for stable
git co stable
~/dev/J2EE/schuybase/src/tools/git-chlg/main.py

# gitk

#===============================================================================
# Problem:
#
# A change was made on the master branch concurrently with a change made
# on a hotfix branch. The hotfix branch was tagged (as a release) and
# merged into master (v1.1.1). After this merge, another release was
# tagged (v1.2). The change made on the master branch was incorrectly
# included with the release notes for the hotfix and was not included in
# the release notes for v1.2.

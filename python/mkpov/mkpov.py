#! /usr/bin/python

import optparse
import os
import sys
import shutil

def Main():
	(opt, args) = GetCLOptions()

	if opt.manual:
		print manualContent
		sys.exit()

	if opt.clean:
		Clean()

	(sceneList,objList)=GetLists(opt)
	Build(sceneList,objList)

#===========================================================================

def GetLists(opt):
	sceneList = opt.scenes.split(",")
	objList = opt.objects.split(",")
	s1=set(sceneList)
	s2=set(objList)
	s3=s1&s2
	if len(s3)>0:
		print "Error: Scenes and objects must all have different names"
		sys.exit()

	return (sceneList,objList)

#===========================================================================

def Clean():
	shutil.rmtree("draft")
	shutil.rmtree("final")
	shutil.rmtree("objects")
	shutil.rmtree("scenes")

#===========================================================================

def mkfile(path,content):
	try:
		fp = open(path,"w")
		fp.write(content)
	except:
		print "Error writing "+"path"
	finally:
		fp.close()

def Build(sceneList,objList):

	os.mkdir("draft")
	os.mkdir("final")
	os.mkdir("objects")
	os.mkdir("scenes")

	mkfile("objects/axes.inc",axesContent)

	for scene in sceneList:

		path="scenes/"+scene+".pov"
		content=sceneContent
		content = content.replace( "{title}", "Final assembly scene for '{obj}'" )
		content = content.replace( "{obj}", scene )
		mkfile(path,content)

		path="draft/"+scene+".ini"
		content=draftContent
		content = content.replace( "{scene}", scene )
		mkfile(path,content)

		path="final/"+scene+".ini"
		content=finalContent
		content = content.replace( "{scene}", scene )
		mkfile(path,content)

	for obj in objList:

		path="objects/"+obj+".inc"
		content=objectContent
		content = content.replace( "{obj}", obj )
		mkfile(path,content)

		path="objects/"+obj+".preview.pov"
		content=sceneContent
		content = content.replace( "{title}", "Preview container scene for {obj} object" )
		content = content.replace( "{obj}", obj )
		mkfile(path,content)

		path="draft/"+obj+".preview.ini"
		content=draftContent
		content = content.replace( "{scene}", obj+".preview" )
		mkfile(path,content)


#===========================================================================

def GetCLOptions():
	parser = optparse.OptionParser( usage="%prog [options]", version="1.0" )
	parser.add_option(
		"--objects",
		dest="objects",
		default="",
		metavar='obj1,obj2',
		help="list of objects to be created"
	)
	parser.add_option(
		"--scenes",
		dest="scenes",
		default="",
		metavar='sn1,sn2',
		help="list of scenes to be created"
	)
	parser.add_option(
		"--man",
		action="store_true",
		dest="manual",
		default=False,
		help="print the manual (more detailed help)"
	)
	parser.add_option(
		"--clean",
		action="store_true",
		dest="clean",
		default=False,
		help="delete the POV folders first"
	)

	(options, args) = parser.parse_args()

	if not options.manual:
		if options.scenes == "":
			print "Error: No scenes specified"
			sys.exit()
		if options.objects == "":
			print "Error: No objects specified"
			sys.exit()


	return (options, args)

#===========================================================================

manualContent="""
The `mkpov` utility builds a boilerplate folder for development of a
POV-Ray project. It assumes that you will be keeping various bits in
certain standard places and running `povray` in the root directory.
For example:

    povray draft/living-room.ini

will render a draft version of the `living-room` scene. These so-called
standards are standard only to mkpov:

File types and their purposes:

File Ext      | Purpose
--------------|------
.ini          | controls execution of rendering engine
.inc          | defines an object
.pov          | constructs a scene from object definitions
.preview.pov  | constructs a scene suitable for object preview

Folders and their purposes:

Folder  | Purpose
--------|------
draft   | .ini files for draft (quick) renderings
final   | .ini files for final (slow, high-quality) renderings
objects | .inc and .preview.pov files for components
scenes  | .pov files for final assemblies

To build such a project, issue a command like the following:

    mkpov --objects=obj1,obj2,obj3 --scenes=scene1,scene2

This command will build a directory structure and a series of files
that you can edit to build the individual pieces (obj1,obj2,obj3),
preview them individually, and build scenes (scene1,scene2) from the
pieces.
"""[1:-1]

draftContent="""
;	Preview rendering of {scene} object/scene
;
;-------------------------------------------------------------------------------
;	Basics
;
Input_file_name={scene}.pov
Output_file_name={scene}.png
Library_Path=./scenes,./objects
Output_File_Type=N
Display=off
;
;-------------------------------------------------------------------------------
;	Size
;
Height=480
Width=640
;
;-------------------------------------------------------------------------------
;	Tracing Options
;
Quality=11
Antialias=off
;
;-------------------------------------------------------------------------------
"""[1:-1]

#-*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-

finalContent="""
;	Publication-quality rendering of {scene} scene
;
;-------------------------------------------------------------------------------
;	Basics
;
Input_file_name={scene}.pov
Output_file_name={scene}.png
Library_Path=./scenes,./objects
Output_File_Type=N
Display=off
Output_Alpha=on
;
;-------------------------------------------------------------------------------
;	Size
;
Height=2400
Width=3200
;
;-------------------------------------------------------------------------------
;	Tracing Options
;
Quality=11
Antialias=on
;
;-------------------------------------------------------------------------------
"""[1:-1]

#-*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-

objectContent="""
/*
Object: {obj}
Origin: 'lower-left' corner of {obj} in vertical orientation
Size: about 5x5x5
*/

#include "colors.inc"

#local objThickness = 5.0;

#declare {obj} = prism {
    linear_sweep
    bezier_spline
    objThickness, //top
    0.0, //bottom
    148 //nr points
    /*   0*/ <2.16837170, 5.01018262>, <2.16837170, 5.01018262>, <1.97103340, 4.35938262>, <1.97103340, 4.35938262>,
    /*   1*/ <1.97103340, 4.35938262>, <1.83054410, 4.31938262>, <1.69702560, 4.26338262>, <1.57165840, 4.19378262>,
    /*   2*/ <1.57165840, 4.19378262>, <1.57165840, 4.19378262>, <0.97181272, 4.51448262>, <0.97181272, 4.51448262>,
    /* ...*/
    /*   Use Inkscape:                            */
    /*		1. Create image                       */
    /*		2. Move to (0,0) == lower left corner */
    /*		3. Resize                             */
    /*		4. Save as .pov file                  */
    /* ...*/

	rotate -90*x
	translate objThickness*z
	pigment
	{
		Green
	}
}
"""[1:-1]

#-*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-

sceneContent="""
//	{title}
//
//------------------------------------------------------------------------------

#version 3.5;
#include "colors.inc"
global_settings { assumed_gamma 1.0 }

//------------------------------------------------------------------------------
//	The universe

background { color Cyan }

//------------------------------------------------------------------------------
//	The lights

#local camera_location=<20, 30, -30>*10;

light_source { camera_location color White}

//------------------------------------------------------------------------------
//	The camera

camera
{
	perspective
	location camera_location
	look_at  <0, 0, 0>
}

//------------------------------------------------------------------------------
//	Place the objects
#include "axes.inc"

#include "{obj}.inc"
object
{
	{obj}
}
"""[1:-1]

#-*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-  -*-

axesContent="""
//	Tinkertoy-like x/y/z axis indicators for helping with object placement
//	and scaling.
//		- No further .inc dependencies
//		- width and length of axis indicators controlled by variables
//		- colors of axes controlled by variables

#local rodWidth = 1;
#local rodLength = 10;
#local arrowheadWidthRatio = 1.5;
#local arrowheadLengthRatio = 3;
#local xColor = rgb <1,0,0>;
#local yColor = rgb <0,1,0>;
#local zColor = rgb <0,0,1>;

sphere
{
	< 0, 0, 0 >,
	2
	pigment { color rgb <1,1,1> }
}

cylinder
{
	< 0, 0, 0 >,
	< rodLength, 0, 0 >,
	rodWidth
	pigment { xColor }
}
cone
{
	< rodLength, 0, 0 >,
	rodWidth*arrowheadWidthRatio
	< rodLength+rodWidth*arrowheadLengthRatio, 0, 0 >,
	0
	pigment { xColor }
}

cylinder
{
	< 0, 0, 0 >,
	< 0, rodLength, 0 >,
	rodWidth
	pigment { yColor }
}
cone
{
	< 0, rodLength, 0 >,
	rodWidth*arrowheadWidthRatio
	< 0, rodLength+rodWidth*arrowheadLengthRatio, 0 >,
	0
	pigment { yColor }
}

cylinder
{
	< 0, 0, 0 >,
	< 0, 0, rodLength >,
	rodWidth
	pigment { zColor }
}
cone
{
	< 0, 0, rodLength >,
	rodWidth*arrowheadWidthRatio
	< 0, 0, rodLength+rodWidth*arrowheadLengthRatio >,
	0
	pigment { zColor }
}
"""[1:-1]

#===========================================================================

if __name__ == '__main__':
	Main()

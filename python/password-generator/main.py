#! /usr/bin/python

import random

vowelList=[ "a", "e", "i", "o", "u", "a", "e", "i", "o", "u", "a", "e", "i", "o", "u", "y" ]
prefixList=["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z", "bl", "bn", "br", "bw", "bz", "ch", "cl", "cr", "cs", "cw", "cz", "dr", "fl", "fn", "fr", "gl", "gr", "gh", "gn", "gw", "kl", "kr", "kv", "kw", "mn", "ph", "pl", "pr", "pw", "rh", "sc", "sh", "sk", "sl", "sm", "sn", "sp", "st", "sw", "tr", "vl", "vr", "wh", "wr", ]
suffixList=["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z", "bl", "ch", "ck", "ct", "dl", "dn", "gh", "lb", "lc", "ld", "lf", "lg", "lk", "lm", "ln", "lp", "mp", "nc", "nd", "ng", "nk", "nv", "nx", "pf", "pl", "rc", "rd", "rf", "rg", "rj", "rk", "rm", "rn", "rp", "sh", "sc", "sk", "sp", "th", "zm", "zn", "bs", "cs", "ds", "fs", "gs", "ks", "ls", "ms", "ns", "ps", "rs", "ts", "vs", "ws", "kt", "lt", "nt", "pt", "rt", "st", ]

def generatePassword(password_len):
	password = ""
	for i in range(password_len):
		password += random.choice(prefixList)
		password += random.choice(vowelList)
		password += random.choice(suffixList)
		if len(password)>password_len:
			break

	return password

print generatePassword(8)
print "Use 'genpassword' to generate a better password"

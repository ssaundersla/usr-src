#! /usr/bin/python

import optparse
import os
import sys
import time
from datetime import datetime

import locale
locale.setlocale(locale.LC_ALL, '')

#===============================================================================
##	Command-line tool to scan a folder hierarchy and report 'stinkers' --
##	folders which are taking a large amount of disk space and thus are
##	candidates for deletion. This tool is reasonably useful right now. I
##	also have a few placeholders and non-completed elements which allow
##	for incorporating the age of the files in a directory as part of the
##	calculation of the folder's 'stinkiness.'
##
##		Usage: stinkers.py [options] rootpath
##
##	The most important option is probably the minimal size of folders
##	to include in the report. Folders smaller than the minimum will
##	not be included in the report. See GetCLOptions() for the default.
##
##	@todo The report leaves a lot to be desired. It would be easier to
##	use if the list were hierarchical as well as sorted by size. That is,
##	it would be good to see a large folder with all of its subfolders
##	listed directly under it.
##
##	@todo Do something with age of files?
#
def Main():
	(opt, path) = GetCLOptions()
	opt.rootPathLength = len(path)
	folderMap = GetFolderMap(opt,path)
	for k, folder in folderMap.iteritems():
		folder.addSelfToParent()
	rootFolder = folderMap[path]
	rootFolder.computeFolderSize()
	if opt.isFlat:
		PrintFlatReport(opt,folderMap)
	else:
		PrintHierarchyReport(opt,rootFolder)

#===============================================================================
##	Represents one folder (directory) in the hierarchy which starts with
##	the root folder specified on the command line (@see GetCLOptions()).
##	The attributes of a Folder deal with the folder's 'stinkiness,' or
##	level of desirability for deletion.
#
class Folder():

	lineFormat="%-80s %20s"
	maxFolderSize=0
	today = datetime.fromtimestamp(time.time())

	def __init__(self, path, parent):
		self.path = path
		self.parent = parent
		self.size = 0
		self.modifyAge = 1000000
		self.accessAge = 1000000
		self.ageOfYoungestFileInDays = 0
		self.childList=[]

	def __repr__(self):
		return locale.format("%d", self.size, grouping=True)
		# return str(self.size)

	#==========================================================================
	## Add a file to the folder. This means adding its size, its modification
	## date, and its access date. The total size of the folder is the total
	## size of the files inside it (at this point). The age of the folder is
	## the age of the newest file in the folder.
	##
	## Note that the size calculation only involves the files immediately
	## inside the folder. The size of subfolders is incoprorated into the
	## total by the computeFolderSize() method.
	##
	## @param filename the name (not the full path) of the file
	#
	def addFile(self,filename):
		fp = os.path.join(self.path, filename)
		try:
			statinfo = os.stat(fp)
			self.size += statinfo.st_size
			diff = Folder.today - datetime.fromtimestamp(statinfo.st_mtime)
			fileAgeInDays = diff.days
			if fileAgeInDays < self.modifyAge:
				self.modifyAge = fileAgeInDays
			diff = Folder.today - datetime.fromtimestamp(statinfo.st_atime)
			fileAgeInDays = diff.days
			if fileAgeInDays < self.accessAge:
				self.accessAge = fileAgeInDays
		except:
			pass

	#==========================================================================
	## Add a folder to the subfolder list of its parent folder. The point of
	## this method is to create the hierarchy of folders.
	#
	def addSelfToParent(self):
		if self.parent != None:
			self.parent.childList.append(self)

	#==========================================================================
	## Computes the size of the folder, including all of its subfolder
	## contents. This method recursively computes the size of all of this
	## folder's subfolders.
	##
	## This method also computes the Folder.maxFolderSize parameter used
	## by formatting.
	#
	def computeFolderSize(self):
		if len(self.childList)>0:
			for child in self.childList:
				child.computeFolderSize()
		for child in self.childList:
			self.size += child.size
		if self.size > Folder.maxFolderSize:
			Folder.maxFolderSize = self.size

	#==========================================================================
	## Sort the subfolder list by size. This method sorts all of this folder's
	## subfolders by size, then recurses into the subfolders to do the same
	## thing.
	#
	def sortChildren(self):
		if len(self.childList)>0:
			t = sorted(self.childList, key=lambda f: f.size)
			t.reverse()
			self.childList = t
		for child in self.childList:
			child.sortChildren()

	#==========================================================================
	## Returns true if this folder qualifies as a 'stinker'. The folder must
	## exceed the size threshhold and the age threshhold if the age is set.
	##
	## @param opt the command line options
	#
	def isStinker(self,opt):
		minSize = GetSize(opt.size)
		minAge = GetAge(opt.age)
		if self.size < minSize:
			return False
		if minAge > 0:
			if opt.ageType == 'access':
				if self.accessAge < minAge:
					return False
			if opt.ageType == 'modify':
				if self.modifyAge < minAge:
					return False
		return True

	#==========================================================================
	## Print the names of the folders which qualify as 'stinkers'. This
	## method is used by the hierarchical report. It recursively lists
	## folders, indenting by the user-specified number of spaces for each
	## level of subfolder.
	##
	## @param opt the command line options
	## @param n the level of the folder in the hierarchy (0=root)
	#
	def printStinkers(self,opt,n):
		if self.isStinker(opt):
			if n<1:
				print Folder.lineFormat % ( self.path, str(self) )
			else:
				partialPath = self.path[opt.rootPathLength:]
				partialPathList = partialPath.split(os.sep)
				partialPathList = partialPathList[n-1:]
				partialPath = os.sep.join(partialPathList)
				print Folder.lineFormat % ( n*opt.indentation*" "+partialPath, str(self) )
			if len(self.childList)>0:
				for child in self.childList:
					child.printStinkers(opt,n+1)

#===============================================================================
##	Generate and print a hierarchical report. This report will list the
##	folders hierarchically in descending order of total size of contents
##	at each level of the hierarchy.
#
def PrintHierarchyReport(opt,rootFolder):
	folderSizeWidth = len(str(Folder.maxFolderSize))
	folderSizeWidth += 2 + (folderSizeWidth / 3) # for the comma separators
	Folder.lineFormat = "%%-%is %%%is" % (opt.filenameWidth,folderSizeWidth)
	rootFolder.sortChildren()
	rootFolder.printStinkers(opt,0)

#===============================================================================
##	Generate and print a report from the `folderMap`. This report will list
##	the folders as path names in descending order of total size of contents.
#
def PrintFlatReport(opt,folderMap):
	folderSizeWidth = len(str(Folder.maxFolderSize))
	folderSizeWidth += 2 + (folderSizeWidth / 3) # for the comma separators
	Folder.lineFormat = "%%-%is %%%is" % (opt.filenameWidth,folderSizeWidth)
	sortedList = sorted(folderMap.values(), key=lambda f: f.size)
	sortedList.reverse()
	for folder in sortedList:
		if folder.isStinker(opt):
			print Folder.lineFormat % ( folder.path, str(folder) )

#===============================================================================
##	Generate the `folderMap` -- the map of path names to Folder objects.
##	The folderMap will start out with each Folder populated with the
##	pathname of the Folder and the total size of the non-directory files
##	which are directly contained by the directory.
#
def GetFolderMap(opt,rootPath):
	folderMap={}
	topLevelFolder = Folder(rootPath,None)
	folderMap[rootPath]=topLevelFolder
	for rootFoldername, subFoldernameList, subFilenameList in os.walk(rootPath):
		parent = folderMap[rootFoldername]
		for subFoldername in subFoldernameList:
			k = os.path.join(rootFoldername,subFoldername)
			thisFolder = Folder(k,parent)
			folderMap[k]=thisFolder
		for subFilename in subFilenameList:
			thisFolder = folderMap[rootFoldername]
			thisFolder.addFile(subFilename)
	return folderMap

#=============================================================================
## Extract size from a string with numbers and units.
#
def GetSize(inputParam):
	l = len(inputParam)
	qty = inputParam[0:l-1]
	units = inputParam[l-1:l]
	m=1
	if units == 'k' or units == 'K': m=1000
	if units == 'm' or units == 'M': m=1000000
	if units == 'g' or units == 'G': m=1000000000
	if units == 't' or units == 'T': m=1000000000000
	return int(qty)*m

#=============================================================================
## Extract age (in days) from a string with numbers and units.
#
def GetAge(inputParam):
	l = len(inputParam)
	qty = inputParam[0:l-1]
	units = inputParam[l-1:l]
	m=1
	if units == 'd' or units == 'D': m=1
	if units == 'w' or units == 'W': m=7
	if units == 'm' or units == 'M': m=31
	if units == 'y' or units == 'Y': m=366
	return int(qty)*m

#===========================================================================

def GetCLOptions():
	parser = optparse.OptionParser( usage="%prog [options] args", version="1.0" )
	parser.add_option(
		"--size",
		dest="size",
		default="100k",
		metavar='100k',
		help="Minimal size for a folder to qualify as a stinker"
	)
	parser.add_option(
		"--age",
		dest="age",
		default="0d",
		metavar='0d',
		help="Minimal age for a folder to qualify as a stinker"
	)
	parser.add_option(
		"--ageType",
		dest="ageType",
		choices=["access","modify"],
		default="access",
		metavar='access|modify',
		help="Type of age to scan for"
	)
	parser.add_option(
		"--indent",
		dest="indentation",
		type="int",
		default=0,
		metavar='0',
		help="Amount of indentation for each folder level"
	)
	parser.add_option(
		"--width",
		dest="filenameWidth",
		type="int",
		default=60,
		metavar='60',
		help="Amount of space allocated for file names"
	)
	parser.add_option(
		"--flat",
		action="store_true",
		dest="isFlat",
		default=False,
		help="List folders by size instead of hierarchy"
	)
	parser.add_option(
		"--manual",
		action="store_true",
		dest="printManual",
		default=False,
		help="Print the manual for this command"
	)

	(options, args) = parser.parse_args()

	if options.printManual:
		print theManual
		sys.exit(1)

	if len(args) != 1:
		print "Usage: stinkers [opts] directory"
		sys.exit(1)

	return (options, args[0])

#===========================================================================
theManual="""

Command-line tool to scan a folder hierarchy and report 'stinkers' --
folders which are taking a large amount of disk space and thus are
candidates for deletion. This tool will optionally also check the age
of the files in a directory as part of the calculation of the folder's
'stinkiness.'

	Usage: stinkers.py [options] rootpath

The most important option is probably the minimal size of folders
to include in the report. Folders smaller than the minimum will
not be included in the report. See GetCLOptions() for the default.

"""[2:-2]
#===========================================================================

if __name__ == '__main__':
	Main()

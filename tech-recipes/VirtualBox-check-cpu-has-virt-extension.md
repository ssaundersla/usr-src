Linux Tip: How to Tell if Your Processor Supports VT

Virtualization Technology (VT) is a set of enhancements to newer processors that improve performance for running a virtual machine by offloading some of the work to the new cpu extensions. Both AMD and Intel have processors that support this technology, but how do you tell if your system can handle it?

It's quite simple: We'll need to take a peek inside the /proc/cpuinfo file and look at the flags section for one of two values, vmx or svm.

    vmx – (intel)
    svm – (amd)

You can use grep to quickly see if either value exists in the file by running the following command:

    egrep '(vmx|svm)' /proc/cpuinfo

or

	grep --color vmx /proc/cpuinfo
	grep --color svm /proc/cpuinfo

If your system supports VT, then you'll see vmx or svm in the list of flags. My system has two processors, so there are two separate sections:

    flags           : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm syscall nx lm constant_tsc pni monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr lahf_lm
    flags           : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm syscall nx lm constant_tsc pni monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr lahf_lm

VT technology can still be disabled in your computer's BIOS, however, so you'll want to check there to make sure that it hasn't been disabled. The flags in cpuinfo simply mean that your processor supports it.

From my research, VT is required in order to run 64-bit guests under the free VMware server for linux so it would logically follow that if you can do so, VT is enabled.

32-bit VT is not enabled by default under VMware server. If you want to enable it, you need to add the following line to your *.vmx file for your virtual machine:

    monitor_control.vt32 = TRUE

VMware does not recommend that you use VT for 32-bit guests, because they say it will actually hurt performance.

VirtualBox – Shrink your VDI images.
June 23, 2008

Often when you install a VM in virtualbox you’ll notice that initial size of the VM image would be more or less equal to the disk space actually used in the VM. However with time, as you play around, you will find that the size of VM image would always keep on increasing. The disk space actually used would be far lesser than the VM image size. We would try to compress the VM image to the space actually used up inside the VM.

Pre-condition – The image that we are going to shrink should have been dynamically expanding type, when you created the disk very first time. This is explained for a windows VM. Theoretically should work on other VMs also.

Ok, Lets get started.

We’ll need the following tools:

1. http://www.feyrer.de/g4u/nullfile-1.02.exe : This tool zeroes out free space, which our next tool compresses. For Linux based OS, search for a file, zerospace.c, which you’ll have to compile yourself.

2. VBoxManage : This tool is the command line management tool that ships with VirtualBox. Whatever you can do with the GUI, can be done by this.

Ok now.

1. First boot into your VM. Defragment your drive at least 2 times. The easiest way is to bring up the Start menu and type “defrag”, the link to Disk Defragmenter will appear.

2. Copy the tool, nullfile mentioned above to the VM and run it. A simple double click should do it.

3. Now shutdown the guest. Open a terminal in the VM image directory. Most probably /home/vm/VirtualBoxVDisks/Vista_Dev.vdi

4. Run our final command, We should be done after this.
VBoxManage modifyvdi /home/vm/VirtualBoxVDisks/Vista_Dev.vdi compact
or
sudo VBoxManage clonehd /home/vm/VirtualBoxVDisks/Vista_Dev.vdi /media/ext3/Vista_Dev.vdi
sudo VBoxManage clonehd /home/vm/VirtualBoxVDisks/Vista_Dev.vdi /media/ntfs/Vista_Dev.vdi

That’s it, It will take some time, and you’ll get your tiny, shiny, compressed VM image.

-------------

Andy Says (April 15, 2009):

Apparently, using the clonehd command will shrink your vdi when it copies it, so that should be the simplest workaround out there.
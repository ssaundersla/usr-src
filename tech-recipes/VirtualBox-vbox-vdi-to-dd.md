To clone a VDI in VirtualBox:

VBoxManage clonehd golden-master.vdi tst.vdi



-------------------------------------------------------------------------------

You can convert a VDI into a disk image with the VBoxManage tool. This command clones a registered virtual disk image to another image file. If you want to convert your virtual disk to another format, this is the official VirtualBox tool to use[*].

VBoxManage clonehd file.vdi output.img --format RAW

    If you're using a dynamic VDI, and you have an older version of VirtualBox, clonehd may not operate properly. VBoxManage's internalcommands tool includes converttoraw, which can convert a dynamic VDI into a raw disk image (source)[+].

VBoxManage internalcommands converttoraw file.vdi output.img

But... that output IMG file isn't an ISO image, and the OS that's installed will not be configured to run from a bootable CD/DVD. You can "burn" (write) the IMG onto a hard drive, and it might boot on bare hardware (eg not in a virtual machine). But it might not, because the OS installed on that IMG is expecting to see the virtual hardware that VirtualBox provides, and you're booting it on real hardware that it isn't expecting.

Some versions of Windows do not handle this situation well; some Linux distributions do. It is sometimes possible to configure an OS (beforehand or afterwards) to migrate it from one environment to the other like this, but specific steps depend completely on the OS being migrated.

On Windows, you may need to specify the full path to the program:

"C:\Program Files\Sun\VirtualBox\VBoxManage.exe" [...]

Add C:\Program Files\Sun\VirtualBox to your PATH to use the short version.

[*] I'm assuming the "--format RAW" option will convert to a standard disk image, as if you'd used the dd command on a physical harddrive. But frankly, I haven't found any documentation that backs this up, so be aware this may not be correct.

-------------------------------------------------------------------------------
How to compact a VirtualBox virtual disk image (VDI)

Windows XP (guest):
	Run Start -> Accessories -> System Tools -> Disk Defragmenter
	Download and run nullfile-1.02.exe
Ubuntu 8.04 (host):
	Run: VBoxManage modifyvdi winxp.vdi compact

VBoxManage modifyhd         <uuid>|<filename>
                            [--type normal|writethrough|immutable|shareable|
                                    readonly|multiattach]
                            [--autoreset on|off]
                            [--compact]


VBoxManage modifyhd Win_XP_dev.vdi --compact


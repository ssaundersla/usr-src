# Runbook for VirtualTourist.com

Requests to virtualtourist.com are now handled by a virtual server running on AWS (Amazon Web Services). It uses Apache httpd only to redirect requests to smartertravel.com and other sites. Regardless of the virtual nature of the server, the site is still running a traditional Linux operating system with Apache just as it would if it were a physical server in a local data center. There are some differences though in how the server must be updated if updates become necessary.

This document is a description of the AWS site with a limited amount of background on AWS itself. The bulk of this document assumes familiarity with AWS terms and concepts. Links are provided if you would like more detail on a topic.

The information here is current as of 2017-02-28. Any references to the 'current' configuration or to the 'current' AWS UI assume this date. For the actual current state you will need to log into the AWS console and will need to understand what you are seeing. This links in the sections below can help you with the necessary background. And fortunately the AWS UI and terminology is very straightforward.

Please note that Caesar is in possession of all virtualtourist.com passwords. If you need to log in to a development instance or to the AWS dashboard please see Caesar about access.

## Apache configuration

The Apache httpd configuration is in the `/etc/httpd` directory. The `httpd.conf` file in the `conf` subdirectory is the main configuration file, but you are more likely to be modifying redirection rules in one of the files in `/etc/httpd/conf.d`. All of the files in this directory whose names end in `.conf` are included in the configuration by default. You can add a file to this directory if you wish. Currently though, the directory contains several files which contain the redirect rules:

* The hotel-geo.conf file contains redirect rules for hotels in a specific geographic area, such as Paris. These, as in most of the configuration files, are mod_alias 'Redirect' directives.

* The hotel.conf file contains redirect rules for specific hotels, such as the King George Hotel in Paris. Hotel URLs from virtualtourist.com are redirected to TripAdvisor.

* The travel-guides.conf file contains redirect rules for the top 5,000 travel guides in virtualtourist.com and maps them to corresponding articles at smartertravel.com.

* The trippy.conf file contains rewrite rules for forum content. The virtualtourist.com forum URLs are being redirected to equivalent content at trippy.com. Please note that these are not Redirect directives as in the other files, although they do redirect traffic. The trippy.conf file makes use of `mod_rewrite` instead of `mod_alias` to handle the redirection.

* The autoindex.conf, notrace.conf, and userdir.conf files are part of the standard Apache startup configuration and have not been altered to create the redirection server instance. More precisely these files were automatically added during the update from Apache version 2.4.23 to Apache version 2.4.25. You can ignore these.

* The welcome.conf file is part of the standard Apache startup configuration, but also contains the default redirect rule for virtualtourist.com. If the URL in the incoming request does not match one of the patterns in one of the above files the default rule is to send the contents of `/var/www/html/index.html`

Note: The configuration files in this directory are processed in alphabetical order. Please see https://httpd.apache.org/docs/2.4/mod/core.html#include for more details. This means that if you add a file to this directory and the name of the file falls after 'welcome' alphabetically, the rules in your file will be ignored. It also means that if you create or edit a configuration file to contain a directive that affects later directives, such as `RewriteEngine On`, you may have to be mindful of how the name of the file falls in the alphabet.

## AWS servers

http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html

The Apache `/etc/httpd` directory is 'on' an AWS virtual server. For a general background on AWS and the various services used by the virtualtourist.com account the above link might be helpful. There are additional and more focussed links in the individual sections following.

Servers in AWS are presumed to exist only temporarily. The configuration is held in virtual machine image files (AMIs, or Amazon Machine Image). Instances (servers) are launched from AMIs. AMIs are created from instances. It would be a chicken-and-egg problem except that Amazon provides a number of pre-built AMIs that you can use to launch your first instance.

If changes to Apache are necessary, you will need to make the changes on a development server (again an AWS virtual server), test your changes, and then port your changed server configuration to the production server. The main purpose of this document is to describe how the porting step works. It's assumed that you already know how to make changes to Apache httpd. For a walkthough of the porting process, please see "Sample update to virtualtourist.com" below. If you do update the Apache configuration it would be a good idea to run `sudo yum update` at the same time to make sure the instance has the latest security patches. Obviously the tesing of the new configuration should happen after all of the yum updates complete successfully.

## Logging in to AWS

The virtualtourist.com EC2 configuration is composed of instances, AMIs, a load balancer, a target group, a launch confguration, and an auto-scaling group. Each of these will be described briefly, especially as they are used by virtualtourist.com. For more detail please use the links to review the AWS documentation.

To log in to the Virtualtourist AWS dashboard, navigate to `https://console.aws.amazon.com` and log in as `ITservices@virtualtourist.com`. From this web dashboard you can update the AWS configuration. This is how you will port your changes.

To log in to a development instance, see "Logging in to a development instance" below. This is how you will make and test your changes.

### Instances

http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Instances.html
http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-lifecycle.html

The AWS account for the virtualtourist.com site makes use of two different categories of instance. The only difference between the two categories of instance is in how they are used -- there is no architectural difference between them.

Virtualtourist.com instances are launched from the most recent AMI. A development instance is launched directly from the AMI and is then accessed via SSH. Developers SSH into this instance to make and test changes to the httpd.conf file. If the changes test well a new custom AMI is built from the development instance. The production instances are then built from this AMI.

The other category of instance is the production instance. The production instance is managed by a load balancer and an auto-scaling group. These are further described below. The lifecycle of the production instance is essentially:

1. AWS detects that the production instance is not running
2. AWS builds a new instance from the custom AMI and launches it
3. AWS instructs the load balancer to direct traffic to the new instance

The production instance is presumed to be ephemeral. A new instance will be created from the AMI in the event that the running instance fails. The target group and the auto-scaling group control how this happens.

So the update cycle is as follows:

1. Launch a development instance from the most recent AMI
2. Edit and test the development instance
3. Build an AMI from the development instance
4. Launch a production instance from the AMI

See "Sample update to the development instance" for a walkthrough of steps 1 and 2. See "Sample update to virtualtourist.com" for a walkthrough of steps 3 and 4.

Note: Building an AMI from a running instance will reboot the instance.

Note: Amazon charges one full hour of time each time an instance is launched from a stopped or terminated state. This means that if you relaunch an instance twenty times in one hour you will be charged for twenty hours, not one hour. Simply rebooting an instance however doesn't start a new instance billing hour.

### AMI

http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html

The Virtualtourist AMI (Amazon Machine Image) is built from the development instance and is in turn used to build the production instance. If the virtualtourist.com site requires a configuration change of some kind, the AMI must be changed. More precisely, a new AMI must be created and the launch configuration changed.

The current Virtualtourist production AMI is `VtRewrite-v7`. Any other AMIs in the dashboard list should be considered obsolete. It is the `VtRewrite-v7` AMI which will be loaded in order to produce another running instance. Once again, the production instance is presumed to be ephemeral. The load balancer will create a new instance from the AMI if anything happens to the running instance.

### Load balancer

https://aws.amazon.com/elasticloadbalancing/
http://docs.aws.amazon.com/elasticloadbalancing/latest/application/application-load-balancers.html

The load balancer is an 'application' load balancer as opposed to a 'classic' load balancer. Either would work equally well for a simple application like the virtualtourist redirection server. The load balancer is at `VtRewrite1-ALB-154133890.us-east-1.elb.amazonaws.com`. When the switchover was made from the data center to AWS, this address was be configured as a CNAME for virtualtourist.com, www.virtualtourist.com, forum.virtualtourist.com, and members.virtualtourist.com. AWS load balancers do not have static IP addresses.

http://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_server-certs.html

The load balancer terminates SSL and routes traffic to the production instances, although currently there is only one production instance at a time. The SSL certificate for virtualtourist.com has already been installed and does not expire until March of 2018, so there should be no need to modify it.

If it does become necessary to replace the SSL certificate, upload a new SSL certificate to AWS IAM and edit the HTTPS listener on the load balancer:

	1. Display the Load Balancers list.
	2. Click the current load balancer (VtRewrite1-ALB)
	3. Find the HTTPS listener in the Listeners list and click the Edit link
	4. Click the "Upload a new SSL certificate" button
	5. Enter a name for the new SSL certificate
	6. Cut and paste the PEM-encoded private key and certificate
	7. Click the Save button

You should not need to alter the load balancer to update the virtualtourist.com Apache configuration.

### Target group

http://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-target-groups.html

An AWS 'target group' works with a load balancer to define the relationship between the load balancer and the instances the load balancer manages. In particular the target group defines the health checks used to determine whether or not an instance has failed. The target group also, in the virtualtourist.com configuration, is the link between the load balancer and the auto-scaling group. The instance(s) included in the target group are launched and terminated according to the rules set up in the auto-scaling group. Without the auto-scaling group, the instances would have to be manually added and removed from the target group.

You should not need to alter the target group to update the virtualtourist.com Apache configuration.

### Auto-scaling group

http://docs.aws.amazon.com/autoscaling/latest/userguide/AutoScalingGroup.html

The load balancer uses the parameters in the auto-scaling group to determine under what circumstances to create extra instances or to terminate running instances. In the case of virtualtourist.com the auto-scaling group rules are very simple: One instance should exist at all times. If the instance fails to respond to HTTP queries, it should be terminated and a new one launched to take its place.

An auto-scaling group uses a 'launch configuration' to determine how a new instance will be created. The auto-scaling group must be updated with a new launch configuration in order to update the Apache configuration.

Note on performance: All the AWS instance is doing is forwarding requests to other sites. After some extensive work testing the site using JMeter, we determined that the single instance is capable of handling more than ten times the peak historical capacity of virtualtourist.com. Therefore there is no multi-instance scaling policy. The auto-scaling group is configured to keep one instance running.

### Launch configuration

http://docs.aws.amazon.com/autoscaling/latest/userguide/LaunchConfiguration.html

The launch configuration determines which AMI will be used to create a new instance. There are other parameters in a launch configuration, but the AMI parameter is the one item for the virtualtourist.com site that you will change when you make changes to the Apache configuration.

A new launch configuration must be created in order to update the Apache configuration. You can simply copy the existing launch configuration and change the AMI. See "Sample update to virtualtourist.com" below for a description. The current launch configuration is `VtRewrite1-lc-v7`. Any other launch configurations in the dashboard list should be considered obsolete.

Note: Make sure to set the security group of the launch configuration to the http-https-ssh security group. I made the mistake once of letting the UI set up a new security configuration and in so doing rendered the resulting instances unable to handle HTTP or HTTPS requests. The critical step is pointed out in the "Sample update to virtualtourist.com" section below.

## Build notes

These are some notes on how the original virtualtourist.com development instance was built in the first place. These might be useful in debugging troublesome issues.

Security settings for an instance must be established when the instance is created. So the first step in creating the first development instance was to create a security group to encapsulate these settings. The security group for the development instance includes all IP addresses and is limited to HTTP, HTTPS, and SSH. You can view this security group in the dashboard. Just view the instance details and click on the security group parameter.

The first development instance was built from the standard Amazon Linux AMI. The latest version of this AMI is available in the AWS marketplace. From there, Apache httpd was installed via yum:

	sudo yum update -y
	sudo yum install -y httpd24
	sudo service httpd start
	sudo chkconfig httpd on
	sudo service httpd status
	sudo cp /etc/httpd.conf /etc/httpd.conf.orig
	sudo mv /etc/httpd/conf.d /etc/httpd/conf.d.orig
	sudo mkdir /etc/httpd/conf.d

With the configuration files thus backed up the only remaining step was to build the custom redirection rules. These have all been placed in `/etc/httpd/conf.d` as described above. The only other configuration of the development instance has been to place a default page in the `/var/www/html` directory.

## Logging in to a development instance

You log into a development instance just as you would log into any other server via SSH. AWS instances disable SSH password access by default. Instead, you will use the key pair set up for the instance. Caesar can provide you with the private key.

So first, obtain the SSH key if you do not already have it. Next, launch a development instance from the most recent AMI. Once you have the instance and the key, either run an SSH command with the key explicitly specified, e.g.

	ssh -i /path/to/key ec2-54-152-65-15.compute-1.amazonaws.com

or set up a configuration in SSH like this:

    Host ec2
        User ec2-user
        Hostname ec2-54-197-196-222.compute-1.amazonaws.com
        Port 22
        IdentityFile /path/to/key

and log in via `ssh ec2`. Once logged in you can modify the httpd configuration files and restart the httpd daemon as usual. You would test changes by browsing to `ec2-54-197-196-222.compute-1.amazonaws.com`.

## Sample update to the development instance

This is a walkthrough of updating and testing the development instance. It assumes that you have the SSH private key for the instance and are using SSH to log into the instance. First of course, log into the EC2 dashboard.

1. Launch a development instance

	1. Display the AMI list
	2. Right-click on the most recent AMI and select the Launch option
	3. Allow several minutes for the instance to launch

2. SSH to the development instance

	1. Display the instance list
	2. Get the Public DNS name of the instance you just launched
	3. SSH to the DNS name
	4. Make and test changes as you normally would

## Sample update to virtualtourist.com

This is a walkthrough of updating the virtualtourist.com site after updating and testing the development instance. First of course, log into the EC2 dashboard.

1. Create a new AMI from the development instance

	1. Display the Instances list
	2. Right-click the development instance you just launched and edited
	3. Select Image > Create Image
	4. Enter the name for the new image (e.g. VtRewrite-v7) and accept the defaults for the rest
	5. Allow several minutes for the new AMI to appear on the AMI list

2. Create a new launch configuration to use the new AMI

	Launch configurations currently cannot be modifed. You 'modify' a
	launch configuration by copying an existing one to a new one.

	1. Display the Launch Configurations list.
	2. Click on the current launch configuration
	3. Click on the "Copy launch configuration" button
	4. Click the "Edit AMI" link. If the new AMI is not listed, delete any text in the search box. The search box acts as a filter and may have been pre-filled with data from the last time you created a new launch configuration
	5. Select the new AMI you just created. A confirmation window will open. Select the "Yes" option and click the "Next" button.
	6. Keep the t2.micro option and click the "Next: Configure details" button
	7. Enter the name (e.g. VtRewrite1-lc-v7)
	8. Click the "Next: Add Storage" button. Accept the defaults.
	9. Click the "Next: Configure Security Group" button. This is the critical step mentioned above in the 'Launch configuration' section: Select the http-https-ssh security group.
	10. Click the "Review" button
	11. Click the "Create launch configuration" button
	12. A confirmation window will open. Make sure you still have the key pair and click the "Create launch configuration" button

3. Update the auto-scaling group to use the new launch configuration

	Auto-scaling groups *can* be modified. You will probably not need
	to create a new auto-scaling group. Rather, you will just edit the
	existing one.

	1. Display the Auto Scaling Groups list
	2. Click on the current auto-scaling group (VtRewrite1-asg)
	3. Click the Edit button
	4. Select the new launch configuration and click the Save button

4. Terminate the running production instance

	This will cause AWS to build a new production instance from the new AMI.

	1. Display the Instances list
	2. Right-click the production instance. It's the one without the name.
	3. Select Instance State > Terminate
	4. Allow several minutes for the new instance to boot and run

# AWS

https://wblinks.com/notes/aws-tips-i-wish-id-known-before-i-started/

http://www.infoworld.com/article/2613845/cloud-computing/free-amazon-web-services----and-how-to-make-the-most-of-them.html

## Upload an SSL certificate to IAM

It's possible also to get CloudFront to use an SSL certificate that you have uploaded to IAM, but the process is not at all straightforward. I attempted to do this when it became necessary to replace the saturn prototype SSL certificate because it had expired. After many false starts with the AWS GUI, I gave up and used the AWS CLI. Fortunately, there are a lot of Stack Overflow questions which describe how to accomplish this end with the AWS CLI.

I had installed the AWS CLI on the Jenkins server in order to facilitate future automation scripts, so this was relatively easy to do using the Jenkins server. Of course this meant uploading the PEM files to the Jenkins server. I created a subdirectory in the jenkins user home directory named `aws-cli` to hold the files and then uploaded them:

	scp /my/local/path/to/cert1.pem jenkinsj:aws-ssl/cert1.pem
	scp /my/local/path/to/chain1.pem jenkinsj:aws-ssl/chain1.pem
	scp /my/local/path/to/privkey1.pem jenkinsj:aws-ssl/privkey1.pem

After that it was a matter of logging in to the Jenkins server as jenkins and running some AWS CLI commands. I started by listing the certificates:

	aws iam list-server-certificates

The next is the 'money' command: The one that uploaded the certificate files so that AWS would add a certificate to the list available to CloudFront:

	aws iam upload-server-certificate \
	--server-certificate-name saturn.prototype.mooreboeck.com3 \
	--certificate-body file://cert1.pem \
	--certificate-chain file://chain1.pem \
	--private-key file://privkey1.pem \
	--path /cloudfront/

Note that it may be necessary to upload the SSL certificate twice: Once for use with the ELB as described above, and once for use by CloudFront. Such action was not necessary in my case since I had already gotten the ELB to upload and use the certificate. But I did check the ELB SSL certificate list and the certificate I had uploaded for CloudFront appeared in it. So it may be that postings to the effect that you have to upload these certificates twice may be incorrect.

Start by using the above command to upload a certificate for CloudFront, since that definitely does not work any other way that I have found. If the SSL certificate appears in the list for the ELB, you're done. If not, the following command will upload the same SSL certificate for use by an ELB:

	aws iam upload-server-certificate \
	--server-certificate-name saturn.prototype.mooreboeck.com4 \
	--certificate-body file://cert1.pem \
	--certificate-chain file://chain1.pem \
	--private-key file://privkey1.pem \
	--path /

I was not able to replace the old expired SSL certificate with the new one. I had to name it saturn.prototype.mooreboeck.com2. In order to delete the old certificate, AWS provides a delete command:

	aws iam delete-server-certificate --server-certificate-name saturn.prototype.mooreboeck.com

The result of this action was: "An error occurred (DeleteConflict) when calling the DeleteServerCertificate operation: Certificate: ASCAISOJZ3YAJDL2GQR36 is currently in use by arn:aws:elasticloadbalancing:us-west-1:877375690873:loadbalancer/app/saturn-prototype-elb-1/0dcea124b5528f8e. Please remove it first before deleting it from IAM." So there is a guard against deleting a certificate that is in use. I did however manage to use the delete command successfully to remove some of my test uploads.


## Set up an auto-scaling ELB-fronted service

To set up the auto-scaling AWS configuration, we'll start by building the simpler pieces and conclude by assembling the simpler pieces into the more complex ones. All of these tasks take place in the AWS EC2 console.

1. Create a security group

	1. Click on Security Groups in the left-column menu
	1. Click Create Security Group
	1. Give the group a name
	1. Click the Add Rule button to add a rule to the group. Add rules for SSH, HTTP, and HTTPS. This list can vary as needed.
	1. Click the Create button

1. Create a key pair

	1. Click on Key Pairs in the left-column menu
	1. Click Create Key Pair
	1. Give the pair a name. Make the name file system-friendly, because it will be used to create a file. See step 5.
	1. Click the Create button
	1. AWS will download to you the private key in a file named {name}.pem.txt where {name} is the name you gave your key. Save this key to `~/.ssh` or wherever you typically keep SSH keys.

1. Create a instance, configure it, and build an AMI from it

	1. Click on AMIs in the left-column menu
	1. Select an AMI and launch an instance from it. This is a multi-step process that should already be familiar.
	1. Log into the instance and configure it, installing Apache and other software as needed. This is a multi-step process that should already be familiar.
	1. Click on Instances in the left-column menu
	1. Control-click, select Image > Create Image
	1. Give the image a name and click the Create button
	1. Terminate the instance (control-click the instance name and select Instance State > Terminate)

1. Create a target group

	1. Click on Target Groups in the left-column menu
	1. Click Create Target Group
	1. Give the group a name
	1. Click the Create button

1. Create a launch configuration (more complex)

	1. Click on Launch Configurations in the left-column menu
	1. Click Create Launch Configuration
	1. Select an AMI. You will probably have to click My AMIs to list the AMI you just created. If the new AMI is not listed, delete any text in the search box. The search box acts as a filter and may have been pre-filled with data from the last time you created a new launch configuration.
	1. Select the new AMI you just created. A confirmation window will open. Select the Yes option and click the Next button.
	1. Select the appropriate machine type option and click the Next: Configure details button
	1. Give the launch configuration a name
	1. Click the Next: Add Storage button. Accept the defaults.
	1. Click the Next: Configure Security Group button. Select the security group you just created.
	1. Click the Review button
	1. Click the Create launch configuration button
	1. A confirmation window will open. Make sure you still have the key pair and click the Create launch configuration button

1. Create an autoscaling group

	1. Click on Auto Scaling Groups in the left-column menu
	1. Click Create Auto Scaling Group
	1. Select "Create an Auto Scaling group from an existing launch configuration" and select the launch configuration you just created.
	1. Give the group a name and choose a starting size (number of instances to launch)
	1. Select a subnet (availability zone) for the auto-scaling group and click the Next Configure Scaling Policies button.
	1. Scaling policies are a topic all by themselves. For now, select " Keep this group at its initial size" and click the Next: Configure Notifications button.
	1. Notifications are also a topic all by themselves. For now, just click the Next: Configure Tags button.
	1. Add tags as needed and click the Review button.
	1. Click the Create Auto Scaling Group button

Within a few seconds, the aut-scaling group will detect that an insufficient number of instances of your AMI are running and will start launching new instances. You can track this progress by looking at the Instances menu item or looking at the auto-scaling group's Activity tab or Instances tab.

1. Attach the target group to the autoscaling group

	1. Click on Auto Scaling Groups in the left-column menu.
	1. Select the auto-scaling group you just created.
	1. In the Details tab at the bottom, look for Target Groups. It should be blank. Click the Edit button for the Details tab.
	1. Click the Target Groups popup to select the target group you just created and click the Save button.

1. Create a load balancer

	1. Click on Load Balancers in the left-column menu.
	1. Click Create Load Balancer.
	1. Select Application Load Balancer and click the Continue button.
	1. Give the load balancer a name
	1. Add one or more listeners (HTTP and/or HTTPS probably)
	1. Add at least two availability zones
	1. Click the Next: Configure Security Settings button
	1. If you added an HTTPS listener to the ELB, choose an SSL certificate. Leave the security policy at the default value. Click the Next: Configure Security Groups.
	1. Select the security group you just created and click the Next: Configure Routing button.
	1. Select the Existing Target Group option and select the target group you just created.
	1. Click the Next: Register Targets button.
	1. Click the Next: Review button.
	1. Click the Review button. This will create the load balancer.

Note: deleting the load balancer (AWS load balancers have to be deleted -- they can't simply be stopped) had no effect on the auto-scaling group. I deleted the load balancer and then terminated the instances associated with it. Seconds later two more instances were spun up.

## Get metadata

Run this on any instance: `curl http://169.254.169.254/latest/meta-data/`
For more info: http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-metadata.html
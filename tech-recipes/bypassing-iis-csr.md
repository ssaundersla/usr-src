2. Create the CSR and the private key
-------------------------------------

openssl req               \
  -new                    \
  -nodes                  \
  -newkey rsa:2048        \
  -keyout mb-key-2018.pem \
  -config mb-csr-2018.cnf \
  -out    mb-csr-2018.csr




			[req]
			default_bits = 2048
			prompt = no
			default_md = sha256
			req_extensions = req_ext
			distinguished_name = dn

			[ dn ]
			C=US
			ST=California
			L=Pasadena
			O=NATIONAL AERONAUTICS AND SPACE ADMINISTRATION
			OU=Europa Program
			CN = europa.nasa.gov
			emailAddress = philip.w.davis@jpl.nasa.gov

			[ req_ext ]
			subjectAltName = @alt_names

			[ alt_names ]
			DNS.1 = europa.nasa.gov
			DNS.2 = europa.jpl.nasa.gov
			No newline at end of file
























Bypassing IIS for a CSR
=======================

The normal process for generating an SSL certificate involves using IIS to generate a CSR, then sending that CSR to a CA to be signed (i.e., "buying a certificate"). The IIS step can be omitted if you can use openssl to generate the CSR. This can be advantageous if remoting in to the server on which IIS is located is a problem, as it usually is.

Note:
=====

As of 2011-09-?? RapidSSL is requiring an intermediate certificate.
Also, example.com will no longer work with www.example.com
But, www.example.com will still work with example.com


(A) Buying a certificate
========================

1. Create the CSR and the private key
-------------------------------------

openssl req -out csr.txt -new -newkey rsa:2048 -nodes -keyout private-key.pem -subj "/C=US/ST=TX/L=Round Rock/O=Serolab/OU=lab/CN=serolab.schuynet.net"


2.1 Get the CSR (csr.txt) signed by RapidSSL
--------------------------------------------

Offline: purchase from RapidSSL and retrieve the certificate. This step produces the cert.txt file and should also provide the intermediate-cert.crt file.

https://www.thesslstore.com/QuickLogin.aspx

While waiting for the certificate, you might as well prep a pair of files:

echo empty > intermediate-cert.crt
echo empty > cert.txt

2.2 Get the CSR (csr.txt) signed by Namecheap
---------------------------------------------

Offline: purchase from Namecheap and retrieve the certificate. This step produces the cert.txt file and should also provide the cacert.crt file.

https://www.namecheap.com/security/ssl-certificates/domain-validation.aspx

In Namecheap's interface, you create and confirm the order and *then*
sign in. Also, in order to actually order the certificate (after you
buy it), you need to go through the ordering process and then select
the "Manage SSL Certificates" item off of the "Hi stevesh" menu in the
far upper left. Then look for an "Activate Now" link, click it, and
you should get to the CSR-entry page. After you enter the CSR, you
pick an email address. Namecheap will invariably respond with "We are
unable to validate the settings of the approver email you have chosen"
to any email address. Just click the checkbox and move on.

While waiting for the certificate, you might as well prep the p7b file:

echo empty > cert.p7b


3.1 Convert to PKCS #12
-----------------------

openssl pkcs12 -export -out import-file.pfx -inkey private-key.pem -in cert.txt -certfile intermediate-cert.crt

password: x

3.2 Convert Namecheap files to PKCS #12
---------------------------------------

Namecheap sends PKCS7 files instead of individual cert files. To convert them to something usable, you have to split out the individual certificates, then edit them into separate files, then recombine them into a PKCS12 file with the private key:

	openssl pkcs7 -print_certs -in cert.p7b -out allcerts.txt
	cp allcerts.txt cert.txt
	cp allcerts.txt cacert.txt
	(edit)
	openssl pkcs12 -export -out import-file.pfx -inkey private-key.pem -in cert.txt -certfile cacert.txt
	rm allcerts.txt cert.txt cacert.txt

password: x

See
<https://support.globalsign.com/customer/portal/articles/1353601-converting-certificates>

4. Import the PKCS #12 file into IIS
------------------------------------

Versions of IIS vary, but all of them have an option for importing a certificate. Move the import-file.pfx file to the IIS server, run IIS, and import the file. Once the file is imported into IIS, set the bindings on the website as usual.

Note that you can also import the PKCS12 file into Windows by just double-clicking the file. I'm not sure what if anything this does, because I still had to explicitly import the file into IIS in order to get it to work.

Note that in at least some versions of IIS, you still have to import the intermediate certificate into Windows even though it's included in the PKCS12 file.



(B) Self-signing a certificate
==============================

1. Generate a 'CA'
------------------

openssl genrsa -des3 -out CA.key 2048

openssl req -new -key CA.key -x509 -days 3650 -out CA.crt -subj "/C=US/ST=CA/L=Valencia/O=Schuyler House/OU=CRO/CN=schuylerhouse.com"


2. Create the CSR and the private key
-------------------------------------

openssl req -out csr.txt -new -newkey rsa:2048 -nodes -keyout private-key.pem -subj "/C=US/ST=TX/L=Round Rock/O=Serolab/OU=lab/CN=serolab.schuynet.net"


3. Sign the CSR
---------------

openssl x509 -req -days 3650 -in csr.txt -CA CA.crt -CAkey CA.key -CAcreateserial -out cert.txt

openssl x509 -in cert.txt -text -noout


4. Convert the certificate, private key, and intermediate to PKCS #12
---------------------------------------------------------------------

openssl pkcs12 -export -out import-file.pfx -inkey private-key.pem -in cert.txt


5. Import the PKCS #12 file into IIS
------------------------------------

Versions of IIS vary, but all of them have an option for importing a certificate. Move the import-file.pfx file to the IIS server, run IIS, and import the file. Once the file is imported into IIS, set the bindings on the website as usual.

Note that double-clicking on the import-file.pfx file with prompt you to import it also, but this import wizard is going to import the file into Windows' stores. IIS is not smart enough to pick up a certificate from these stores -- you're best off importing the import-file.pfx file directly into IIS.




(C) Self-signing a certificate
==============================

This process works on Win2008

1. Generate a server key
------------------------

openssl genrsa -des3 -out server.key 2048

2. Create the CSR
-----------------

openssl req -new -key server.key -out server.csr -subj "/C=US/ST=TX/L=Round Rock/O=Serolab/OU=lab/CN=serolab.schuynet.net"

Remove the passphrase from the server certificate:

cp server.key server.key.org
openssl rsa -in server.key.org -out server.key

3. Generate the self-signed certificate
---------------------------------------

openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

4. Convert the certificate and private key to PKCS #12
------------------------------------------------------

openssl pkcs12 -export -out import-file.pfx -inkey server.key -in server.crt

5. Import the PKCS #12 file into IIS
------------------------------------

Versions of IIS vary, but all of them have an option for importing a certificate. Move the import-file.pfx file to the IIS server, run IIS, and import the file. Once the file is imported into IIS, set the bindings on the website as usual.

openssl x509 -in server.crt -text -noout

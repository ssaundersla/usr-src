I spent a lot of time using curl to diagnose connection issues. The 'curl' utility can be configured to log in to a site like the CMS server using user-supplied authentication tokens, but the syntax is finicky. I've gotten the following command to work reliably from both my laptop and on an EC2 server:

	curl -Ivvv -u 'mb:THE_PASSWORD' -k -H 'Accept: text/*;q=0.3, text/html;q=0.7, text/html;level=1, text/html;level=2;q=0.4, */*;q=0.5, application/json' https://s.rps.jpl.mooreboeck.com

curl -Ivvv -u 'mb:_underSc0re' -k -H 'Accept: text/*;q=0.3, text/html;q=0.7, text/html;level=1, text/html;level=2;q=0.4, */*;q=0.5, application/json' https://s.rps.jpl.mooreboeck.com


# Important images for SS

## Used constantly









# Important images for MB

## Used constantly

ssaunders/aws-cli                latest
postgres                         9.6

## TBD -- document these for use as a registry

registry                         2
joxit/docker-registry-ui         static

## TBD -- document these for use by MBCMS

phusion/baseimage                0.11
httpd                            2.4
alpine                           3.6
phusion/passenger-ruby24         1.0.1
ruby                             2.4.3

## TBD -- document these for use as documentation generation

asciidoctor/docker-asciidoctor   latest


## DELETE
```
aws-cli                          latest
mysql                            5.7
ssaunders/httpsinabox            snapshot
ssaunders/httpsinabox            spst-cd747135
ssaunders/httpsinabox            spst-6e41a82b
nginx                            latest
geminabox                        latest
postgres                         9.5
ssaunders/guide_rails_app        snapshot
ssaunders/guide_rails_app        spst-80be6933
ssaunders/guide_dev_db           snapshot
ssaunders/guide_dev_db           spst-80be6933
mesosphere/aws-cli               latest
garland/aws-cli-docker           latest
```

================================================================================

# Starting from scratch with Docker Toolbox

## Install

We start by installing Docker. Go to the following link (current as of
2016-07-02), download the installer for your OS, and follow the
instructions.

<https://www.docker.com/products/docker-toolbox>

The rest of these instructions assume that you are using MacOS.

## Build a 'machine'

Next we initialize a virtual machine to run Docker. First, halt any
running boot2docker instances. If you've just installed Docker for the
first this this is probably not applicable. Next, run the following:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-machine ls
docker-machine create --driver virtualbox default
docker-machine ls
docker-machine env
docker-machine ip
docker-machine status
docker-machine start
docker-machine status
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Add to /etc/hosts

This part is not necessary, but it makes life with Docker easier
later. The default Docker VM takes 192.168.99.100 as its IP address.
So set that in `/etc/hosts`:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
192.168.99.100  dockerhost
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With that change, you can point your web browser at
`http://dockerhost/...` to access web content running in containers.
Next add the following to your .bashrc file:

```bash
if [ -n "$(which docker-machine)" ]; then
  alias dm="docker-machine"
  if [ $(docker-machine status) = 'Running' ]; then
    eval $(docker-machine env)
  fi
fi
if [ -n "$(which docker-compose)" ]; then
    alias dcu="docker-compose up -d"
    alias dcd="docker-compose down -rmi=all"
fi
```

With that change, you can use docker commands right from your normal
Mac terminal window without having to log into the Docker VM first.

The rest of these instructions assume that you have done that.

================================================================================

# Starting from scratch, all

## Run hello-world

As usual, we start a new journey by saying hello to the world. Enter the following at a terminal prompt.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker run hello-world
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You should see a greeting from this very basic Docker container.

## Run Alpine, run some Unix commands

Now for something a little more complex.

Enter the following at a terminal prompt.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker run -it alpine /bin/sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You are now able to run commands inside the Docker container.

## Run Alpine, connect to local storage, test

Enter the following at a terminal prompt.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd ~/Desktop
mkdir tmp-docker-notes
cd tmp-docker-notes
docker run -it -v $(PWD):/home alpine /bin/sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the container, create a file in /home. On the host, see what's now
in `~/Desktop/tmp-docker-notes`

## Build a simple image from a Dockerfile

https://docs.docker.com/engine/userguide/containers/dockerimages/

Enter the following at a terminal prompt.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd ~/dev/docker/learn/01-alpine
docker build -t ssaundersla/alpine-l1:v1 .
docker images
cd ~/Desktop/tmp-docker-notes
docker run -it -v $(PWD):/home ssaundersla/alpine-l1:v1 /bin/sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Build a simple image from a Dockerfile, pt 2

Enter the following at a terminal prompt.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd ~/dev/docker/learn/02-alpine
docker build -t ssaundersla/alpine-l1:v2 .
docker images
cd ~/Desktop/tmp-docker-notes
docker run -it -v $(PWD):/home ssaundersla/alpine-l1:v2 /bin/sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The above fails because I mount the external directory right on top of the one I just built.

So...

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker run -it ssaundersla/alpine-l1:v2 /bin/sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker ps -a
docker rm ...
docker images
docker rmi ssaundersla/alpine-l1:v1
docker rmi ssaundersla/alpine-l1:v2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## run postgres, run client from laptop

In this lesson we install a database server and ...

Enter the following at a terminal prompt.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker run -d -p 5432:5432 --name cdb -e POSTGRES_PASSWORD=asdf postgres:9.5
psql -h dockerhost --username=postgres --password
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Build a custom postgres image and query it from laptop, pt 1

In this lesson we pave the way to customizing our database server by building it from a Dockerfile.

Enter the following at a terminal prompt.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd ~/dev/docker/learn/03-psql
docker build -t ssaundersla/psql-l1:0.1 .
docker images
docker run -d -p 5432:5432 --name ldb -e POSTGRES_PASSWORD=asdf ssaundersla/psql-l1:0.1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

psql -h dockerhost --username=postgres --password

Clean up:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker ps -a
docker stop ldb
docker rm ldb
docker images
docker rmi ssaundersla/psql-l1:0.1
docker images
docker ps -a
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Build a custom postgres image and query it from laptop, pt 2

In this lesson we customize our database server by building it from a
Dockerfile. All we do at this stage is add some initial data to the
database.

Enter the following at a terminal prompt.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd ~/dev/docker/learn/04-psql
docker build -t ssaundersla/psql-l1:0.2 .
docker images
docker run -d -p 5432:5432 --name ldb -e POSTGRES_PASSWORD=asdf ssaundersla/psql-l1:0.2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
psql -h dockerhost --username=postgres --password
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(run some SQL queries)

Clean up:
	docker ps -a
	docker stop ldb
	docker rm ldb
	docker images
	docker rmi ssaundersla/psql-l1:0.2
	docker images
	docker ps -a


## Build a custom postgres image and query it from laptop, pt 3

In this lesson we further customize our database server by building it
from a Dockerfile. This lesson adds users other than `root`.

Enter the following at a terminal prompt.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd ~/dev/docker/learn/learn-docker/05-psql

docker build -t ssaundersla/psql-l1:0.3 .
docker run -d -p 5432:5432 --name ldb ssaundersla/psql-l1:0.3

psql --host=dockerhost --username=atrium --dbname=atrium
psql --host=dockerhost --username=atriumro --dbname=atrium
(run some SQL queries)

Clean up:

docker stop ldb
docker rm ldb
docker rmi ssaundersla/psql-l1:0.3
docker images
docker ps -a


## Build a custom postgres image and query it from laptop, pt 4

In this lesson we trim down the docker command options by putting them
into a file. Instead of running `docker` directly, we will be running
`docker-compose`.

Enter the following at a terminal prompt.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd ~/dev/docker/learn/learn-docker/06-psql
docker-compose build --no-cache
docker-compose up -d
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `docker-compose` command uses the contents of the `docker-
compose.yml` file to build an image and then run it as a container.
You can now use the container, e.g.:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
psql --host=dockerhost --username=atrium --dbname=atrium
(run some SQL queries)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When you are done, use the following command to stop the containers
and remove their images:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose down --rmi=all
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you installed the bashrc shortcuts, you can go from Dockerfile to
running container and then clean it all up again with:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
dcu
(do things with the container)
dcd
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Instead of:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker build -t ssaundersla/psql-l1:0.3 .
docker run -d -p 5432:5432 --name ldb ssaundersla/psql-l1:0.3
docker stop ldb
docker rm ldb
docker rmi ssaundersla/psql-l1:0.3
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


docker images
docker ps

docker-compose build --no-cache
docker images
docker ps

docker-compose up -d
docker images
docker ps

psql --host=dockerhost --username=atrium --dbname=atrium
psql --host=dockerhost --username=atriumro --dbname=atrium
(run some SQL queries)

Clean up:

docker-compose down --rmi=all
docker images
docker ps




## docker pull jenkins and get it running

Enter the following at a terminal prompt.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker pull jenkins:1.642.4
docker run --name jenkins -p 8080:8080 -p 50000:50000 -v /var/jenkins_home jenkins:1.642.4
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

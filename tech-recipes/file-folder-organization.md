Subfolders of "Documents"
-------------------------

These are the folders as I would have set them up at VT in hindsight. Hopefully this will be a model for the next situation.

HR/                                 - All things HR
	new-hire/                       - All files related to new-hire
		drivers-license.pdf         - my driver's license (image)
		ssn.pdf                     - my SS card (image)
		new-hire-notes.md           - misc notes from the first few weeks on the new job
	2017-performance-reviews/       - performance review files from 2017
	2017-misc/                      - all other HR things from 2017
	receipts/                       - receipts for which I need reimbursement

how-things-work/                    - Notes on the workings of various techniques and technologies
	dev-setup.md                    - how to set up a dev machine
	build-notes.md                  - how the releases are build

deploying/                          - (N/A?) notes regarding deploying releases

config-backups/                     - handy config files for various scenarios

software-licenses/                  - software license files/info

active-tickets/                     - root folder for folders containing notes about tickets
	misc/                           - all things ticket-related that do not have ticket numbers

completed-tickets/                  - folders from active-tickets once they are done (released)

deferred-tickets/                   - folders from active-tickets if something has delayed work on them

obsolete/                           - should be in the trash but I'm too chicken to delete it

projects                            - Any work that does not have a ticket associated with it
	misc/                           - (bad idea?) projects too small to warrant a folder

% Git Recipes
% Steve Saunders
% 2012-03-09

# Github

## Set up to contribute an open-source project


On github, fork e.g. timsneath/dart_console.git into ssaundersla/dart_console.git

Clone your fork

	cd ~/Desktop
	rm -rf dart_console
	git clone ssh://ss.github.ssh.invalid/ssaundersla/dart_console.git
	cd dart_console

Add an 'upstream' remote

	git remote add upstream https://github.com/timsneath/dart_console.git
	git fetch upstream
	git checkout master
	git merge upstream/master

Branch 'master', assuming master is where code gets merged

	git mkb scrollback

Test, add, commit, etc

Push changes

	git push --set-upstream origin scrollback

Git and Github will respond with a URL to use to make a PR back to the original.



# Repos

General Assumptions:
- your working directory is the working copy

## Create A Private Repository From A Folder Full Of Uncontrolled Files

Scenario: You have a project that you've been working on. You have not yet done anything about source control. The project has gotten to the point where you want to control it though.

	git init
	git add .
	git commit -a -m 'initial project import'

Certainty: very

Assumes:
- ?

This creates a repository in the working directory with a master branch. Your working copy files are already present.


## Create A Central Shared Repository From A Private Repository

Scenario: You have made a repository out of your folder full of files. You have also gotten to the point where you want to post it on the company server so that others can work on it.

	git clone --bare . /gitserver/myproj.git

At this point, you should probably add the central repo as the origin of your original repo and set `push` and `pull` to use it.

	git remote add origin /gitserver/myproj.git
	git branch --set-upstream-to origin master

Verify that everything worked as expected by issuing:

	git remote show origin

Certainty: very

Assumes:
- the company server is mounted at /gitserver


## Create A Central Shared Repository From A Folder Full Of Uncontrolled Files

Scenario: You have a project that you've been working on. You have not yet done anything about source control. The project has not only gotten to the point where you want to control it, it's also gotten to the point where you want to post it on the company server so that others can work on it.

	- repeat the above two steps

Certainty: very


## Create A Private Repository From Nothing

Scenario: You want to begin a project with source control right from the beginning.

Note: Don't do this. Create a folder and at least one file and go through the above steps.


## Create A Central Shared Repository From Nothing

Scenario: You want to begin a project with source control right from the beginning, and you want it on the company server so that others can work on it.

The easiest way is still to create a folder and at least one file and go through the above steps. But see below for how this would work for an online repository.

## Check Out A Working Copy From A Server Repository

Scenario: Someone else has placed a repository on the company server and you want to begin working on it.

	git clone /gitserver/myproj.git ./myproj

See Activate Branches of a Cloned Repo below.



# Online Repository Services

Gitolite, Github, BitBucket, etc. all let you manage git repositories on a computer somewhere 'in the cloud.' Here are some tips for working with them. All of these tips assume the following:

- You have the online account
- You have created RSA key files in your ~/.ssh/config file
- You have configured your online account with these RSA key files

## Check Out A Working Copy From An Online Repository

Scenario: Someone else has placed a repository in an online service and you want to begin working on it.

1. Github

	git clone git://github.com/schacon/grit.git mygrit

Certainty: very

Assumes:
- the project is hosted at github
- your working directory is the root of what will be your working copy
- you will next cd to ./myproj and begin working

3. SSH server

	git clone git@example.com:myrepo example-repo

Certainty: ???

Assumes:
- the project is hosted at example.com via SSH
- your working directory is the root of what will be your working copy
- you will next cd to ./example-repo and begin working

4. Gitolite

	git clone git@example.com:myrepo example-repo

Certainty: ???

Assumes:
- the project is hosted at example.com via SSH
- your working directory is the root of what will be your working copy
- you will next cd to ./example-repo and begin working

## 'Upload' a Local Repository to an Online Repository

Scenario: You have a project that is under source control and you want to 'upload it' to an online service repo, such as github. That is, cause your local repo to become a clone of a not-currently-existent online repo. First, you will have to use the service's web interface to create the repo. Once you have that done:

	git remote rm origin
	git remote add origin ssh://{my-ssh-config-alias}/{myaccount}/{myrepo}.git
	git branch --set-upstream-to origin master


Certainty: very

Assumes:
- the project is hosted at github or somewhere
- your working directory is the root of your working copy
- {my-config-alias} is the Host name in your ~/.ssh/config file
- {myaccount} is your account name at the service
- {myrepo} is the service's name of the repo (generally created via browser)


## Switching your local repo to a different central shared repo

Scenario: You have abandoned Gitservice.com and want to move your local repo to use Gitotherservice.com

	git remote rm origin
	git remote add origin ssh://gitotherservice/user/project.git
	git branch --set-upstream-to origin/master

Where `gitotherservice` is the name of the service as configured in SSH (.ssh/config)


# Adding, Removing, and Renaming Files


## Adding New Working Copy Files To A Repository

Scenario: You want to add new files to the local repository.

Use "git add .", not "git add *"

	git add .
	git commit -m"changed some things"

"git add *" will ignore your .gitignore files.

Certainty: very

Assumes:
- ?

## Removing Working Copy Files From A Repository

Scenario: You have some files that are no longer necessary and you want to remove them from the local repository.

The 'approved' method in git to delete a file is to use 'git rm'. However, git will also notice a file you've deleted via plain rm. BTW, git rm will also issue a plain rm.

Certainty: very

Assumes:
- ?

## Renaming Working Copy Files in A Repository

Scenario: You have some files you have renamed. What now?

The 'approved' method in git to rename a file is to use 'git mv'. However, git may be able to detect a file rename if the file contents have not changed. Rename the file outside of git, and

	mv file-1.txt f1.txt
	git add .
	git status

will show:

	# On branch master
	# Changes to be committed:
	#   (use "git reset HEAD <file>..." to unstage)
	#
	#	new file:   f1.txt
	#
	# Changes not staged for commit:
	#   (use "git add/rm <file>..." to update what will be committed)
	#   (use "git checkout -- <file>..." to discard changes in working directory)
	#
	#	deleted:    file-1.txt

But in spite of that, committing the changes results in:

	[master ecffecd] (incremental save)
	 1 files changed, 0 insertions(+), 0 deletions(-)
	 rename beta/bravo/{file-1.txt => f1.txt} (100%)

...which is the same thing that happens when you use git mv.

Certainty: very

Assumes:
- ?

# Branches

Background: There are two basic kinds of branch in git; local branches and remote branches. The remote branches are prefixed with 'origin,' e.g. 'origin/master.' You can check out a remote branch but you can't make changes to it. In order to make changes to a branch of a cloned repository, you have to create a local branch which 'tracks' the remote branch. Git does not automatically create local tracking branches for remote branches, except for 'master.'

* local branch = a branch you create in your local repo
* remote branch = a useless artifact of git's weird workflow
* remote tracking branch = a local branch that 'tracks' a remote branch

If you haven't already issue the following command:

	git config --global push.default tracking

or add this to your global .gitconfig file:

	[push]
		default = tracking

## 'Activate' Branches of a Cloned Repo

Scenario: You have just cloned a repo, and you know that it contains more than one branch. You've probably gotten only the 'master' branch. You want to work on a different branch.

Solution: First, get a complete list of branches:

	git branch -a

Next, create your local 'remote tracking branch' from the 'remote branch' of your choice:

	git checkout branch-name

For example, `git branch -a` displays:

	* master
	  remotes/origin/HEAD -> origin/master
	  remotes/origin/R-3_0
	  remotes/origin/R-3_1

You want to do some work on branch R-3_1, so you enter:

	git checkout R-3_1

Note that you enter the branch name without the prefixes. This creates a local tracking branch for the remote branch, configures it for push and pull, and checks it out for you.

Certainty: very

Assumes:
- you already have a multi-branch repo with working copy.
- your working directory is the working copy


## Download A Copy of a Central Shared Branch

Scenario: Someone has made a feature branch in the central shared repository and you want to do some work on it.

	git pull
	* check to see what the remote branch is called and then... *
	git checkout -b steves-feature-branch origin/steves-feature-branch

Certainty: very

Assumes:
- ?


## Switch From One Branch To Another

Scenario: You have been working on one branch of your project, and now want to switch to a different branch.

	git checkout other-branch

Warning: Git will let you checkout a different branch even if you have uncommitted changes in your working copy.

Certainty: very

Assumes:
- you already have a multi-branch repo with working copy.
- your working directory is the working copy


## Create A Private Branch

Scenario: You need to make a branch and you don't want to share it with the rest of your co-workers, at least not yet.

	git checkout the-branch-I-want-to-branch-from
	git branch my-new-branch
	git checkout my-new-branch

Certainty: very

Assumes:
- ?


## Convert A Private Branch To A Central Shared Branch

Scenario: You have made a private feature branch, maybe done some work on it, and now you want to share it with the rest of your co-workers. Assume for the sake of completeness that you have named your local branch something you don't want the public branch to be named.

	git push origin my-feature-branch
	* now delete the local branch *
	git checkout master
	git branch -D my-feature-branch
	* and re-add it from the repo *
	git checkout --track origin/my-feature-branch

Certainty: very

Assumes:
- ?


## Show which branch is tracking what

Scenario: You've lost track of which of your local branches is tracking which central repo branch.

	git remote show origin

		* remote origin
		  Fetch URL: /Users/steve/Dropbox/Projects/git/central/myproj.git
		  Push  URL: /Users/steve/Dropbox/Projects/git/central/myproj.git
		  HEAD branch: master
		  Remote branch:
		    master tracked
		  Local branch configured for 'git pull':
		    master merges with remote master
		  Local ref configured for 'git push':
		    master pushes to master (up to date)


## Create A Central Shared Branch

Scenario: You need to make a feature branch and you want your co-workers to be able to work on the feature also, right from the beginning.

Well, you can't do this in git. You have to create a private branch (see) and then push it to the central shared repository (see).

Certainty: ???

Assumes:
- ?


## Delete A Central Shared Branch

Scenario: You and your co-workers have finished the feature and now want to delete the feature branch.

You can't do this with one command in git. Each person who has a tracking branch of the central branch has to delete it individually. So one person deletes the branch on the shared repository, and everyone deletes their own tracking branches. Finally, everyone deletes their 'remote banches'

	* one person does this... *
	git push origin :steves-feature-branch
	* and everyone else does this... *
	git branch -d (whatever the person named the tracking branch)
	git remote prune origin

Certainty: very

Assumes:
- ?


## List Shared Branches

Scenario: You want to list the branches that are on the server repository.

	git branch -r
	* or *
	git branch -a
	* to list all *

Certainty: very

Assumes:
- ?


## List Private Branches

Scenario: You want to list the branches that are only on your local copy of the repository.

	git branch

Certainty: very

Assumes:
- ?


## Rename a Private Branch

Scenario: You want to rename the branch that you are currently on.

	git branch -m <new-name>

Certainty: very

Scenario: You want to rename a branch in your local repo other than the one that you are currently on.

	git branch -m <current-name> <new-name>

Certainty: very




# Committing Changes



## Commit From Your Working Copy To The Local Repository

Scenario: You want to commit your work *locally*

	git add .
	git commit -m"changed some things"

Certainty: very

Assumes:
- you don't have a central repo or don't want to send changes to it


## Commit From Your Working Copy To The Central Repository

Scenario: You want to commit your work *to the server*

	git add .
	git commit -m"changed some things"
	git push

Certainty: very

Assumes:
- you have checked out a 'remote tracking branch' and are working on its working copy.


## Squashing commits

Scenario: You have several trivial commits and want to remove them so that the commits are merged into one. DO NOT DO THIS if you have pushed the commits. Some argue persuasively that you should not do this under any circumstances at all. But if you must:

	git status
	(look for "Your branch is ahead of ... by N commits")

	git rebase -i HEAD~M
	(where M = N-1)
	(follow the generated instructions -- keep the first commit)

You may want to run:

	git merge-base master mybranch

Certainty: very


# Updating

## Update Your Local Repository From The Central Repository

Scenario: You suspect that your co-workers have committed some work to the server, so now you want to update your copy of the repository and of your working copy to reflect that work.

	git pull

This will update your local repository and your working copy.

Certainty: very

Assumes:
- you have checked out a 'remote tracking branch' and are working on its working copy.


## Update Your Working Copy From The Local Repository

	* see checkout *


# Merging

## Merge Another Branch Into Your Working Copy

Scenario 1: There are bug fixes on the master branch, and you want to merge them into your feature branch.

	* do an update just to be safe *
	git checkout master
	git pull

	* do the merge *
	git checkout my-feature
	git merge master

Scenario 2: You have finished your new feature and want to merge it into the master branch. Same arrangement as in Scenario 1:

	git checkout master
	git merge my-feature

Certainty: very

Assumes:
- you have checked out a 'remote tracking branch' and are working on its working copy.


## Resolve A Conflict

Scenario: You just merged and now you have a conflict.

	git mergetool
	git clean -f
	git commit

Certainty: very



# Tagging


## Tag A Revision in Your Private Repository

Scenario: You're working on a private project and you want to tag a revision.

	git tag v1.1            # tags current revision
	git tag v1.1 7ddad9d    # tags named revision

Generally-accepted best practice is to use annotated tags. Annotated
tags include the name of the tagger, which might not be the same as
the person who committed the change, and the date the tag was created.
Annotated tags can also include a commit message, which might be a
good place to store release notes. To create an annotated tag:

	git tag -a v1.1            # tags current revision
	git tag -a v1.1 7ddad9d    # tags named revision

You will be prompted to enter a commit message.


## List Tags in Your Private Repository

Scenario: You're working on a private project and you want to find an old tag.

	git tag
	git tag -l 'v1.4.2.*'

Git doesn't make a distinction between local tags and remote tags. You look for tags in your local repo only. Push and pull introduce tags which have been newly created in someone's local repo.

Certainty: ???



## Describe Tags in Your Private Repository

Scenario: You want to find out what one of the tags was all about.

	git cat-file -p {tag}

This will dump the name of the tagger, the date the tag was created
(which is not the same thing as the date the commit was made), and the
contents of the tag message. It also dumps the tag name, the hash, and
the string "type commit".

Certainty: very


## Tag A Revision in the Central Shared Repository

Scenario: This is an official release. You want to tag the revision on the server.

Git doesn't do this. You first tag your own repo, then push the tagging to the central repo with:

	git push --tags

Certainty: TBT



## List Tags in the Central Shared Repository

Scenario: You want to find the tag for an old release.

Git doesn't make a distinction between local tags and remote tags. You look for tags in your local repo only. Push and pull introduce tags which have been newly created in someone's local repo.



# Fixing Mistakes

One of the points of any source control system is fixing mistakes. You want to be able to look back through the history of a directory of files and get back to a previous state. This is a collection of tips for doing that in git.


## Revert Changes To Your Working Copy

Scenario: You've accidentally deleted a file, or you've messed up a file so badly that you just want to throw it away and start over fresh from whatever was last committed.

	git checkout -- (name of deleted file)

Scenario: You've messed up several files in your working copy, to the point where you want to chuck the whole thing and go back to whatever was in the repository. Only do this if you haven't yet committed your mistake(s):

	git reset --hard HEAD

This will throw away any changes you may have added to the git index as well as any outstanding changes you have in your working tree.

Certainty: tested


## Search for a deleted file

Scenario: You want to get back a file that you once had but have now deleted. All you know is the filename or part of the filename ({fragment}). First, you will have to get the full (relative) path name. Use:

	git log --diff-filter=D --summary | grep delete | grep {fragment}

Example: Find 'WebDate.java'

	git log --diff-filter=D --summary | grep delete | grep WebDate

returns

	path/to/WebDate.java

Next, you will probably want to find the most recent commit of the file. See 'Find the most recent commit of a file'.

Note: If you don't have the filename but only remember a string or two inside the file, see 'Search for a string in all revisions of an entire git history'.

Certainty: tested


## Find the most recent commit of a file

Scenario: You have the full (relative) path name ({path}) of a file that you once had but have now deleted, and you want to know the ID of the last commit which contained the file. Probably as part of recovering it. Use:

	git log --name-only --date-order --reverse -- {path}

The last entry in the log will give you the ID of the commit (the SHA hash), the date, and the commit message. Note that this commit is the one in which the file was deleted, so the file does not actually exist in this commit. See 'Recover a deleted file'

Example: Find the commit that deleted 'WebDate.java'

	git log --name-only --date-order --reverse -- path/to/WebDate.java

returns

	commit b6f87acbc8cdfea101955a25ae0e883a8ea5de9e
	Author: Stephen Saunders <steve@schuylerhouse.com>
	Date:   Fri May 17 16:34:53 2013 -0700

	    Minor housekeeping: TODO tags

	path/to/WebDate.java

Note: This will also work for non-deleted files, but if the file is not deleted you don't need to recover the most recent commit -- you already have it. See 'Look at an old version of a file' if you need to do that instead.

Certainty: tested


## Recover a deleted file

Scenario: You want to get back a file that you once had but have now deleted. You will need the full (relative) path name of the file ({path}) and the ID of the commit in which it was deleted ({id}). See 'Search for a deleted file' and 'Find the most recent commit of a file'. Then, use:

	git checkout {id}^ -- {path}

The '^' is important. The {id} commit is the one in which the file was deleted, so the file does not actually exist in this commit. It exists in the commit immediately before {id}.

Warning: This `git checkout` command recovers the file and stages it in spite of the .gitignore file.

Certainty: tested


## Search for a string in all revisions of an entire git history

Scenario: You remember a piece of code, a function name, a constant, a file, that was there. And now it’s gone. What happened? In which commit was it removed? This trick looks for differences that introduce or remove an instance of {string}:

	git log --reverse --date-order -S{string}

The output lists the commits -- not the files -- which introduce the changes. To see which files changed, see "List file changes between commits" below. The " --date-order --reverse" part is just to list the commits in date order.

Certainty: tested


## List file changes between commits

Scenario: You've located a commit which introduced some changes you are interested in, such as with 'git log -S' Now you want to see which files could have introduced the changes.

	git log --name-only --reverse --date-order {commit}^..{commit}

This log command will list the files which might have contained the changes. You'll need info from outside git to know which file(s) to examine, although you can use the `git show` command (below) to look at an old version of a file.

Certainty: tested


## Use diff tool to compare a file between two commits

Scenario: You've identified a specific file and commit which you want to investigate further. You can in general compare a file between any two commits with either diff or difftool (if you have a diff tool configured) with this:

	git (diff|difftool) {commit1} {commit2} -- {file path}

In most cases, this will probably mean comparing the file in the commit of interest with the file in the prior commit. For example, to see how CMDGET.C changed in commit 6e9ddac:

	git difftool 6e9ddac^ 6e9ddac -- ./CMDGET.C

Certainty: tested


## Look at an old version of a file

Scenario: You want to review code from a previous commit, such as a file that you changed a year ago, but you don't want to revert the entire working copy. This trick prints the contents of file {file path} of commit {commit}:

	git show {commit}:{file path}

You will probably want to pipe the output to a file or a pager or to grep.

Certainty: tested


## Amend A Commit

Scenario: You committed something that you should not have committed or you committed some changes but forgot to run git add. You have only committed to your local repo -- you have not yet pushed.

	git commit -m 'there I fixed it'
	* oops *
	git add forgotten_file
	git commit --amend

Certainty: tested

Note: You should never do this to the central repo. Git does not normally expect the "history" of a project to change, and cannot correctly perform repeated merges from a branch that has had its history changed.


## Unstaging a Staged File

Scenario: You changed two files and git-added both, but upon further thought consider only one of them safe to commit to the repository. That is, your staging area is contaminated but not (yet) your repository.

	git add .
	* oops *
	git reset HEAD unsafe_file
	git commit ...

Certainty: tested


## Un-tracking Files in A Repository

Scenario: You forgot to add something to your .gitignore file and accidentally added it to your repository. That is, your repository is contaminated.

	git rm --cached myclass.o

Certainty: TBT



## TBD

Scenario: You created a new file, but it's not showing up in `git status`. You want to check your .gitignore file to see why it's being ignored.

	git check-ignore -v bin/updateViatorCsvFile.sh

You'll see the gitignore file and the line and the rule that causes the file to be ignored:

	/Users/steve/.gitignore:79:bin/	bin/updateViatorCsvFile.sh


## Getting Annotated File Listings

Scenario:

Use the annotate command to print a file, with the name of the committer for each line of the file

	git annotate {file path}


## Get the commit after a commit

Git is good at showing ancestors of a commit. If you want the next child of a commit, the following kludge may work:

	git log --reverse --ancestry-path --pretty=format:"%h %s (%ar)" {commit}^..HEAD|head -2






# Getting Info


## Getting Info About The Repository

Where is the central shared repository (if any)?

	git remote -v

Which branch pushes/pulls from where?

	git remote show origin


## List deleted files and the commits which deleted them

	git log --diff-filter=D --summary


## Getting Info About The Working Copy

Scenario:

This command gets info about the state of the working copy with respect to its repository:

	git status


## Getting File 'diff' Listings

Scenario:

Use the diff command to list the changes that you have made to your working copy. It shows the differences between your working copy and the last commit.

	git diff


## Getting Annotated File Listings

Scenario:

Use the annotate command to print a file, with the name of the committer for each line of the file

	git annotate path\to\file



# Misc

# Resetting the origin

Scenario: You have an empty online repo somewhere, such as one you just created with gitolite or bitbucket, and you want to make a local repo the one that's online.

First, remove the current origin if necessary:

	git remote rm origin

Now add the new origin, and beat git into understanding what's going on:

	git remote add origin ssh://{my-ssh-config-alias}/{myaccount}/{myrepo}.git
	git branch --set-upstream-to origin/master master

## Export A Branch For A Production Build/test

Scenario: You want to create a non-controlled copy of the branch for a test build.

	git archive -0 --output=../myproj.zip HEAD

Certainty: TBT


# Troubleshooting

## "No refs in common and none specified"

Courtesy of Riyad Kalla;

Today I ran a 'git push' to shove my commits from my local repository back into the main remote repo, the result was this:

    $ git push
    No refs in common and none specified; doing nothing.
    Perhaps you should specify a branch such as 'master'.
    error: failed to push some refs to 'git@github.com:painfreepr/<repo>.git'

The odd bit is that I had just done this with a previous repo about 30 mins ago and it worked fine; this was a new repository I was setting up. As it turns out this is the result of originally cloning an empty repository which is exactly what I did. I had created a new repo on GitHub and wanted to pull the repo down in IntelliJ to then add some files to it via the GUI instead of from the command line; so I had checked out the empty repo right after creating it.

The fix, fortunately, is dead easy:

    $ git push origin master

Doing this should provide output like:

    $ git push origin master

    Counting objects: 568, done.
    Delta compression using up to 2 threads.
    Compressing objects: 100% (559/559), done.
    Writing objects: 100% (568/568), 2.28 MiB | 2.18 MiB/s, done.
    Total 568 (delta 205), reused 0 (delta 0)
    To git@github.com:painfreepr/<repo>.git
    * [new branch]      master -> master

It is my understanding that the core issue is that there are no files in common between the original remote repo you cloned (empty) and the one on-disk (now full of files). Doing the git-push-origin-master shoves your repo up into the empty repository and gives you that common base again so you can do a ‘git push‘ without issue.

## fatal: The current branch master is not tracking anything.

Sometimes no amount of "git push origin master" will beat it into git's head where the origin is. If you get the above error message, enter this:

	git config branch.master.merge refs/heads/master
	git config branch.master.remote origin


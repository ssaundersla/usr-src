================================================================================
Setting up gitolite
@ server = the actions take place on the server
@ home = the actions take place on the client
================================================================================

@ server
	(install git)
	(create user 'git')
	(create /home/git)
	(get the DNS or IP address of the server)

@ home
	cd
	mkdir online-git-repos
	mkdir .ssh
	cd .ssh
	ssh-keygen -t rsa -f odysseus
	rsync odysseus.pub root@192.168.1.110:/home/git

@ server
	cd /home
	chown -R git:users git
	su git
	cd
	mkdir .ssh
	chmod 700 .ssh
	cp odysseus.pub .ssh/authorized_keys
	chmod 600 .ssh/authorized_keys

@ home (just to check ssh)
	ssh -vvv -i ~/.ssh/odysseus git@192.168.1.110
	exit
	(edit ~/.ssh/config -- see below)
	ssh ithaca
	exit

@ server (or just stay logged in)
	git clone git://github.com/sitaramc/gitolite
	gitolite/install
	/home/git/gitolite/src/gitolite setup -pk /home/git/odysseus.pub
	(edit .ssh/authorized_keys to delete original entry [the first line])

@ home
	cd ~/online-git-repos
	ssh -i ~/.ssh/odysseus git@192.168.1.110 info -h
	cp ~/.ssh/odysseus ~/.ssh/id_rsa
	ssh git@192.168.1.110 info -h
	git clone git@192.168.1.110:gitolite-admin ithaca-gitolite-admin
	(assuming ~/.ssh/config has been edited)
	ssh ithaca info -h
	git clone ithaca:gitolite-admin ithaca-gitolite-admin

================================================================================
config
================================================================================

Host ithaca
	User git
	Hostname 192.168.1.108
	Port 22
	IdentityFile ~/.ssh/odysseus

================================================================================
Using gitolite:
================================================================================

Get info about the repositories:
	ssh ithaca info -h

Check out the management repository
	git clone ithaca:gitolite-admin ithaca-gitolite-admin

Check out a content repository
	git clone ithaca:my-spiffy-project

================================================================================
adding a user
================================================================================

@ home
	cd ~/.ssh
	ssh-keygen -t rsa -f telemachus
	(move telemachus.pub to ithaca-gitolite-admin)
	(edit ithaca-gitolite-admin/conf/gitolite.conf as needed)
	(edit .ssh/config, see below)
	(in ithaca-gitolite-admin, git commit and push)

================================================================================
config
================================================================================

Host ithaca
	User git
	Hostname 192.168.1.108
	Port 22
	IdentityFile ~/.ssh/odysseus
Host ithaca-t
	User git
	Hostname 192.168.1.108
	Port 22
	IdentityFile ~/.ssh/telemachus

================================================================================
Add a repo (with existing history)
================================================================================

	(create a bare repository, myoldrepo.git)

	(create a bare repository, homepwner.git)
	rsync homepwner.git.tar.gz root@192.168.1.110:/home/git/repositories
	ssh root@192.168.1.110
	cd /home/git/repositories
	tar xzf homepwner.git.tar.gz
	chown -R git:users *
	su git
	/home/git/gitolite/src/gitolite setup
	exit
	(edit ithaca-gitolite-admin/conf/gitolite.conf, add the new repo)
	(in ithaca-gitolite-admin, git commit and push)
	cd ithaca-gitolite-admin
	(git commit ...
	git push
	cd ..
	git clone ithaca:homepwner

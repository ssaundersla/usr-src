How to clone a repo
-------------------

e.g.:

	git clone git@openserver.kodanwebtech.com:tips ./tmp


How to add a completely new repo
--------------------------------

First, edit the config file. If you don't already have it, you need to
clone the admin repo:

	git clone git@openserver.kodanwebtech.com:gitolite-admin ./tmp

Then, edit the file at

	./tmp/conf/gitolite.conf

Add the repo by editing the file, commit, push. Then clone this new
repo, which will complain that it's empty. Add a file to the new
cloned repo, and push it with *this* command:

	git push origin master

You have to do this or the repo won't know who it's daddy is. Once you
do this once, you should be able to clone, push, pull etc. from then
on normally.


How to add an existing repo
---------------------------

First, edit/commit/push the config file as above to add the repo. DO
NOT clone this new repo. Instead,

	git remote add origin git@openserver.kodanwebtech.com:sublime_text_config.git

Check that this worked:

	git remote -v

Then push to the remote:

	git push origin master

The for good measure pull from the remote in case git hasn't yet
figured out where the remote is:

	git pull origin master
jquery without CSS identifiers
==============================

Use `data-click` e.g.:

<li class="waves-effect waves-light hide-on-med-and-down"><a href="#" data-click="open-login-modal">Log In</a></li>

$("a[data-click='open-login-modal']").click(function(){
    $('.mobile-personal-button').sideNav('hide');
    $("#join-modal-id").modal('close');
    $("#forgot-password-modal-id").modal('close');
    $("#login-modal-id").modal('open');
});

Be sure to include the tag type ('a' in the above case) or performance suffers: The parser has to check each element rather than limit checking to just the 'a' tags.

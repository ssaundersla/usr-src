This set of instructions assumes you have installed the certbot client for Let's Encrypt.

# DNS Method

These instructions are appropriate for the following scenario:

* You need a 'real' SSL certificate quickly
* You don't have a web server handy or don't want to deal with one
* You only need the certificate for 90 days or less
* You are able to edit the zone file for a domain

## Dry run

Create a temporary directory to hold the certificate files. This is necessary because you are not using certbot in its more typical web server mode.

	mkdir ./tmp

Do a dry-run using the staging server:

	certbot certonly --manual --staging --dry-run \
	--preferred-challenges dns \
	--config-dir ./tmp/conf --work-dir ./tmp/work --logs-dir ./tmp/logs \
	-m stephen@mooreboeck.com -d saturn.prototype.jpl.mooreboeck.com

It can take several minutes for certbot to get back to you.

Note: Certbot does all of its verification *after* you have added records to the zone file. That is, you have to add the TXT records and leave them in the zone file until the command has finished. If you create an entry for e.g. `www.mooreboeck.com` when prompted, and then re-use the entry for `mail.mooreboeck.com` when prompted next, the verification will fail.

At the end of this process you will have nothing. This is just a no-cost dry run of certbot. Next we'll generate a 'fake' certificate.

## Fake certificate

in this case the issuer identifies itself as the 'fake' version of Let's Encrypt. You can't use this certificate, but you don't use up your quota of certificates by using the staging server.

Re-create the temporary directory:

	rm -rf ./tmp
	mkdir ./tmp

Use the staging server again:

	certbot certonly --manual --staging \
	--preferred-challenges dns \
	--config-dir ./tmp/conf --work-dir ./tmp/work --logs-dir ./tmp/logs \
	-m stephen@mooreboeck.com -d saturn.prototype.jpl.mooreboeck.com

You will experience the same wait and the same questions. At the end of this process though you will have a 'fake' certificate, probably in your `./tmp/conf/archive/{SITENAME}/` directory.

## Real certificate

Once you are comfortable generating fake certificates, it's time to move on to generating real ones. Re-create the temporary directory:

	rm -rf ./tmp
	mkdir ./tmp

Use the real server this time:

	certbot certonly --manual \
	--preferred-challenges dns \
	--config-dir ./tmp/conf --work-dir ./tmp/work --logs-dir ./tmp/logs \
	-m stephen@mooreboeck.com -d saturn.prototype.jpl.mooreboeck.com

You will experience the same wait and the same questions. At the end of this process though you will have a real certificate.

	open ./tmp/conf/archive
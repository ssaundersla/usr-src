# Sample apps

```
       | ZIP                               | IMAGE                            |
--------------------------------------------------------------------------------
java   |                                   | fei_ms                           |
Python | profanity_ms                      | netcdf_ms                        |
ruby   | spice_api_ms                      | ruby_ms (maybe)                  |
       | forms_api_ms                      |                                  |
       | force_deploy_ms                   |                                  |
--------------------------------------------------------------------------------

fei_ms           | j | i | samproject/PsycheFeiMainFunction
force_deploy_ms  | r | z | sam-app/decide_fn
forms_api_ms     | r | z | sam-app/src
netcdf_ms        | p | i | sam-app-image/CvtPrototypeFunction
profanity_ms     | p | z | sam-app/src
spice_api_ms     | r | z | sam-app/src

Discontinued:
proposals_ms     | r | z | sam-app/src (replaced by ec2 cron)
ruby_ms          | r | z | sam-app/src (jekyll)
```

# Environments

## Dev environment

Defined by sam-app/template-dev.yaml and sam-app/samconfig-dev.toml

## Production environment

Defined by sam-app/template-production.yaml and sam-app/samconfig-production.toml. For some sites the difference is only a different set of endpoints and a different database.


# Unit testing

Use something like `sam-app/tests/unit/test_app.rb` to test the logic of the app. Since this will be run locally, authentication to AWS will have to be through AWS keys, which means setting the environment variables correctly.

Unit tests use the e.g. `sam-app/tests/env/ocio.json` files to authenticate to AWS. These files will have to be updated every three months because of JPL's key rotation.

Unit tests use the e.g. `sam-app/events/post-entry.json` files to mock the `event` object which is passed to the Lambda handler. These files will have to be updated only if you want different data in them.

* The "path" element of the e.g. `sam-app/events/post-entry.json` files specifies the URL being simulated by the test.

* The "body" element of the e.g. `sam-app/events/post-entry.json` files specifies the (JSON) body of the request being simulated by the test, if any.

* The "queryStringParameters" element of the e.g. `sam-app/events/post-entry.json` files specifies the query strings (e.g. "?log_level=debug&api_key=123") of the request being simulated by the test, if any.


# Local tests

## Local tests, config

Local tests (via `sam invoke`) use the same files to mock the `event` object as the unit tests (described above).

## Local tools, config

* `runtools build` will invoke the default operation as set by `default_deploy_env` in the config.sh file. You can change this by e.g. `runtools prod build`.

* `runtools test local` looks for environment JSON files in the directory path as set by `env_test_dir` in the config.sh file and defaults to sam-app/tests/env, as do the unit tests. You cannot change this at the command line -- it should be constant for a project.

* `runtools test local` will invoke the default operation as set by `default_deploy_env` in the config.sh file. You can change this by e.g. `runtools prod test local`

* `runtools test local` will invoke the default argument package as set by `argument_package_name` in the config.sh file. This in turn determines the function being run and the JSON file that is invoked as mocked input. You can change this by using the `--args KEYWORD` option. The keywords are also defined in the config.sh file (see set_argument_package). e.g. `runtools test local --args version`


## Local tests, running

NOTE: The following assumes that Docker is running.

In order to run a local test, first build the project:
```
runtools build
```
and then run the test:
```
runtools test local
```


* The command can contain several sub-commands strung together, e.g.:
```
runtools prod build test local
```


# Deploying

NOTE: The following assumes that Docker is running.

In order to deploy a project to AWS, first build the project:
```
runtools build
```
and then deploy it:
```
runtools deploy
```
or do both at the same time
```
runtools build deploy
```

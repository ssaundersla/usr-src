MySQL cheatsheet
================

Running MySQL in Docker
-----------------------

# Launch

docker run                                  \
  --name mars_legacy                        \
  --env MYSQL_ROOT_PASSWORD=czynvfibs       \
  --env MYSQL_DATABASE=mars                 \
  --env MYSQL_USER=mars                     \
  --env MYSQL_PASSWORD=czynvfibs            \
  --detach                                  \
  --publish 3306:3306                       \
  --rm                                      \
  mysql:5.7

# Interactive

mysql -u mars --password=czynvfibs -h 127.0.0.1 mars

or

mysql \
  --user=admin \
  --password=tbdtbdtbdtbdtbd  \
  --host=mb-nasa-wp-dev-230117115238.cexarkw1eai9.us-west-2.rds.amazonaws.com \
  mysql

# Import data

mysql -u mars --password=czynvfibs -h 127.0.0.1 mars <my-file.sql> my-file.log

# export data

mysqldump -u swot --password=oLZVPefWDn3C -h 127.0.0.1 swot > seal.sql


Misc
----

mysql -u root -p --host=localhost mysql
{root password for local mysql should be same as login password}

create database schuybase_dev;

grant all on schuybase_dev.* to 'schuybase_dev'@'localhost' identified by 'sbdev';
mysql -u schuybase_dev --password=sbdev schuybase_dev

grant all on schuybase_1.* to schuybase_1@"192.168.%" identified by ...
mysql -u schuybase_1 --password=safetyon -h schuybase.schuylerhouse.com schuybase_1

use mysql;
select user,host from user where user like 'sch%';
select user,host,db from db where user like 'sch%';

mysql -u root --password=... my_db <my-file.sql> my-file.log


Installing MySQL on CentOS
--------------------------

yum install mysql-server
yum install mysql
chkconfig --list|more
chkconfig --level 2345 mysqld on
chkconfig --list|more
service mysqld status
service mysqld start
(11:50 AM) ~/dev/schuybase-0/ss.private (115)
ssaunders@steve>

sqlite3 info (probably pointless)
---------------------------------

sqlite3 tst.db
.read tst.sql
select * from hardware_enum;
.quit

Datebase corruption
===================

How to recover a MySQL instance that will not start

This assumes that you have installed MySQL using the installer from Oracle.

Check the error log at:

	/usr/local/mysql/data/mysqld.local.err

Look for a line like:

	"2017-08-15 11:30:44 43478 [ERROR] InnoDB: Attempted to open a previously opened tablespace. Previous tablespace {table name} uses space ID: {integer} at filepath: {file path}. Cannot open tablespace {other table name} which uses space ID: {same integer} at filepath: {other file path}"

This is evidence of a corrupted database. MySQL has a command line option to fix it. Start by displaying the error log in a separate window or tab:

	sudo tail -f /usr/local/mysql/data/mysqld.local.err

Search in the `/Library/LaunchDaemons` folder for a file named `com.oracle.oss.mysql.mysqld.plist` or something similar. Enter the following command in a terminal window to ensure that MySQL is shut down:

	sudo launchctl unload -w /Library/LaunchDaemons/com.oracle.oss.mysql.mysqld.plist

The plist file should be a plain text XML file you can edit with a text editor. Edit the file and look for a list of program arguments (look for "<key>ProgramArguments</key>") and add this argument to the list:

	<string>--innodb_force_recovery=1</string>

Save the file and enter the following command in a terminal window to start MySQL.

	sudo launchctl load -w /Library/LaunchDaemons/com.oracle.oss.mysql.mysqld.plist

You should soon see a line in the error log like:

	2017-08-15 11:30:44 43478 [Note] /usr/local/mysql/bin/mysqld: ready for connections.

Edit the plist file again and remove the `innodb_force_recovery` line. If this does not work, you can try again with `innodb_force_recovery=2`. You can also try a value of 3, but 4 or greater run the risk of damaging your database.


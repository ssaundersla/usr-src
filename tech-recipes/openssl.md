Viewing CSRs and certificates
=============================

openssl req  -text -in csr.txt
openssl x509 -text -in cert.txt
openssl x509 -issuer -issuer_hash -in the-cert.txt
openssl rsa -noout -text -in private-key.pem
openssl pkcs12 -info -in import-file.pfx
openssl s_client -connect p-laboratories.schuynet.net:443 -showcerts
openssl x509 -noout -sha256 -fingerprint -in the-cert.txt

# download a certificate from a site
# it's much easier to do this than to fight with a browser
openssl s_client -connect HOST:PORTNUMBER

Note:
=====

As of 2011-09-?? RapidSSL is requiring an intermediate certificate.
Also, example.com will no longer work with www.example.com
But, www.example.com will still work with example.com





================================================================================

General OpenSSL Commands
------------------------

These commands allow you to generate CSRs, Certificates, Private Keys and do other miscellaneous tasks.

    Generate a new private key and Certificate Signing Request

    openssl req -out CSR.csr -new -newkey rsa:2048 -nodes -keyout privateKey.key

    Generate a self-signed certificate (see How to Create and Install an Apache Self Signed Certificate for more info)

    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout privateKey.key -out certificate.crt

    Generate a certificate signing request (CSR) for an existing private key

    openssl req -out CSR.csr -key privateKey.key -new

    Generate a certificate signing request based on an existing certificate

    openssl x509 -x509toreq -in certificate.crt -out CSR.csr -signkey privateKey.key

    Remove a passphrase from a private key

    openssl rsa -in privateKey.pem -out newPrivateKey.pem

Checking Using OpenSSL
----------------------

If you need to check the information within a Certificate, CSR or Private Key, use these commands. You can also check CSRs and check certificates using our online tools.

    Check a Certificate Signing Request (CSR)

    openssl req -text -noout -verify -in CSR.csr

    Check a private key

    openssl rsa -in privateKey.key -check

    Check a certificate

    openssl x509 -in certificate.crt -text -noout

    Check a PKCS#12 file (.pfx or .p12)

    openssl pkcs12 -info -in keyStore.p12

Debugging Using OpenSSL
-----------------------

If you are receiving an error that the private doesn't match the certificate or that a certificate that you installed to a site is not trusted, try one of these commands. If you are trying to verify that an SSL certificate is installed correctly, be sure to check out the SSL Checker.

    Check an MD5 hash of the public key to ensure that it matches with what is in a CSR or private key

    openssl x509 -noout -modulus -in certificate.crt | openssl md5
    openssl rsa -noout -modulus -in privateKey.key | openssl md5
    openssl req -noout -modulus -in CSR.csr | openssl md5

    Check an SSL connection. All the certificates (including Intermediates) should be displayed

    openssl s_client -connect www.paypal.com:443

Converting Using OpenSSL
------------------------

These commands allow you to convert certificates and keys to different formats to make them compatible with specific types of servers or software. For example, you can convert a normal PEM file that would work with Apache to a PFX (PKCS#12) file and use it with Tomcat or IIS. Use our SSL Converter to convert certificates without messing with OpenSSL.

    Convert a DER file (.crt .cer .der) to PEM

    openssl x509 -inform der -in certificate.cer -out certificate.pem

    Convert a PEM file to DER

    openssl x509 -outform der -in certificate.pem -out certificate.der

    Convert a PKCS#12 file (.pfx .p12) containing a private key and certificates to PEM

    openssl pkcs12 -in keyStore.pfx -out keyStore.pem -nodes

    You can add -nocerts to only output the private key or add -nokeys to only output the certificates.
    Convert a PEM certificate file and a private key to PKCS#12 (.pfx .p12)

    openssl pkcs12 -export -out certificate.pfx -inkey privateKey.key -in certificate.crt -certfile CACert.crt


================================================================================
Moved to /home/steve/Reference/SSL/bypassing-iis-csr.txt
================================================================================

Creating CSRs and certificates
==============================

openssl req -out CSR.txt -new -newkey rsa:2048 -nodes -keyout privateKey.key
openssl req -text -noout -verify -in CSR.txt
openssl rsa -in privateKey.key -check



1. Create the CSR and the private key
-------------------------------------

openssl req -new rsa:2048 -nodes -out the-csr.txt -newkey -keyout the-private-key.pem -subj "/C=US/ST=CA/L=Valencia/O=Schuyler House/OU=CRO/CN=connect.schuylerhouse.com"

2.1. Get the CSR (the-csr.txt) signed
-------------------------------------

(Offline: purchase from RapidSSL or somewhere and retrieve the certificate)

2.2. Self-sign the CSR
----------------------

__________________________

3.1. Convert the certificate and private key to PKCS #12
--------------------------------------------------------

openssl pkcs12 -export -out the-import-file.p12 -inkey the-private-key.pem -in the-cert.txt

3.2. Convert the certificate, private key, and intermediate to PKCS #12
-----------------------------------------------------------------------

openssl pkcs12 -export -out the-import-file.p12 -inkey the-private-key.pem -in the-cert.txt -certfile intermediate-cert.crt

4.1. Import the PKCS #12 file into Windows
4.2. Import the PKCS #12 file into IIS
4.3. Import the PKCS #12 file into IIS and Windows

I was able to import the PKCS12 file into Windows at 819GRE by just double-clicking the file. I'm not sure what if anything this did. I still had to explicitly import the file into IIS. Once I had explicitly imported the file into IIS, I was able to set the bindings on the website as usual.

Creating Multi-SAN CSRs
=======================

Command

	openssl req   \
	  -new        \
	  -nodes      \
	  -key    /path/to/private.key \
	  -config /path/to/config-file.cnf     \
	  -out    /path/to/new-csr.csr

config-file.cnf contents:

	[req]
	default_bits = 2048
	prompt = no
	default_md = sha256
	req_extensions = req_ext
	distinguished_name = dn

	[ dn ]
	C=US
	ST=California
	L=Pasadena
	O=NATIONAL AERONAUTICS AND SPACE ADMINISTRATION
	OU=Europa Program
	CN = europa.nasa.gov
	emailAddress = philip.w.davis@jpl.nasa.gov

	[ req_ext ]
	subjectAltName = @alt_names

	[ alt_names ]
	DNS.1 = europa.nasa.gov
	DNS.2 = europa.jpl.nasa.gov

PostgreSQL
==========

## Login

PostgreSQL should be started by a normal System V init script in
/etc/init.d/postgresql. It should be running when the OS boots.

To log in as 'sndemo':

	psql --username=sndemo sndemo -h localhost
	(password = 'sndemo')

To log in as 'sysop':

	psql --username=sysop postgres -h localhost
	(password = linux login password)

To log in as 'root':

	sudo -u postgres psql
	(password = linux login password)

## Server startup

This works when run as an admin user (not as root)
	/usr/local/opt/postgresql/bin/postgres -D /usr/local/var/postgres

This works when run as an admin user (not as root)
	pg_ctl -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log start

This does not work
	homebrew
	After a day spent trying to get the homebrew installation of postgres to run using either the homebrew utilities or the launchclt script that ships with homebrew I have given up.

## Configuration (Universal?)

After logging in using this method, I then created a non-super administrator role in PostgreSQL:

	CREATE ROLE sysop WITH LOGIN CREATEDB CREATEROLE PASSWORD 'nat4che';

I was able to log in to PostgreSQL as this sysop using:

	psql --username=sysop postgres -h localhost

As sysop, I was able to create a new user 'sndemo':

	DROP ROLE sndemo;
	CREATE ROLE sndemo WITH LOGIN CREATEDB PASSWORD 'sndemo';

After logging out of sysop and logging in to sndemo,

	psql --username=sndemo postgres -h localhost

...I was able to create the sndemo database:

	CREATE DATABASE sndemo WITH OWNER sndemo;

I was then finally able to log in as sndemo and start creating tables:

	psql --username=sndemo sndemo -h localhost

Note: The '-h localhost' option is vital. Without it, psql for some reason ignores password verification and fails to log in.

Note: I was not able to create the 'sndemo' database with owner 'sndemo' while logged in as sysop. I had to create sndemo (the user) with CREATEDB, log out, then log in again as sndemo (the user) in order to create sndemo (the database).

## Installation, Linux

I installed PostgreSQL on a Linux Mint Maya system using the standard Software Manager (wrapper for apt-get) method. Immediately after installation, the PostgreSQL daemon seemed to be running. The only way I could connect with it though was as the PostgreSQL superuser using the postgres Unix user:

	sudo -u postgres psql
	(password = linux login password)

## Installation, MacOS

I installed PostgreSQL on a MacOS 10.7.5 system using the installer downloaded from EnterpriseDB.com. Immediately after installation, the PostgreSQL daemon seemed to be running:

su macmini
sudo su
su postgres
/Library/PostgreSQL/9.1/bin/pg_ctl status -D /Library/PostgreSQL/9.1/data

Note: This condition persisted even through a full shutdown and restart of the host machine.

Note: The `postgres` login can only be reached through `sudo su` from root. A simple `su postgres` even with the postgres password will not do it.

I was not able to log in to the RDBMS using the above Linux method. Instead, the only way I could connect with it was:

	psql --username=postgres -h localhost
	(password = installation password*)
	* as part of the action of the EnterpriseDB installer, I gave a password
	  to the 'postgres' user.

## Installation, Docker

Installing PostgreSQL using docker is the simplest solution of them all so far. Assuming that you have downloaded and installed Docker Tools and have SSHed to the VM, run the following command:

	docker run --name cdb -e POSTGRES_PASSWORD=nat4che -p 5432:5432 -d postgres:9.4

This will download PostgreSQL 9.4, create a local docker image for it, and run it as a container. Make sure you specify the host when you connect with the `psql` command:

	psql -h dockerhost --username=postgres --password

I set up `dockerhost` in /etc/hosts to resolve to the default docker machine on OSX:

	192.168.99.100  dockerhost

If you don't have a local psql client, you can use the one inside the docker container:

	docker exec -it cdb psql --username=nemo --dbname=nemo














## PSQL Commands (frequently used)

Command  | Description
---------|--------------------------------------------------------------------
\d       | list tables, views, and sequences
\d TABLE | as mysql 'describe'
\du      | list roles (users)
\dg      | list roles (groups)
\dp      | list table, view, and sequence access privileges
\e       | edit the query buffer with external editor (vi)
\q       | quit psql

## Scribbles

sudo -u postgres psql
(password = linux login password)

CREATE ROLE sysop WITH LOGIN CREATEDB CREATEROLE PASSWORD 'nat4che';
CREATE ROLE sndemo WITH LOGIN CREATEDB PASSWORD 'nat4che';

createdb is a wrapper around the SQL command CREATE DATABASE. There is
no effective difference between creating databases via this utility
and via other methods for accessing the server.

CREATE DATABASE sndemo WITH OWNER sndemo;

psql --username=sndemo sndemo -h localhost

## Documentation: 8.1.4. Serial Types

The data types serial and bigserial are not true types, but merely a notational convenience for setting up unique identifier columns (similar to the AUTO_INCREMENT property supported by some other databases). In the current implementation, specifying:

	CREATE TABLE tablename (
	    colname SERIAL
	);

is equivalent to specifying:

	CREATE SEQUENCE tablename_colname_seq;
	CREATE TABLE tablename (
	    colname integer NOT NULL DEFAULT nextval('tablename_colname_seq')
	);
	ALTER SEQUENCE tablename_colname_seq OWNED BY tablename.colname;

Thus, we have created an integer column and arranged for its default values to be assigned from a sequence generator. A NOT NULL constraint is applied to ensure that a null value cannot be explicitly inserted, either. (In most cases you would also want to attach a UNIQUE or PRIMARY KEY constraint to prevent duplicate values from being inserted by accident, but this is not automatic.) Lastly, the sequence is marked as "owned by" the column, so that it will be dropped if the column or table is dropped.

Note: Prior to PostgreSQL 7.3, serial implied UNIQUE. This is no longer automatic. If you wish a serial column to be in a unique constraint or a primary key, it must now be specified, same as with any other data type.

## Fun with tables

### Autoincrement

PostgreSQL uses 'sequences'. Create a table with a 'serial' primary key:

	CREATE TABLE COMPANY
	(
	   id             SERIAL PRIMARY KEY,
	   name           TEXT      NOT NULL,
	   age            INT       NOT NULL,
	   salary         REAL
	);

Give it some rows:

	INSERT INTO COMPANY (name,age,address,salary) VALUES ( 'Paul', 32, 20000.00 );
	INSERT INTO COMPANY (name,age,address,salary) VALUES ( 'Wendy', 29, 40000.00 );
	INSERT INTO COMPANY (name,age,address,salary) VALUES ( 'Mary', 29, 90000.00 );
	INSERT INTO COMPANY (name,age,address,salary) VALUES ( 'Glen', 62, 3000.00 );

Try to read them

	select * from company;

and you get

	 id | name  | age | salary
	----+-------+-----+--------
	  1 | Paul  |  32 |  20000
	  2 | Wendy |  29 |  40000
	  3 | Mary  |  29 |  90000
	  4 | Glen  |  62 |   3000
	(4 rows)

### Indexes

PostgreSQL makes you create them separately. For example,

	drop table if exists user_x_priv;
	create table user_x_priv
	(
		user_id        integer UNSIGNED,
		priv_code      integer UNSIGNED,
		INDEX          (user_id),
		INDEX          (priv_code)
	)
	;

becomes

	drop table if exists user_x_priv;
	create table user_x_priv
	(
		user_id        integer,
		priv_code      integer
	)
	;
	create index user_x_priv__user_id on user_x_priv(user_id);
	create index user_x_priv__priv_code on user_x_priv(priv_code);

### Datetime

Datetime becomes timestamp. For example,

	drop table if exists contact_info;
	create table contact_info
	(
		id            SERIAL PRIMARY KEY,
		last_name     varchar(40),
		first_name    varchar(80),
		removal_date  datetime
	);

becomes

	drop table if exists contact_info;
	create table contact_info
	(
		id            SERIAL PRIMARY KEY,
		last_name     varchar(40),
		first_name    varchar(80),
		removal_date  timestamp
	);

### SQL Files

This command will execute a file of SQL commands

	psql --echo-all --username=sndemo sndemo -h localhost --file=/path/to/t1.sql

### SQL comments

The file may contain comments, e.g.:

	drop table if exists contact_info;
	create table contact_info
	(
		id            SERIAL PRIMARY KEY,
		last_name     varchar(40),
		first_name    varchar(80),
		removal_date  timestamp
	);

	/* this type of comment also seems to work in PostgreSQL */

	insert into contact_info (last_name,first_name,removal_date)
		values ('Saunders','Mary',now());
	insert into contact_info (last_name,first_name,removal_date)
		values ('Saunders','Steve',now());
	insert into contact_info (last_name,first_name,removal_date)
		values ('Hamilton','Darlene',now());
	insert into contact_info (last_name,first_name,removal_date)
		values ('Bryant','Scott',now());

In my experience, \cd and \i seem to have permissions problems.


### Quotes

Double quotes do not work in PostgreSQL. This works:

	insert into contact_info (last_name,first_name,removal_date)
		values ('Bryant','Scott',now());

This fails:

	insert into contact_info (last_name,first_name,removal_date)
		values ("Bryant","Scott",now());

## JDBC

### getGeneratedKeys() TBD?!

	protected void runInsertQuery( QueryInjector queryInjector )
	{
		...
		statement = connection.prepareStatement
			( queryInjector.getQuery(), Statement.RETURN_GENERATED_KEYS );
		queryInjector.buildStatement( statement );
		statement.executeUpdate();
		resultSet = statement.getGeneratedKeys();
		if( resultSet.next() )
		{
			String id = resultSet.getString("id");
			queryInjector.setId( id );
		}
		else
		{
			queryInjector.autoincrementFailure();
		}

## Useful links

* <http://en.wikibooks.org/wiki/Converting_MySQL_to_PostgreSQL>
* <http://www.PostgreSQL.org/docs/9.1/static/tutorial-accessdb.html>
* <http://troels.arvin.dk/db/rdbms/>

## PSQL Commands

General
  \copyright             show PostgreSQL usage and distribution terms
  \g [FILE] or ;         execute query (and send results to file or |pipe)
  \h [NAME]              help on syntax of SQL commands, * for all commands
  \q                     quit psql

Query Buffer
  \e [FILE]              edit the query buffer (or file) with external editor
  \ef [FUNCNAME]         edit function definition with external editor
  \p                     show the contents of the query buffer
  \r                     reset (clear) the query buffer
  \s [FILE]              display history or save it to file
  \w FILE                write query buffer to file

Input/Output
  \copy ...              perform SQL COPY with data stream to the client host
  \echo [STRING]         write string to standard output
  \i FILE                execute commands from file
  \o [FILE]              send all query results to file or |pipe
  \qecho [STRING]        write string to query output stream (see \o)

Informational
  (options: S = show system objects, + = additional detail)
  \d[S+]                 list tables, views, and sequences
  \d[S+]  NAME           describe table, view, sequence, or index
  \da[S]  [PATTERN]      list aggregates
  \db[+]  [PATTERN]      list tablespaces
  \dc[S]  [PATTERN]      list conversions
  \dC     [PATTERN]      list casts
  \dd[S]  [PATTERN]      show comments on objects
  \ddp    [PATTERN]      list default privileges
  \dD[S]  [PATTERN]      list domains
  \des[+] [PATTERN]      list foreign servers
  \deu[+] [PATTERN]      list user mappings
  \dew[+] [PATTERN]      list foreign-data wrappers
  \df[antw][S+] [PATRN]  list [only agg/normal/trigger/window] functions
  \dF[+]  [PATTERN]      list text search configurations
  \dFd[+] [PATTERN]      list text search dictionaries
  \dFp[+] [PATTERN]      list text search parsers
  \dFt[+] [PATTERN]      list text search templates
  \dg[+]  [PATTERN]      list roles (groups)
  \di[S+] [PATTERN]      list indexes
  \dl                    list large objects, same as \lo_list
  \dn[+]  [PATTERN]      list schemas
  \do[S]  [PATTERN]      list operators
  \dp     [PATTERN]      list table, view, and sequence access privileges
  \drds [PATRN1 [PATRN2]] list per-database role settings
  \ds[S+] [PATTERN]      list sequences
  \dt[S+] [PATTERN]      list tables
  \dT[S+] [PATTERN]      list data types
  \du[+]  [PATTERN]      list roles (users)
  \dv[S+] [PATTERN]      list views
  \l[+]                  list all databases
  \z      [PATTERN]      same as \dp

Formatting
  \a                     toggle between unaligned and aligned output mode
  \C [STRING]            set table title, or unset if none
  \f [STRING]            show or set field separator for unaligned query output
  \H                     toggle HTML output mode (currently off)
  \pset NAME [VALUE]     set table output option
                         (NAME := {format|border|expanded|fieldsep|footer|null|
                         numericlocale|recordsep|tuples_only|title|tableattr|pager})
  \t [on|off]            show only rows (currently off)
  \T [STRING]            set HTML <table> tag attributes, or unset if none
  \x [on|off]            toggle expanded output (currently off)

Connection
  \c[onnect] [DBNAME|- USER|- HOST|- PORT|-]
                         connect to new database (currently "postgres")
  \encoding [ENCODING]   show or set client encoding
  \password [USERNAME]   securely change the password for a user

Operating System
  \cd [DIR]              change the current working directory
  \timing [on|off]       toggle timing of commands (currently off)
  \! [COMMAND]           execute command in shell or start interactive shell

Variables
  \prompt [TEXT] NAME    prompt user to set internal variable
  \set [NAME [VALUE]]    set internal variable, or list all if no parameters
  \unset NAME            unset (delete) internal variable

Large Objects
  \lo_export LOBOID FILE
  \lo_import FILE [COMMENT]
  \lo_list
  \lo_unlink LOBOID      large object operations

## pset

\pset option [ value ]

    This command sets options affecting the output of query result tables. option indicates which option is to be set. The semantics of value vary depending on the selected option. For some options, omitting value causes the option to be toggled or unset, as described under the particular option. If no such behavior is mentioned, then omitting value just results in the current setting being displayed.

    Adjustable printing options are:

    border

        The value must be a number. In general, the higher the number the more borders and lines the tables will have, but this depends on the particular format. In HTML format, this will translate directly into the border=... attribute; in the other formats only values 0 (no border), 1 (internal dividing lines), and 2 (table frame) make sense.
    columns

        Sets the target width for the wrapped format, and also the width limit for determining whether output is wide enough to require the pager or switch to the vertical display in expanded auto mode. Zero (the default) causes the target width to be controlled by the environment variable COLUMNS, or the detected screen width if COLUMNS is not set. In addition, if columns is zero then the wrapped format only affects screen output. If columns is nonzero then file and pipe output is wrapped to that width as well.
    expanded (or x)

        If value is specified it must be either on or off, which will enable or disable expanded mode, or auto. If value is omitted the command toggles between the on and off settings. When expanded mode is enabled, query results are displayed in two columns, with the column name on the left and the data on the right. This mode is useful if the data wouldn't fit on the screen in the normal "horizontal" mode. In the auto setting, the expanded mode is used whenever the query output is wider than the screen, otherwise the regular mode is used. The auto setting is only effective in the aligned and wrapped formats. In other formats, it always behaves as if the expanded mode is off.
    fieldsep

        Specifies the field separator to be used in unaligned output format. That way one can create, for example, tab- or comma-separated output, which other programs might prefer. To set a tab as field separator, type \pset fieldsep '\t'. The default field separator is '|' (a vertical bar).
    fieldsep_zero

        Sets the field separator to use in unaligned output format to a zero byte.
    footer

        If value is specified it must be either on or off which will enable or disable display of the table footer (the (n rows) count). If value is omitted the command toggles footer display on or off.
    format

        Sets the output format to one of unaligned, aligned, wrapped, html, latex, or troff-ms. Unique abbreviations are allowed. (That would mean one letter is enough.)

        unaligned format writes all columns of a row on one line, separated by the currently active field separator. This is useful for creating output that might be intended to be read in by other programs (for example, tab-separated or comma-separated format).

        aligned format is the standard, human-readable, nicely formatted text output; this is the default.

        wrapped format is like aligned but wraps wide data values across lines to make the output fit in the target column width. The target width is determined as described under the columns option. Note that psql will not attempt to wrap column header titles; therefore, wrapped format behaves the same as aligned if the total width needed for column headers exceeds the target.

        The html, latex, and troff-ms formats put out tables that are intended to be included in documents using the respective mark-up language. They are not complete documents! (This might not be so dramatic in HTML, but in LaTeX you must have a complete document wrapper.)
    linestyle

        Sets the border line drawing style to one of ascii, old-ascii or unicode. Unique abbreviations are allowed. (That would mean one letter is enough.) The default setting is ascii. This option only affects the aligned and wrapped output formats.

        ascii style uses plain ASCII characters. Newlines in data are shown using a + symbol in the right-hand margin. When the wrapped format wraps data from one line to the next without a newline character, a dot (.) is shown in the right-hand margin of the first line, and again in the left-hand margin of the following line.

        old-ascii style uses plain ASCII characters, using the formatting style used in PostgreSQL 8.4 and earlier. Newlines in data are shown using a : symbol in place of the left-hand column separator. When the data is wrapped from one line to the next without a newline character, a ; symbol is used in place of the left-hand column separator.

        unicode style uses Unicode box-drawing characters. Newlines in data are shown using a carriage return symbol in the right-hand margin. When the data is wrapped from one line to the next without a newline character, an ellipsis symbol is shown in the right-hand margin of the first line, and again in the left-hand margin of the following line.

        When the border setting is greater than zero, this option also determines the characters with which the border lines are drawn. Plain ASCII characters work everywhere, but Unicode characters look nicer on displays that recognize them.
    null

        Sets the string to be printed in place of a null value. The default is to print nothing, which can easily be mistaken for an empty string. For example, one might prefer \pset null '(null)'.
    numericlocale

        If value is specified it must be either on or off which will enable or disable display of a locale-specific character to separate groups of digits to the left of the decimal marker. If value is omitted the command toggles between regular and locale-specific numeric output.
    pager

        Controls use of a pager program for query and psql help output. If the environment variable PAGER is set, the output is piped to the specified program. Otherwise a platform-dependent default (such as more) is used.

        When the pager option is off, the pager program is not used. When the pager option is on, the pager is used when appropriate, i.e., when the output is to a terminal and will not fit on the screen. The pager option can also be set to always, which causes the pager to be used for all terminal output regardless of whether it fits on the screen. \pset pager without a value toggles pager use on and off.
    recordsep

        Specifies the record (line) separator to use in unaligned output format. The default is a newline character.
    recordsep_zero

        Sets the record separator to use in unaligned output format to a zero byte.
    tableattr (or T)

        Specifies attributes to be placed inside the HTML table tag in html output format. This could for example be cellpadding or bgcolor. Note that you probably don't want to specify border here, as that is already taken care of by \pset border. If no value is given, the table attributes are unset.
    title

        Sets the table title for any subsequently printed tables. This can be used to give your output descriptive tags. If no value is given, the title is unset.
    tuples_only (or t)

        If value is specified it must be either on or off which will enable or disable tuples-only mode. If value is omitted the command toggles between regular and tuples-only output. Regular output includes extra information such as column headers, titles, and various footers. In tuples-only mode, only actual table data is shown.

https://docs.python.org/3/library/venv.html

# Creation of virtual environments

## venv

The venv module has the advantage of being built into Python3 already. It provides support for creating lightweight “virtual environments” with their own site directories, optionally isolated from system site directories. Each virtual environment has its own Python binary (which matches the version of the binary that was used to create this environment) and can have its own independent set of installed Python packages in its site directories.

```
python3 -m venv /Users/mb/dev/billytoons/venv-build-1
```

## virtualenv

virtualenv claims venv to be a subset of virtualenv, and that the venv module does not offer all features of virtualenv. OTOH, virtualenv is yet another thing that must be installed.

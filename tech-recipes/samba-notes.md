First of all, samba has to be installed. It is not part of the CentOS
minimal installation, for example. So to install samba, run yum:

	yum install samba

Once samba is installed, it has to be added to chkconfig:

	chkconfig --level 2345 smb on

The users will have to be set up

	* set up a regular Unix user using vipw and passwd, e.g. 'dev'
	* create the home directory, chown it to nobody:nobody
	* set up a corresponding samba user, e.g. `smbpasswd -L -a dev`

The configuration will have to be set up in /etc/samba/smb.conf. Here
is an example:

	-----------------------------------------
	[global]
	  workgroup = WORKGROUP
	  server string = Samba Server Version %v
	  netbios name = CLASSROOM
	  log file = /var/log/samba/%m.log
	  max log size = 50
	  load printers = no
	  security = user
	  passdb backend = tdbsam
	  null passwords = true
	  username map = /etc/samba/smbusers

	[dev]
	  path = /home/dev
	  read only = No
	  force user = nobody
	  force group = nobody
	  guest ok = Yes
	  public = yes
	  browseable = yes
	-----------------------------------------

Finally, start samba:

	service smb start
	service smb status

It may be necessary to disable both iptables and selinux. Once samba
is started on the server, mount the volume on the client:

	mount -t cifs //192.168.0.223/dev /cro/j2eeqa

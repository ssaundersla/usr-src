Using SSH without a password
============================

## Client Side

On your desktop machine, add entries for the remote server to the SSH
configuration file in `~/.ssh/config`, e.g.:

	Host jqaroot
		User root
		Hostname 192.168.0.223
		Port 22
		IdentityFile ~/.ssh/id_rsa
	Host jqa
		User dev
		Hostname 192.168.0.223
		Port 22
		IdentityFile ~/.ssh/id_rsa

I have a multitude of keys pairs in my `.ssh` directory, so I always
specify the private keys in this file. Note that if you do not yet
have a key pair, you will have to generate one:

	ssh-keygen -t rsa

## Server Side

Use SSH from your home directory to configure the server. You will
have to enter the remote-user password after each of these commands,
and you will have to go through the process for each remote user you
set up in the config file. The above example has users `root` and
`dev`, but I'll just show `dev` here:

1. Use SSH from your desktop machine to connect to the server as the
   selected remote user and create .ssh directory under it. e.g.:

ssh dev@192.168.0.223 mkdir -p .ssh


2. Use SSH from your desktop machine again to upload your public key
   to the server under the selected remote user's `.ssh` directory as
   a file named `authorized_keys`, e.g.:

cat .ssh/id_rsa.pub | ssh dev@192.168.0.223 'cat >> .ssh/authorized_keys'


3. Due to different SSH versions on servers, set permissions on the
   `.ssh` directory and the `authorized_keys` file, e.g.:

ssh dev@192.168.0.223 "chmod 700 .ssh; chmod 640 .ssh/authorized_keys"

## SSH bypassing ~/.ssh/config

For ad hoc logins, you can use the following:

	ssh -i ~/.ssh/sealevel_prototype.pem mooreboeck@54.153.111.61

#! /bin/sh
#
# This script should be run after each change to the files in the
# '~/usr' in order to regenerate the '~/usr/bin folder'. This script
# assumes that the '~/usr' folder has been created, that the 'src'
# folder has been cloned, and that the 'install' script has been run.
#
# Note however that the commands which are symbolic links will
# automatically pick up changes. There is no need to run this script
# when those are updated.
#
#-------------------------------------------------------------------------------
# Recreate the `bin` directory
#-------------------------------------------------------------------------------
#
cd "$HOME/usr"
rm -rf bin
mkdir bin
#
# Always install the bash scripts
#
ln -s ~/usr/src/bash/mrk.sh ~/usr/bin/mrk
# ln -s ~/usr/src/bash/mgit.sh ~/usr/bin/mgit
# ln -s ~/usr/src/bash/mgitclean.sh ~/usr/bin/mgitclean
#
# If OS = Linux (gnome)
#
if [ "$(uname)" = "Linux" ]; then
  if [ -d "$HOME/.config/caja" ]; then
    ln -sf ~/usr/src/gnome/find-emblems/find-emblems.py    ~/usr/bin/find-emblems
    ln -sf ~/usr/src/gnome/caja-scripts/create-manifest.py ~/.config/caja/scripts/'Create manifest'
    ln -sf ~/usr/src/gnome/caja-scripts/create-tex-from-md.py ~/.config/caja/scripts/'Generate PDF from MD'
    ln -sf ~/usr/src/gnome/caja-scripts/duplicate-file.py ~/.config/caja/scripts/'Duplicate'
    ln -sf ~/usr/src/gnome/caja-scripts/meld-2-files.py ~/.config/caja/scripts/'Meld two files'
    ln -sf ~/usr/src/gnome/caja-scripts/pause-files.py ~/.config/caja/scripts/'Pause or unpause'
  fi
fi
#
# If Perl is installed
#
if [ -n "$(which perl)" ]; then
  ln -sf ~/usr/src/perl/cloc-1.62.pl                     ~/usr/bin/cloc
  ln -sf ~/usr/src/perl/mkmk.pl                          ~/usr/bin/mkmk
fi
#
# If Python is installed
#
if [ -n "$(which python)" ]; then
  ln -sf ~/usr/src/python/dropbox.py                     ~/usr/bin/dropbox.py
  ln -sf ~/usr/src/python/dk/dk.py                       ~/usr/bin/dk
  ln -sf ~/usr/src/python/git-changelog/main.py          ~/usr/bin/ggcl
#   ln -sf ~/usr/src/python/mkpov/mkpov.py                 ~/usr/bin/mkpov
  ln -sf ~/usr/src/python/password-generator/main.py     ~/usr/bin/genpass
  ln -sf ~/usr/src/python/stinkers/stinkers.py           ~/usr/bin/stinkers
#   ln -sf ~/usr/src/python/unc/main.py                    ~/usr/bin/unc
fi
#
# If todo-txt is installed
#
for file in "$HOME"/Applications/todo.txt_cli-*
do
  ln -s "${file}/todo.sh" ~/usr/bin/todo
done
#
# Note: If the e.g. python folder had a better convention, something like
# this would work:
#
#    for d in ~/usr/src/python/*; do
#      ln -sf $d ~/usr/bin/$(basename $d)
#    done
#
#-------------------------------------------------------------------------------
# Update dotfiles-directory config files
# Always append files from local if they exist
#-------------------------------------------------------------------------------
#
cp ~/usr/src/dotfiles/exrc  ~/.exrc
if [ -f ~/usr/local/dotfiles/exrc ]; then
  cat ~/usr/local/dotfiles/exrc >> ~/.exrc
fi
#
cp ~/usr/src/dotfiles/gitignore  ~/.gitignore
if [ -f ~/usr/local/dotfiles/gitignore ]; then
  cat ~/usr/local/dotfiles/gitignore >> ~/.gitignore
fi
#
#-------------------------------------------------------------------------------
# Perform local customization other than dotfiles-directory config files
#-------------------------------------------------------------------------------
#
cd
cd usr/local
./update.sh

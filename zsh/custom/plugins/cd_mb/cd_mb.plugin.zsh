function cd_mb ()
{
  if [ -z "$1" ]; then
    cd "$HOME/dev"
  else
    case "$1" in
      ssh)
        cd "$HOME/.ssh"
        ;;
      doc)
        cd "$HOME/Documents"
        ;;
      desktop)
        cd "$HOME/Desktop"
        ;;
      vscode)
        cd "$HOME/Library/Application Support/Code/User"
        ;;
      usr)
        cd "$HOME/usr/src"
        ;;
      earth)
        cd "$HOME/dev/estd"
        ;;
      *)
        cd "$HOME/dev/$1"
        ;;
    esac
  fi
}

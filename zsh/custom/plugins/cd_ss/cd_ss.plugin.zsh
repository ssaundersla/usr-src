function cd_ss ()
{
  if [ -z "$1" ]; then
    cd "$HOME/dev"
  else
    case "$1" in
      ssh)
        cd "$HOME/.ssh"
        ;;
      doc)
        cd "$HOME/Documents"
        ;;
      desktop)
        cd "$HOME/Desktop"
        ;;
      vscode)
        cd "$HOME/Library/Application Support/Code/User"
        ;;
      usr)
        cd "$HOME/usr/src"
        ;;
      billytoons)
        cd "$HOME/dev/billytoons"
        ;;
      pax)
        cd "$HOME/Dropbox/RPG-Pax-Telluris"
        ;;
      *)
        cd "$HOME/dev/$1"
        ;;
    esac
  fi
}

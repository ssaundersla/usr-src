function cdr ()
{
  if [ -z "$1" ]; then
    echo "/websites/jpl/rails"
  else
    case "$1" in
      root)
	    echo "/websites/jpl"
		;;
      boot)
	    echo "/websites/jpl/boot"
		;;
      elk_config)
	    echo "/websites/jpl/elk_config"
		;;
      logs)
	    echo "/websites/jpl/logs"
		;;
      cron)
	    echo "/websites/jpl/logs/cron"
		;;
      tools)
	    echo "/websites/jpl/tools"
		;;
      rails)
	    echo "/websites/jpl/rails"
		;;
      demo-1)
	    echo "/websites/jpl/rails/demo-1"
		;;
      demo-2)
	    echo "/websites/jpl/rails/demo-2"
		;;
      demo-3)
	    echo "/websites/jpl/rails/demo-3"
		;;
      demo-4)
	    echo "/websites/jpl/rails/demo-4"
		;;
      demo-5)
	    echo "/websites/jpl/rails/demo-5"
		;;
      development)
	    echo "/websites/jpl/rails/development"
		;;
      production)
	    echo "/websites/jpl/rails/production"
		;;
      staging)
	    echo "/websites/jpl/rails/staging"
		;;
      httpd)
	    echo "/etc/httpd"
		;;
      apache)
	    echo "/etc/httpd"
		;;
      vhosts)
	    echo "/etc/httpd/conf.d"
		;;
      *)
        echo "/websites/jpl/rails/$1"
        ;;
    esac
  fi
}



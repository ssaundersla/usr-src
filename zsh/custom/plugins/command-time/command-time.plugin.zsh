_command_time_preexec() {
  timer=${timer:-$SECONDS}
  set_my_shell_prompt
}

_command_time_precmd() {
  set_my_shell_prompt
  if [ $timer ]; then
    timer_secs=$(($SECONDS - $timer))
    set_my_shell_prompt ${timer_secs}
    unset timer
  fi
}

precmd_functions+=(_command_time_precmd)
preexec_functions+=(_command_time_preexec)
